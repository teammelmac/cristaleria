<?php
/**
 * Lollum
 * 
 * The Header for our theme
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?><!DOCTYPE html>
<?php
$animations_check = ((get_option('lol_check_animations')  == 'true') ? 'lol-animations-yes' : 'lol-animations-no');
$animations_touch_check = ((get_option('lol_check_animations_touch')  == 'true') ? 'lol-animations-touch-yes' : 'lol-animations-touch-no');
?>
<!--[if IE 8]> <html class="no-js lt-ie9 <?php echo $animations_check.' '.$animations_touch_check; ?>" <?php language_attributes();?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js <?php echo $animations_check.' '.$animations_touch_check; ?>" <?php language_attributes();?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title('|', true, 'right');
	// Add the blog name.
	bloginfo('name');
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) {
		echo " | $site_description";
	}
	// Add a page number if necessary:
	if ($paged >= 2 || $page >= 2) {
		echo ' | ' . sprintf(__('Page %s', 'lollum'), max($paged, $page));
	}
	?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php // if (is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply'); ?>
	<link rel="alternate" title="<?php printf(__('%s RSS Feed', 'lollum'), get_bloginfo('name')); ?>" href="<?php bloginfo('rss2_url'); ?>">
	<link rel="alternate" title="<?php printf(__('%s Atom Feed', 'lollum'), get_bloginfo('name')); ?>" href="<?php bloginfo('atom_url'); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php if (get_option('lol_custom_favicon')) { ?>
		<link rel="shortcut icon" href="<?php echo get_option('lol_custom_favicon'); ?>">
	<?php } ?>
	<script>document.documentElement.className += " js";</script>
	<!-- BEGIN WP -->
	<?php wp_head(); ?>
	<!-- END WP -->
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/ie8.js?ver=1.0" type="text/javascript"></script>
	<![endif]-->
</head>
<body <?php body_class();?>>

<!-- BEGIN wrap -->
<div id="wrap" class="<?php echo (get_option('lol_layout') == 'layout-boxed') ? 'boxed' : ''; ?>">

	<?php if (get_option('lol_check_top_header') == 'true') : ?>
	
	<div id="top-header">
		<div class="container">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-12 -->
				<div class="col-12">
					<div class="company-info">
						<?php if (get_option('lol_tel_company') != '') : ?>
							<div class="info"><i class="icon icon-phone"></i>+<?php echo get_option('lol_tel_company'); ?></div>
						<?php endif; ?>
						<?php if (get_option('lol_email_company') != '') : ?>
							<div class="info"><i class="icon icon-envelope"></i><a href="mailto:<?php echo get_option('lol_email_company'); ?>"><?php echo get_option('lol_email_company'); ?></a></div>
						<?php endif; ?>
					</div>
					<div class="top-header-nav">
						<?php if (defined('ICL_SITEPRESS_VERSION') && get_option('lol_top_lang_switcher') == 'true') : ?>
							<span class="block-sep"></span>
							<div class="top-language-switcher">
								<?php lollum_languages_list_header(); ?>
							</div>
						<?php endif; ?>
						<?php if (get_option('lol_check_social_header') == 'true') : ?>
						<span class="block-sep"></span>
						<div class="header-social-links block">
							<ul class="social-links">
								<?php if (get_option('lol_h_facebook') != '') : ?>
									<li><a class="lol-facebook" href="<?php echo get_option('lol_h_facebook'); ?>" title="Facebook">Facebook</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_twitter') != '') : ?>
									<li><a class="lol-twitter" href="<?php echo get_option('lol_h_twitter'); ?>" title="Twitter">Twitter</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_dribbble') != '') : ?>
									<li><a class="lol-dribbble" href="<?php echo get_option('lol_h_dribbble'); ?>" title="Dribbble">Dribbble</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_linkedin') != '') : ?>
									<li><a class="lol-linkedin" href="<?php echo get_option('lol_h_linkedin'); ?>" title="Linkedin">Linkedin</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_flickr') != '') : ?>
									<li><a class="lol-flickr" href="<?php echo get_option('lol_h_flickr'); ?>" title="Flickr">Flickr</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_tumblr') != '') : ?>
									<li><a class="lol-tumblr" href="<?php echo get_option('lol_h_tumblr'); ?>" title="Tumblr">Tumblr</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_vimeo') != '') : ?>
									<li><a class="lol-vimeo" href="<?php echo get_option('lol_h_vimeo'); ?>" title="Vimeo">Vimeo</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_vine') != '') : ?>
									<li><a class="lol-vine" href="<?php echo get_option('lol_h_vine'); ?>" title="Vine">Vine</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_youtube') != '') : ?>
									<li><a class="lol-youtube" href="<?php echo get_option('lol_h_youtube'); ?>" title="Youtube">Youtube</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_instagram') != '') : ?>
									<li><a class="lol-instagram" href="<?php echo get_option('lol_h_instagram'); ?>" title="Instagram">Instagram</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_google') != '') : ?>
									<li><a class="lol-google" href="<?php echo get_option('lol_h_google'); ?>" title="Google Plus">Google Plus</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_stumbleupon') != '') : ?>
									<li><a class="lol-stumbleupon" href="<?php echo get_option('lol_h_stumbleupon'); ?>" title="StumbleUpon">StumbleUpon</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_forrst') != '') : ?>
									<li><a class="lol-forrst" href="<?php echo get_option('lol_h_forrst'); ?>" title="Forrst">Forrst</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_behance') != '') : ?>
									<li><a class="lol-behance" href="<?php echo get_option('lol_h_behance'); ?>" title="Behance">Behance</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_digg') != '') : ?>
									<li><a class="lol-digg" href="<?php echo get_option('lol_h_digg'); ?>" title="Digg">Digg</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_delicious') != '') : ?>
									<li><a class="lol-delicious" href="<?php echo get_option('lol_h_delicious'); ?>" title="Delicious">Delicious</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_deviantart') != '') : ?>
									<li><a class="lol-deviantart" href="<?php echo get_option('lol_h_deviantart'); ?>" title="DeviantArt">DeviantArt</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_foursquare') != '') : ?>
									<li><a class="lol-foursquare" href="<?php echo get_option('lol_h_foursquare'); ?>" title="Foursquare">Foursquare</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_github' )!= '') : ?>
									<li><a class="lol-github" href="<?php echo get_option('lol_h_github'); ?>" title="GitHub">GitHub</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_myspace') != '') : ?>
									<li><a class="lol-myspace" href="<?php echo get_option('lol_h_myspace'); ?>" title="MySpace">MySpace</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_orkut') != '') : ?>
									<li><a class="lol-orkut" href="<?php echo get_option('lol_h_orkut'); ?>" title="Orkut">Orkut</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_pinterest') != '') : ?>
									<li><a class="lol-pinterest" href="<?php echo get_option('lol_h_pinterest'); ?>" title="Pinterest">Pinterest</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_soundcloud') != '') : ?>
									<li><a class="lol-soundcloud" href="<?php echo get_option('lol_h_soundcloud'); ?>" title="SoundCloud">SoundCloud</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_stackoverflow') != '') : ?>
									<li><a class="lol-stackoverflow" href="<?php echo get_option('lol_h_stackoverflow'); ?>" title="Stack Overflow">Stack Overflow</a></li>
								<?php endif; ?>
								<?php if (get_option('lol_h_rss') != '') : ?>
									<li><a class="lol-rss" href="<?php echo get_option('lol_h_rss'); ?>" title="RSS">RSS</a></li>
								<?php endif; ?>
							</ul>
						</div>
						<?php endif; ?>
						
						<?php if (get_option('lol_check_menu_header') == 'true') : ?>
							<span class="block-sep"></span>
							<div class="top-header-menu block">
								<?php wp_nav_menu(array('theme_location' => 'header', 'depth' => -1)); ?>
							</div>
						<?php endif; ?>
						<?php if (get_option('lol_check_search_header') == 'true') : ?>
							<span class="block-sep"></span>
							<?php if (get_option('lol_type_search_header') == 'normal') { ?>
								<div class="header-search block">
									<a href="#" class="icon-search-btn"><i class="icon-search"></i></a>
									<div class="searchbox-wrap">
										<?php get_search_form(); ?>
									</div>
								</div>
							<?php } elseif (get_option('lol_type_search_header') == 'products' && lollum_check_is_woocommerce()) { ?>
								<div class="header-search block">
									<a href="#" class="icon-search-btn"><i class="icon-search"></i></a>
									<div class="searchbox-wrap">
										<?php get_product_search_form(); ?>
									</div>
								</div>
							<?php } ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php endif; ?>

	<!-- BEGIN branding -->
	<header id="branding" role="banner">
		<div class="container">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-12 -->
				<div class="col-12">
					<!-- BEGIN #logo -->
					<div id="logo">
						<a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr( get_bloginfo('name', 'display') ); ?>">
							<?php if (get_option('lol_custom_logo')): ?> 
								<img src="<?php echo get_option('lol_custom_logo'); ?>" alt="<?php bloginfo('name'); ?>" id="desktop-logo">
								<?php if (get_option('lol_custom_logo_retina')): ?>
									<img src="<?php echo get_option('lol_custom_logo_retina'); ?>" alt="<?php bloginfo('name'); ?>" id="retina-logo">
								<?php endif; ?>
							<?php else: ?>
								<h1 id="site-title"><?php bloginfo('name'); ?></h1>
							<?php endif; ?>
						</a>
					</div>

					<div class="<?php echo (lollum_check_is_woocommerce() && (get_option('lol_check_cart_header') == 'true')) ? "cart-yes" : "" ?>">

						<!-- BEGIN nav-menu -->
						<nav id="nav-menu" role="navigation">
							<?php // BEGIN screen reader links ?>
							<h3 class="assistive-text"><?php _e('Main menu', 'lollum'); ?></h3>
							<div class="skip-link">
								<a class="assistive-text" href="#content" title="<?php esc_attr_e('Skip to primary content', 'lollum'); ?>"><?php _e('Skip to primary content', 'lollum'); ?></a>
							</div>
							<div class="skip-link">
								<a class="assistive-text" href="#secondary" title="<?php esc_attr_e('Skip to secondary content', 'lollum'); ?>"><?php _e('Skip to secondary content', 'lollum'); ?></a>
							</div>
							<?php // END screen reader links ?>

							<?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'sf-menu', 'depth' => 4)); ?>
						</nav>

						<nav id="mobile-nav-menu" role="navigation">
							<div class="mobile-nav-menu-inner">
								<?php lollum_mobile_menu('primary'); ?>
							</div>
							<?php if (lollum_check_is_woocommerce() && (get_option('lol_check_cart_header') == 'true')) { ?>

								<?php global $woocommerce; ?>

								<a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" id="icon-cart-menu"><i class="icon-suitcase"></i></a>

							<?php } ?>
						</nav>

					</div>

					<?php if (lollum_check_is_woocommerce() && (get_option('lol_check_cart_header') == 'true')) { ?>

						<?php global $woocommerce; ?>

						<!-- BEGIN mini-cart -->
						<div id="lol-mini-cart">
							<div id="header-cart">
								<div id="header-cart-inner">
									<div class="cart-title"><?php _e('Cart', 'lollum'); ?></div>
									<div class="cart-contents">
										<?php echo $woocommerce->cart->get_cart_total(); ?> / <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'lollum'), $woocommerce->cart->cart_contents_count);?>
									</div>
								</div>
							</div>
							<div class="cart-items">
								<div class="cart-items-inner">
									<?php if ( sizeof($woocommerce->cart->cart_contents)>0 ) { ?>
										<?php foreach ($woocommerce->cart->cart_contents as $cart_item_key => $cart_item) { ?>
											<?php
											$bag_product = $cart_item['data']; 
											$product_title = $bag_product->get_title();
											?>
											<?php if ($bag_product->exists() && $cart_item['quantity']>0) { ?>
												<div class="cart-product">   	
													<figure><a class="cart-product-img" href="<?php echo get_permalink($cart_item['product_id']); ?>"><?php echo $bag_product->get_image(); ?></a></figure>                   
													<div class="cart-product-details">
														<div class="cart-product-title">
															<a href="<?php echo get_permalink($cart_item['product_id']); ?>"><?php echo apply_filters('woocommerce_cart_widget_product_title', $product_title, $bag_product); ?></a>
														</div>
														<div class="cart-product-quantity"><?php _e('Quantity:', 'lollum'); ?> <?php echo $cart_item['quantity']; ?></div>
														<div class="cart-product-price"><?php echo woocommerce_price($bag_product->get_price()); ?></div>
													</div>
												</div>
											<?php } ?>
										<?php } ?>
										<div class="cart-buttons">
											<a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" class="lol-button small view-cart"><?php _e('View shopping bag', 'lollum'); ?></a>
											<a href="<?php echo esc_url($woocommerce->cart->get_checkout_url()); ?>" class="lol-button small"><?php _e('Proceed to checkout', 'lollum'); ?></a>
										</div>
									<?php } else { ?>
										<p><?php _e('No products in the shopping bag.', 'lollum'); ?></p>
									<?php } ?>
								</div>
							</div>
						</div>
						<!-- END mini-cart -->

					<?php } ?>

				</div>
				<!-- END col-12 -->
			</div>
			<!-- END row -->
		</div>
	</header>
	<!-- END branding -->