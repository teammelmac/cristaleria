(function($) {
	$(document).ready(function() {
		"use strict";
		/* global lolfmk_progress_vars, jQuery */
		/*jslint bitwise: true */
		var color = lolfmk_progress_vars.barColor;
		$('.chart').appear(function() {
			var currentChart = $(this);
			currentChart.easyPieChart({
				animate: 1000,
				barColor: color,
				trackColor: '#ebebeb',
				lineWidth: 7,
				lineCap: 'square',
				size: '110',
				scaleColor: false,
				onStep: function(value) {
					this.$el.find('span').text(~~value+'%');
				},
				onStop: function() {
					this.$el.find('span').text(currentChart.data('percent')+'%');
				}
			});
		});
	});
})(jQuery);