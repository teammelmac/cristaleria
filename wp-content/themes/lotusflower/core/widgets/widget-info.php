<?php
/**
 * Lollum
 * 
 * Custom Info Widget
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

class Lollum_Widget_Info extends WP_Widget {

	function Lollum_Widget_Info() {

		$widget_ops = array(
			'classname' => 'lol_widget_info', 
			'description' => __('Use this widget to display your company information.', 'lollum') 
		);
		$control_ops = array('id_base' => 'lol_widget_info');
		$this->WP_Widget('lol_widget_info', __('Custom Info Widget', 'lollum'), $widget_ops, $control_ops);
	}

	function widget($args, $instance) {

		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);

		echo $before_widget;

		if ($title) { 
			echo $before_title . $title . $after_title; 
		}

		$text = $instance['text'];
		$address = $instance['address'];
		$region = $instance['region'];
		$country = $instance['country'];
		$phone = $instance['phone'];
		$fax = $instance['fax'];
		?>
			
		<div class="info_widget">
			<?php if ($text) { ?>
				<p><?php echo $text; ?></p>
			<?php } ?>
			<div class="vcard">
				<div class="adr">
					<p>
				<?php if ($address) { ?>
					<span class="street-address"><?php echo $address; ?></span><br>
				<?php } ?>
				<?php if ($region) { ?>
					<span class="region"><?php echo $region; ?></span>
				<?php } ?>
				<?php if ($country) { ?>
					<span class="country-name"><?php echo $country; ?></span>
				<?php } ?>
					</p>
				<?php if ($phone) { ?>
					<span class="tel"><i class="icon-phone"></i><span class="value"><?php echo $phone; ?></span></span><br>
				<?php } ?>
				<?php if ($fax) { ?>
					<span class="fax"><i class="icon-print"></i><span class="value"><?php echo $fax; ?></span></span>
				<?php } ?>
				</div>
			</div>
		</div>
	
		<?php
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
		
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['address'] = strip_tags($new_instance['address']);
		$instance['region'] = strip_tags($new_instance['region']);
		$instance['country'] = strip_tags($new_instance['country']);
		$instance['phone'] = strip_tags($new_instance['phone']);
		$instance['fax'] = strip_tags($new_instance['fax']);

		return $instance;
	}

	function form($instance) {

		$defaults = array(
			'title' => 'Contact Info'
		);

		$instance = wp_parse_args((array) $instance, $defaults); 

		$text = (isset($instance['text']) ? $instance['text'] : '');
		$address = (isset($instance['address']) ? $instance['address'] : '');
		$region = (isset($instance['region']) ? $instance['region'] : '');
		$country = (isset($instance['country']) ? $instance['country'] : '');
		$phone = (isset($instance['phone']) ? $instance['phone'] : '');
		$fax = (isset($instance['fax']) ? $instance['fax'] : '');

		?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'lollum') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text info:', 'lollum') ?></label>
			<textarea style="height: 150px;" class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo stripslashes(htmlspecialchars($text, ENT_QUOTES)); ?></textarea>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address:', 'lollum') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" value="<?php echo $address; ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('region'); ?>"><?php _e('Region:', 'lollum') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('region'); ?>" name="<?php echo $this->get_field_name('region'); ?>" value="<?php echo $region; ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('country'); ?>"><?php _e('Country:', 'lollum') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('country'); ?>" name="<?php echo $this->get_field_name('country'); ?>" value="<?php echo $country; ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone:', 'lollum') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" value="<?php echo $phone; ?>">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('fax'); ?>"><?php _e('Fax:', 'lollum') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('fax'); ?>" name="<?php echo $this->get_field_name('fax'); ?>" value="<?php echo $fax; ?>">
		</p>


	
	<?php
	}

}

add_action('widgets_init', 'lol_info_widget');

function lol_info_widget() {
	register_widget('Lollum_Widget_Info');
}