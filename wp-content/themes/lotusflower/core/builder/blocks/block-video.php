<?php
/**
 * Lollum
 * 
 * The template for displaying the video block
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lolfmk_print_block_video')) {
	function lolfmk_print_block_video($item) {
		$block_title = lolfmk_find_xml_value($item, 'block-title');
		$block_subtitle = lolfmk_find_xml_value($item, 'block-subtitle');
		$block_video_embed = lolfmk_find_xml_value($item, 'block-video-embed');
		$text_color = lolfmk_find_xml_value($item, 'text-color');

		if ($block_title == '' && $block_subtitle == '') {
			echo '<div class="lol-item-block-video no-margin '.$text_color.'">';
		} else {
			echo '<div class="lol-item-block-video '.$text_color.'">';
		}

		if ($block_title != '') {
			echo '<h2>'.$block_title.'</h2>';
		}
		if ($block_subtitle != '') {
			echo '<p>'.$block_subtitle.'</p>';
		}
		
		echo "<div class='entry-video'>";
		echo stripslashes(htmlspecialchars_decode($block_video_embed));
		echo "</div>";
		echo '</div>';
	}
}