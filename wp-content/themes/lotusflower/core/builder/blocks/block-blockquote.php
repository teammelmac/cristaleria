<?php
/**
 * Lollum
 * 
 * The template for displaying the blockquote block
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lolfmk_print_blockquote')) {
	function lolfmk_print_blockquote($item) {
		$blockquote_text = lolfmk_find_xml_value($item, 'blockquote-text');
		$blockquote_author = lolfmk_find_xml_value($item, 'blockquote-author');

		echo '<div class="lol-item-blockquote">';
		echo '<blockquote>“'.$blockquote_text.'”</blockquote>';
		echo '<cite>'.$blockquote_author.'</cite>';
		echo '</div>';
	}
}