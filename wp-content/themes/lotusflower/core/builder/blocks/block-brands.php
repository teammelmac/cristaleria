<?php
/**
 * Lollum
 * 
 * The template for displaying the brands block
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lolfmk_print_brands')) {
	function lolfmk_print_brands($item) {
		$header_text = lolfmk_find_xml_value($item, 'header-text');
		$brands_number = lolfmk_find_xml_value($item, 'brands-number');
		$list_brands = lolfmk_find_xml_node($item, 'child-group');

		if ($header_text != '') {
			echo '<div class="divider"><h3>'.$header_text.'</h3></div>';
		}

		$col = '';
		switch($brands_number) {
			case '2':
				$col = 6;
				break;
			case '3':
				$col = 4;
				break;
			case '4':
				$col = 3;
				break;
			case '6':
				$col = 2;
				break;
		}

		echo '<div class="lol-brands">';

		echo '<div class="row">';

		$i = 0;
		foreach ($list_brands->childNodes as $brand) {
			if ($i > 0) {
				$img_src = lolfmk_find_xml_value($brand, 'image-src');
				$alt_image = lolfmk_find_xml_value($brand, 'alt-image');
				$link_image = lolfmk_find_xml_value($brand, 'link-image');
				echo '<div class="lol-item-brand col-'.$col.'">';
				if ($link_image != '') {
					echo '<a href="'.$link_image.'">';
				}
				echo '<img src="'.$img_src.'" alt="'.$alt_image.'">';
				if ($link_image != '') {
					echo '</a>';
				}
				echo '</div>';
			}
			$i++;
		}

		echo '</div>';

		echo '</div>';
	}
}