<?php
/**
 * Lollum
 * 
 * The template for displaying the blog full block
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lolfmk_print_blog_full')) {
	function lolfmk_print_blog_full($item) {
		$header_text = lolfmk_find_xml_value($item, 'header-text');
		$blog_type = lolfmk_find_xml_value($item, 'blog-type');
		$post_category = lolfmk_find_xml_value($item, 'post-category');

		if ($header_text != '') {
			echo '<div class="divider"><h3>'.$header_text.'</h3></div>';
		}

		if ($blog_type == 'recent') {
			$args = array(
				'post_type' => 'post',
				'ignore_sticky_posts' => true,
				'posts_per_page' => 4
			);
		} else {
			$args = array(
				'post_type' => 'post',
				'ignore_sticky_posts' => true,
				'cat' => $post_category,
				'posts_per_page' => 4
			);
		}

		echo '<div class="row">';

		$post_query = new WP_Query($args);
		while($post_query->have_posts()) : $post_query->the_post(); ?>

			<div class="col-3">
				<div class="post-item">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="post-mask">
						<div class="post-mask-inner"></div>
						<div class="post-mask-inner2"></div>
							<div class="post-link">
								<i class="icon-search"></i>
							</div>
							<div class="post-thumb">
								<?php the_post_thumbnail('square-thumb'); ?>
							</div>
					</a>
					<div class="post-meta">
						<div class="divider-sm">
							<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						</div>
						<div><?php the_excerpt(); ?></div>
					</div>
				</div>
			</div>

		<?php endwhile;
		wp_reset_postdata();

		echo '</div>';
	}
}