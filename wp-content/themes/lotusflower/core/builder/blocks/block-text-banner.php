<?php
/**
 * Lollum
 * 
 * The template for displaying the text-banner block
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lolfmk_print_block_text_banner')) {
	function lolfmk_print_block_text_banner($item) {
		$header_text = lolfmk_find_xml_value($item, 'header-text');
		$banner_title = lolfmk_find_xml_value($item, 'banner-title');
		$banner_subtitle = lolfmk_find_xml_value($item, 'banner-subtitle');
		$banner_link = lolfmk_find_xml_value($item, 'banner-link');

		if ($header_text != '') {
			echo '<div class="divider"><h3>'.$header_text.'</h3></div>';
		}

		echo '<div class="lol-item-text-banner-wrap">';
		echo '<div class="lol-item-text-banner">';
		if ($banner_link != '') {
			echo '<a href="'.$banner_link.'">';
		}
		echo '<h2>'.$banner_title.'</h2>';
		echo '<p>'.$banner_subtitle.'</p>';
		if ($banner_link != '') {
			echo '</a>';
		}
		echo '</div>';
		echo '</div>';
	}
}