<?php
/**
 * Lollum
 * 
 * The template for displaying the testimonial block
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lolfmk_print_testimonial')) {
	function lolfmk_print_testimonial($item) {
		$header_text = lolfmk_find_xml_value($item, 'header-text');
		$list_testimonials = lolfmk_find_xml_node($item, 'child-group');

		if ($header_text != '') {
			echo '<div class="divider"><h3>'.$header_text.'</h3></div>';
		}

		echo '<div class="flexslider flex-testimonial lol-item-testimonial">';
		echo '<ul class="slides">';
		echo '<div class="preloader"></div>';
		$i = 0;
		foreach ($list_testimonials->childNodes as $testimonial) {
			if ($i > 0) {
				$testimonial_text = lolfmk_find_xml_value($testimonial, 'testimonial-text');
				$testimonial_author = lolfmk_find_xml_value($testimonial, 'testimonial-author');
				$testimonial_subtitle = lolfmk_find_xml_value($testimonial, 'testimonial-subtitle');
				$testimonial_avatar = lolfmk_find_xml_value($testimonial, 'testimonial-avatar');
				echo '<li>';
				echo '<blockquote class="testimonial-content">'.$testimonial_text.'</blockquote>';
				echo '<div class="testimonial-meta">';
				if ($testimonial_avatar) {
					echo '<img src="'.$testimonial_avatar.'" alt="'.$testimonial_author.'">';
				}
				echo '<cite>'.$testimonial_author.'</cite>';
				if ($testimonial_subtitle) {
					echo '<span>'.$testimonial_subtitle.'</span>';
				}
				echo '</div>';
				echo '</li>';
			}
			$i++;
		}
		echo '</ul>';
		echo '</div>';
	}
}