<?php
/**
 * Lollum
 * 
 * Register general scripts
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

add_action('wp_enqueue_scripts', 'lollum_register_js');
if (!function_exists('lollum_register_js')) {
	function lollum_register_js() {
		if (!is_admin()) {
			global $wp_scripts, $wp_version;
			if ($wp_version >= '3.6') {
				wp_deregister_script('mediaelement');
				wp_deregister_script('wp-mediaelement');
			}
			wp_register_script('lolfmk-modernizr', LOLLUM_URI . '/js/modernizr.js', array(), '1.0', 0);
			wp_register_script('lotusflower-plugins', LOLLUM_URI . '/js/lotusflower-plugins.js', array('jquery'), '1.0', 1);
			wp_register_script('mediaelement', LOLLUMCORE_URI . 'mediaelement/mediaelement-and-player.min.js', array('jquery'), '2.13.0', 1);
			wp_register_script('wp-mediaelement', LOLLUMCORE_URI . 'mediaelement/wp-mediaelement.js', array('mediaelement'), '1.0', 1);
			wp_register_script('lotusflower-custom', LOLLUM_URI . '/js/lotusflower-custom.js', array('jquery', 'lotusflower-plugins'), '1.0', 1);
			wp_register_script('lolfmk-progress', LOLLUM_URI . '/js/progress-circle.js', array('jquery', 'lotusflower-plugins'), '1.0', 1);

			wp_localize_script( 'lolfmk-progress', 'lolfmk_progress_vars', 
				array(
					'barColor' => get_option('lol_primary_color')
				)
			);
			
			wp_enqueue_script('lolfmk-modernizr');
			wp_enqueue_script('lotusflower-plugins');
			wp_enqueue_script('lotusflower-custom');
		}
		if (is_singular() && comments_open() && get_option('thread_comments') && !is_page()) {
			wp_register_script('lollum-comment-reply', LOLLUM_URI . '/js/comment-reply.min.js', '1.0', 1);
			wp_enqueue_script('lollum-comment-reply');
		}
	}
}