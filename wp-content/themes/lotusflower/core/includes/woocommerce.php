<?php
/**
 * Lollum
 * 
 * WooCommerce functions and definitions
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

/**
 * Disable and register WooCommerce styles
 */

define('WOOCOMMERCE_USE_CSS', false);

if (!function_exists('lollum_woocommerce_disable_css')) {
	function lollum_woocommerce_disable_css() {
		update_option('woocommerce_frontend_css', 0);
	}
}
add_action('after_setup_theme', 'lollum_woocommerce_disable_css');

if (!function_exists('lol_woocommerce_register_css')) {
	function lol_woocommerce_register_css() {
		wp_register_style('woocommerce-css', LOLLUM_URI . '/woocommerce/css/woocommerce.css', '1.0');
		wp_enqueue_style('woocommerce-css');
	}
}
add_action('wp_enqueue_scripts', 'lol_woocommerce_register_css');


/**
 * Disable and register WooCommerce scripts.
 */

if (!function_exists('lol_woocommerce_register_js')) {
	function lol_woocommerce_register_js() {
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'wc-cart' );
		wp_dequeue_script( 'woocommerce' );
		wp_enqueue_script( 'wc-add-to-cart', LOLLUM_URI . '/woocommerce/js/add-to-cart.min.js' , array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'wc-cart', LOLLUM_URI . '/woocommerce/js/cart.min.js' , array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'woocommerce', LOLLUM_URI . '/woocommerce/js/woocommerce.min.js' , array( 'jquery', 'jquery-blockui', 'jquery-placeholder' ), '1.0', true );
		wp_register_script('lolfmk-checkout', LOLLUM_URI . '/woocommerce/js/lollum-checkout.js', array('jquery'), '1.0', 1);
		wp_register_script('lolfmk-my-account', LOLLUM_URI . '/woocommerce/js/lollum-my-account.js', array('jquery'), '1.0', 1);
	}
}
add_action('wp_enqueue_scripts', 'lol_woocommerce_register_js');


/**
 * Remove theme wrappers
 */

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);


/**
 * Reposition breadcrumb
 */

if (!function_exists('lollum_remove_woo_breadcrumb')) {
	function lollum_remove_woo_breadcrumb() {
		remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
	}
}
add_action('woocommerce_before_main_content', 'lollum_remove_woo_breadcrumb');

if (!function_exists('lollum_woo_custom_breadcrumb')) {
	function lollum_woo_custom_breadcrumb() {
		woocommerce_breadcrumb();
	}
}
add_action('show_woo_breadcrumb', 'lollum_woo_custom_breadcrumb');


/**
 * Change woocommerce breadcrumb output
 */

add_filter('woocommerce_breadcrumb_defaults', 'lollum_woo_breadcrumb_filter');
if (!function_exists('lollum_woo_breadcrumb_filter')) {
	function lollum_woo_breadcrumb_filter() {
		return array(
		'delimiter'   => ' / ',
		'wrap_before' => '<nav class="crumbs"><span>'.__('You are here:', 'lollum').'</span>',
		'wrap_after'  => '</nav>',
		'before'      => '',
		'after'       => '',
		'home'        => _x('Home', 'breadcrumb', 'woocommerce' ),
		);
	}
}

/**
 * Change number of products per page
 */

add_filter('loop_shop_per_page', create_function( '$cols', 'return 9;' ), 20);


/**
 * Change order actions woocommerce_before_shop_loop
 */

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 20 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 30 );


/**
 * Add default thumbnails dimensions to woocommerce
 */

if (!function_exists('lollum_woocommerce_default_thumbs')) {
	function lollum_woocommerce_default_thumbs() {
		update_option('shop_catalog_image_size', array('width' => 720, 'height' => 720, 'crop' => true));
		update_option('shop_single_image_size', array('width' => 720, 'height' => 720, 'crop' => true)); 
		update_option('shop_thumbnail_image_size', array('width' => 150, 'height' => 150, 'crop' => true));
	}
}
add_action('after_setup_theme', 'lollum_woocommerce_default_thumbs');


/**
 * Add register new account text option 
 */

add_filter( 'woocommerce_general_settings', 'lollum_woocommerce_create_account_text' );

if (!function_exists('lollum_woocommerce_create_account_text')) {
	function lollum_woocommerce_create_account_text( $settings ) {
		$updated_settings = array();

		foreach ( $settings as $section ) {
			if ( isset( $section['id'] ) && 'checkout_account_options' == $section['id'] && isset( $section['type'] ) && 'sectionend' == $section['type'] ) {

				$updated_settings[] = array(
					'name'     => __( 'Create Account Text', 'lollum' ),
					'id'       => 'woocommerce_lollum_create_account_text',
					'type'     => 'textarea',
					'css'      => 'min-width:300px;min-height:150px;',
					'std'      => '',  // WC < 2.0
					'default'  => '',  // WC >= 2.0
				);
			}

			$updated_settings[] = $section;

		}

		return $updated_settings;
	}
}


/**
 * Add shop sidebar option
 */

add_filter( 'woocommerce_general_settings', 'lollum_woocommerce_add_sidebar' );

if (!function_exists('lollum_woocommerce_add_sidebar')) {
	function lollum_woocommerce_add_sidebar( $settings ) {
		$updated_settings = array();

		foreach ( $settings as $section ) {
			if ( isset( $section['id'] ) && 'general_options' == $section['id'] && isset( $section['type'] ) && 'sectionend' == $section['type'] ) {

				$updated_settings[] = array(
					'name'     => __( 'Global Products Sidebar', 'lollum' ),
					'id'       => 'woocommerce_lollum_search_sidebar',
					'desc_tip' => __( 'With this option you can select your global layout when your users search your shop site. This is not an option to create a shop page with sidebar, see the documentation for that.', 'lollum' ),
					'type'     => 'select',
					'options' => array(
						'left'        => __( 'Left Sidebar', 'lollum' ),
						'right'       => __( 'Right Sidebar', 'lollum' ),
						'full'  => __( 'Full Width layout', 'lollum' )
					),
					'css'     => 'min-width:150px;',
					'std'      => 'left',  // WC < 2.0
					'default'  => 'left',  // WC >= 2.0
					'desc'     => __( 'Select your global shop layout when your users search your shop site', 'lollum' ),
				);
			}

			$updated_settings[] = $section;

		}

		return $updated_settings;
	}
}


/**
 * Remove stars in loop
 */

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);


/**
 * Update header cart
 */

if (!function_exists('lollum_header_add_to_cart_fragment')) {
	function lollum_header_add_to_cart_fragment($fragments) {
		global $woocommerce;
		
		ob_start();
		
		?>

		<div id="lol-mini-cart">
			<div id="header-cart">
				<div id="header-cart-inner">
					<div class="cart-title"><?php _e('Cart', 'lollum'); ?></div>
					<div class="cart-contents">
						<?php echo $woocommerce->cart->get_cart_total(); ?> / <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'lollum'), $woocommerce->cart->cart_contents_count);?>
					</div>
				</div>
			</div>
			<div class="cart-items">
				<div class="cart-items-inner">
					<?php if ( sizeof($woocommerce->cart->cart_contents)>0 ) { ?>
						<?php foreach ($woocommerce->cart->cart_contents as $cart_item_key => $cart_item) { ?>
							<?php
							$bag_product = $cart_item['data']; 
							$product_title = $bag_product->get_title();
							?>
							<?php if ($bag_product->exists() && $cart_item['quantity']>0) { ?>
								<div class="cart-product">   	
									<figure><a class="cart-product-img" href="<?php echo get_permalink($cart_item['product_id']); ?>"><?php echo $bag_product->get_image(); ?></a></figure>                   
									<div class="cart-product-details">
										<div class="cart-product-title">
											<a href="<?php echo get_permalink($cart_item['product_id']); ?>"><?php echo apply_filters('woocommerce_cart_widget_product_title', $product_title, $bag_product); ?></a>
										</div>
										<div class="cart-product-quantity"><?php _e('Quantity:', 'lollum'); ?> <?php echo $cart_item['quantity']; ?></div>
										<div class="cart-product-price"><?php echo woocommerce_price($bag_product->get_price()); ?></div>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
						<div class="cart-buttons">
							<a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" class="lol-button small view-cart"><?php _e('View shopping bag', 'lollum'); ?></a>
							<a href="<?php echo esc_url($woocommerce->cart->get_checkout_url()); ?>" class="lol-button small"><?php _e('Proceed to checkout', 'lollum'); ?></a>
						</div>
					<?php } else { ?>
						<p><?php _e('No products in the shopping bag.', 'lollum'); ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php
		
		$fragments['#lol-mini-cart'] = ob_get_clean();
		
		return $fragments;
		
	}
}
add_filter('add_to_cart_fragments', 'lollum_header_add_to_cart_fragment');


/**
 * Modify default shortcodes
 */

if (!function_exists('lollum_custom_woo_shortcodes')) {
	function lollum_custom_woo_shortcodes() {
		remove_shortcode('product_category');
		remove_shortcode('product_categories');
		remove_shortcode('recent_products');
		remove_shortcode('products');
		remove_shortcode('product');
		remove_shortcode('sale_products');
		remove_shortcode('best_selling_products');
		remove_shortcode('top_rated_products');
		remove_shortcode('product_attribute');
		remove_shortcode('featured_products');
		remove_shortcode('related_products');
		remove_shortcode('add_to_cart');
		add_shortcode('product_category', 'lollum_custom_product_category');
		add_shortcode('product_categories', 'lollum_custom_product_categories');
		add_shortcode('recent_products', 'lollum_custom_recent_products');
		add_shortcode('products', 'lollum_custom_products');
		add_shortcode('product', 'lollum_custom_product');
		add_shortcode('sale_products', 'lollum_custom_sale_products');
		add_shortcode('best_selling_products', 'lollum_custom_best_selling_products');
		add_shortcode('top_rated_products', 'lollum_custom_top_rated_products');
		add_shortcode('product_attribute', 'lollum_custom_product_attribute');
		add_shortcode('featured_products', 'lollum_custom_featured_products');
		add_shortcode('related_products', 'lollum_custom_related_products');
		add_shortcode('add_to_cart', 'lollum_product_add_to_cart');
	}
}
add_action('init', 'lollum_custom_woo_shortcodes');


// Product category shortcode

if (!function_exists('lollum_custom_product_category')) {
	function lollum_custom_product_category($atts) {
		global $woocommerce, $woocommerce_loop;

	  	if ( empty( $atts ) ) return;

		extract( shortcode_atts( array(
			'per_page' 		=> '12',
			'columns' 		=> '4',
		  	'orderby'   	=> 'title',
		  	'order'     	=> 'desc',
		  	'category'		=> ''
			), $atts ) );

		if ( ! $category ) return;

		// Default ordering args
		$ordering_args = $woocommerce->query->get_catalog_ordering_args( $orderby, $order );

	  	$args = array(
			'post_type'				=> 'product',
			'post_status' 			=> 'publish',
			'ignore_sticky_posts'	=> 1,
			'orderby' 				=> $ordering_args['orderby'],
			'order' 				=> $ordering_args['order'],
			'posts_per_page' 		=> $per_page,
			'meta_query' 			=> array(
				array(
					'key' 			=> '_visibility',
					'value' 		=> array('catalog', 'visible'),
					'compare' 		=> 'IN'
				)
			),
			'tax_query' 			=> array(
		    	array(
			    	'taxonomy' 		=> 'product_cat',
					'terms' 		=> array( esc_attr($category) ),
					'field' 		=> 'slug',
					'operator' 		=> 'IN'
				)
		    )
		);

		if ( isset( $ordering_args['meta_key'] ) ) {
	 		$args['meta_key'] = $ordering_args['meta_key'];
	 	}

	  	ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

				<?php
				$count = 0;
				$col = 3;
				$col_mod = 4;
				switch ($columns) {
					case '2':
						$col_mod = 2;
						$col = 6;
						break;
					case '3':
						$col_mod = 3;
						$col = 4;
						break;
					case '6':
						$col_mod = 6;
						$col = 2;
						break;
					default:
						$col_mod = 4;
						$col = 3;
						break;
				}
				?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Product categories shortcode

if (!function_exists('lollum_custom_product_categories')) {
	function lollum_custom_product_categories($atts) {
		global $woocommerce_loop;

		extract( shortcode_atts( array (
			'number'     => null,
			'orderby'    => 'name',
			'order'      => 'ASC',
			'columns' 	 => '4',
			'hide_empty' => 1,
			'parent'     => ''
			), $atts ) );

		if ( isset( $atts[ 'ids' ] ) ) {
			$ids = explode( ',', $atts[ 'ids' ] );
		  	$ids = array_map( 'trim', $ids );
		} else {
			$ids = array();
		}

		$hide_empty = ( $hide_empty == true || $hide_empty == 1 ) ? 1 : 0;

		// get terms and workaround WP bug with parents/pad counts
		$args = array(
			'orderby'    => $orderby,
			'order'      => $order,
			'hide_empty' => $hide_empty,
			'include'    => $ids,
			'pad_counts' => true,
			'child_of'   => $parent
		);

		$product_categories = get_terms( 'product_cat', $args );

		if ( $parent !== "" )
			$product_categories = wp_list_filter( $product_categories, array( 'parent' => $parent ) );

		if ( $number )
			$product_categories = array_slice( $product_categories, 0, $number );

		ob_start();

		if ( $product_categories ) {

			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}

			woocommerce_product_loop_start();

			foreach ( $product_categories as $category ) {

				$open = !($count%$col_mod) ? '<div class="row">' : '';
				$close = !($count%$col_mod) && $count ? '</div>' : '';
				echo $close.$open; ?>

				<div class="product-category col-<?php echo $col; ?>">

					<?php do_action( 'woocommerce_before_subcategory', $category ); ?>

					<a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>">

						<?php
							/**
							 * woocommerce_before_subcategory_title hook
							 *
							 * @hooked woocommerce_subcategory_thumbnail - 10
							 */
							do_action( 'woocommerce_before_subcategory_title', $category );
						?>

						<h3>
							<?php
								echo $category->name;

								if ( $category->count > 0 )
									echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">(' . $category->count . ')</mark>', $category );
							?>
						</h3>

						<?php
							/**
							 * woocommerce_after_subcategory_title hook
							 */
							do_action( 'woocommerce_after_subcategory_title', $category );
						?>

					</a>

					<?php do_action( 'woocommerce_after_subcategory', $category ); ?>

				</div>

				<?php $count++;

			}

			echo $count ? '</div>' : '';

			woocommerce_product_loop_end();

		}

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Recent products shortcode

if (!function_exists('lollum_custom_recent_products')) {
	function lollum_custom_recent_products($atts) {
		global $woocommerce_loop, $woocommerce;

		extract(shortcode_atts(array(
			'per_page' 	=> '12',
			'columns' 	=> '4',
			'orderby' => 'date',
			'order' => 'desc'
		), $atts));

		$meta_query = $woocommerce->query->get_meta_query();

		$args = array(
			'post_type'	=> 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'	=> 1,
			'posts_per_page' => $per_page,
			'orderby' => $orderby,
			'order' => $order,
			'meta_query' => $meta_query
		);

		ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Products shortcode

if (!function_exists('lollum_custom_products')) {
	function lollum_custom_products( $atts ) {
		global $woocommerce_loop;

	  	if (empty($atts)) return;

		extract(shortcode_atts(array(
			'columns' 	=> '4',
		  	'orderby'   => 'title',
		  	'order'     => 'asc'
			), $atts));

	  	$args = array(
			'post_type'	=> 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'	=> 1,
			'orderby' => $orderby,
			'order' => $order,
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' 		=> '_visibility',
					'value' 	=> array('catalog', 'visible'),
					'compare' 	=> 'IN'
				)
			)
		);

		if(isset($atts['skus'])){
			$skus = explode(',', $atts['skus']);
		  	$skus = array_map('trim', $skus);
	    	$args['meta_query'][] = array(
	      		'key' 		=> '_sku',
	      		'value' 	=> $skus,
	      		'compare' 	=> 'IN'
	    	);
	  	}

		if(isset($atts['ids'])){
			$ids = explode(',', $atts['ids']);
		  	$ids = array_map('trim', $ids);
	    	$args['post__in'] = $ids;
		}

	  	ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Products shortcode

if (!function_exists('lollum_custom_product')) {
	function lollum_custom_product( $atts ) {
	  	if (empty($atts)) return;

	  	$args = array(
	    	'post_type' => 'product',
	    	'posts_per_page' => 1,
	    	'no_found_rows' => 1,
	    	'post_status' => 'publish',
	    	'meta_query' => array(
				array(
					'key' => '_visibility',
					'value' => array('catalog', 'visible'),
					'compare' => 'IN'
				)
			)
	  	);

	  	if(isset($atts['sku'])){
	    	$args['meta_query'][] = array(
	      		'key' => '_sku',
	      		'value' => $atts['sku'],
	      		'compare' => '='
	    	);
	  	}

	  	if(isset($atts['id'])){
	    	$args['p'] = $atts['id'];
	  	}

	  	ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

				<div class="row">

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<div class="product-item col-3">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

				<?php endwhile; // end of the loop. ?>

				</div>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Sale Products shortcode

if (!function_exists('lollum_custom_sale_products')) {
	function lollum_custom_sale_products( $atts ){
		global $woocommerce_loop, $woocommerce;

		extract( shortcode_atts( array(
		  'per_page'      => '12',
		  'columns'       => '4',
		  'orderby'       => 'title',
		  'order'         => 'asc'
		  ), $atts ) );

		// Get products on sale
		$product_ids_on_sale = woocommerce_get_product_ids_on_sale();

		$meta_query = array();
		$meta_query[] = $woocommerce->query->visibility_meta_query();
		$meta_query[] = $woocommerce->query->stock_status_meta_query();
		$meta_query   = array_filter( $meta_query );

		$args = array(
			'posts_per_page'=> $per_page,
			'orderby' 		=> $orderby,
	        'order' 		=> $order,
			'no_found_rows' => 1,
			'post_status' 	=> 'publish',
			'post_type' 	=> 'product',
			'meta_query' 	=> $meta_query,
			'post__in'		=> $product_ids_on_sale
		);

	  	ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Best Selling Products shortcode

if (!function_exists('lollum_custom_best_selling_products')) {
	function lollum_custom_best_selling_products( $atts ){
		global $woocommerce_loop;

		extract( shortcode_atts( array(
			'per_page'      => '12',
			'columns'       => '4'
		), $atts ) );

		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'   => 1,
			'posts_per_page' => $per_page,
			'meta_key' 		 => 'total_sales',
			'orderby' 		 => 'meta_value_num',
			'meta_query' => array(
			   array(
					'key' => '_visibility',
					'value' => array( 'catalog', 'visible' ),
					'compare' => 'IN'
			   )
			)
		);

	  	ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Top Rated Products shortcode

if (!function_exists('lollum_custom_top_rated_products')) {
	function lollum_custom_top_rated_products( $atts ){
		global $woocommerce_loop;

		extract( shortcode_atts( array(
			'per_page'      => '12',
			'columns'       => '4',
			'orderby'       => 'title',
			'order'         => 'asc'
		), $atts ) );

		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'   => 1,
			'orderby' => $orderby,
			'order' => $order,
			'posts_per_page' => $per_page,
			'meta_query' => array(
			   array(
			       'key' => '_visibility',
			       'value' => array('catalog', 'visible'),
			       'compare' => 'IN'
			   )
			)
		);

	  	ob_start();

	  	// $aa = new WC_Shortcodes ();

	  	add_filter( 'posts_clauses', 'lollum_order_by_rating_post_clauses' );

		$products = new WP_Query( $args );

		remove_filter( 'posts_clauses', 'lollum_order_by_rating_post_clauses' );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}

if (!function_exists('lollum_order_by_rating_post_clauses')) {
	function lollum_order_by_rating_post_clauses( $args ) {

		global $wpdb;

		$args['where'] .= " AND $wpdb->commentmeta.meta_key = 'rating' ";

		$args['join'] .= "
			LEFT JOIN $wpdb->comments ON($wpdb->posts.ID = $wpdb->comments.comment_post_ID)
			LEFT JOIN $wpdb->commentmeta ON($wpdb->comments.comment_ID = $wpdb->commentmeta.comment_id)
		";

		$args['orderby'] = "$wpdb->commentmeta.meta_value DESC";

		$args['groupby'] = "$wpdb->posts.ID";

		return $args;
	}
}


// Featured Products shortcode

if (!function_exists('lollum_custom_featured_products')) {
	function lollum_custom_featured_products( $atts ) {

		global $woocommerce_loop;

		extract(shortcode_atts(array(
			'per_page' 	=> '12',
			'columns' 	=> '4',
			'orderby' => 'date',
			'order' => 'desc'
		), $atts));

		$args = array(
			'post_type'	=> 'product',
			'post_status' => 'publish',
			'ignore_sticky_posts'	=> 1,
			'posts_per_page' => $per_page,
			'orderby' => $orderby,
			'order' => $order,
			'meta_query' => array(
				array(
					'key' => '_visibility',
					'value' => array('catalog', 'visible'),
					'compare' => 'IN'
				),
				array(
					'key' => '_featured',
					'value' => 'yes'
				)
			)
		);

		ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Product Attribute shortcode

if (!function_exists('lollum_custom_product_attribute')) {
	function lollum_custom_product_attribute( $atts ) {
		global $woocommerce_loop;

		extract( shortcode_atts( array(
			'per_page'  => '12',
			'columns'   => '4',
			'orderby'   => 'title',
			'order'     => 'asc',
			'attribute' => '',
		  	'filter'    => ''
		), $atts ) );

		$attribute 	= strstr( $attribute, 'pa_' ) ? sanitize_title( $attribute ) : 'pa_' . sanitize_title( $attribute );
		$filter 	= sanitize_title( $filter );

		$args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $per_page,
			'orderby'             => $orderby,
			'order'               => $order,
			'meta_query'          => array(
				array(
					'key'               => '_visibility',
					'value'             => array('catalog', 'visible'),
					'compare'           => 'IN'
				)
			),
			'tax_query' 			=> array(
				array(
					'taxonomy' 	=> $attribute,
					'terms' 	=> $filter,
					'field' 	=> 'slug'
				)
			)
		);

		ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php while ( $products->have_posts() ) : $products->the_post(); ?>

					<?php
					$open = !($count%$col_mod) ? '<div class="row">' : '';
					$close = !($count%$col_mod) && $count ? '</div>' : '';
					echo $close.$open;
					?>

					<div class="product-item col-<?php echo $col; ?>">

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					</div>

					<?php $count++; ?>

				<?php endwhile; // end of the loop. ?>

				<?php echo $count ? '</div>' : ''; ?>

			<?php woocommerce_product_loop_end(); ?>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Related Products shortcode

if (!function_exists('lollum_custom_related_products')) {
	function lollum_custom_related_products($atts) {
		global $product;
		extract( shortcode_atts( array(
			'per_page' 	=> '4',
			'columns' 	=> '4',
			'orderby' => 'rand',
		), $atts));

		$related = $product->get_related();

		if ( sizeof( $related ) == 0 ) return;

		$args = apply_filters('woocommerce_related_products_args', array(
			'post_type'				=> 'product',
			'ignore_sticky_posts'	=> 1,
			'no_found_rows' 		=> 1,
			'posts_per_page' 		=> $per_page,
			'orderby' 				=> $orderby,
			'post__in' 				=> $related,
			'post__not_in'			=> array($product->id)
		) );

		ob_start();

		$products = new WP_Query( $args );

		if ( $products->have_posts() ) : ?>

			<?php
			$count = 0;

			$col = 3;
			$col_mod = 4;
			switch ($columns) {
				case '2':
					$col_mod = 2;
					$col = 6;
					break;
				case '3':
					$col_mod = 3;
					$col = 4;
					break;
				case '6':
					$col_mod = 6;
					$col = 2;
					break;
				default:
					$col_mod = 4;
					$col = 3;
					break;
			}
			?>

			<div class="related products">
				
				<div class="divider">
					<h2><?php _e( 'Related Products', 'woocommerce' ); ?></h2>
				</div>

				<?php woocommerce_product_loop_start(); ?>

					<?php while ( $products->have_posts() ) : $products->the_post(); ?>

						<?php
						$open = !($count%$col_mod) ? '<div class="row">' : '';
						$close = !($count%$col_mod) && $count ? '</div>' : '';
						echo $close.$open;
						?>

						<div class="product-item col-<?php echo $col; ?>">

							<?php woocommerce_get_template_part( 'content', 'product' ); ?>

						</div>

						<?php $count++; ?>

					<?php endwhile; // end of the loop. ?>

					<?php echo $count ? '</div>' : ''; ?>

				<?php woocommerce_product_loop_end(); ?>

			</div>

		<?php endif;

		wp_reset_postdata();

		return '<div class="woocommerce shortcode-row">' . ob_get_clean() . '</div>';
	}
}


// Add to Cart shortcode

if (!function_exists('lollum_product_add_to_cart')) {
	function lollum_product_add_to_cart( $atts ) {
	  	global $wpdb, $woocommerce;

	  	if ( empty( $atts ) ) return;

	  	if ( ! isset( $atts['style'] ) ) $atts['style'] = 'border:4px solid #f5f5f5; padding: 12px;';

	  	if ( isset( $atts['id'] ) ) {
	  		$product_data = get_post( $atts['id'] );
		} elseif ( isset( $atts['sku'] ) ) {
			$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $atts['sku'] ) );
			$product_data = get_post( $product_id );
		} else {
			return;
		}

		if ( 'product' == $product_data->post_type ) {

			$product = $woocommerce->setup_product_data( $product_data );

			ob_start();
			?>
			<p class="product woocommerce add-to-cart-shortcode" style="<?php echo $atts['style']; ?>">

				<?php echo $product->get_price_html(); ?>

				<?php woocommerce_template_loop_add_to_cart(); ?>

			</p><?php

			wp_reset_postdata();

			return ob_get_clean();

		} elseif ( 'product_variation' == $product_data->post_type ) {

			$product = get_product( $product_data->post_parent );

			$GLOBALS['product'] = $product;

			$variation = get_product( $product_data );

			ob_start();
			?>
			<p class="product product-variation add-to-cart-shortcode" style="<?php echo $atts['style']; ?>">

				<?php echo $product->get_price_html(); ?>

				<?php

				$link 	= $product->add_to_cart_url();

				$label 	= apply_filters('add_to_cart_text', __( 'Add to cart', 'woocommerce' ));

				$link = add_query_arg( 'variation_id', $variation->variation_id, $link );

				foreach ($variation->variation_data as $key => $data) {
					if ($data) $link = add_query_arg( $key, $data, $link );
				}

				printf('<a href="%s" rel="nofollow" data-product_id="%s" class="button add_to_cart_button product_type_%s">%s</a>', esc_url( $link ), $product->id, $product->product_type, $label);

				?>

			</p><?php

			wp_reset_postdata();

			return ob_get_clean();

		}
	}
}

/**
 * Register new custom WC widgets
 */

add_action( 'widgets_init', 'lollum_override_woocommerce_widgets', 15 );
 
if (!function_exists('lollum_override_woocommerce_widgets')) {
	function lollum_override_woocommerce_widgets() {
	 
		if ( class_exists( 'WC_Widget_Best_Sellers' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-best-sellers.php' );
			register_widget( 'Lollum_WC_Widget_Best_Sellers' );
		}
		if ( class_exists( 'WC_Widget_Featured_Products' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-featured-products.php' );
			register_widget( 'Lollum_WC_Widget_Featured_Products' );
		}
		if ( class_exists( 'WC_Widget_Onsale' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-onsale.php' );
			register_widget( 'Lollum_WC_Widget_Onsale' );
		}
		if ( class_exists( 'WC_Widget_Random_Products' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-random-products.php' );
			register_widget( 'Lollum_WC_Widget_Random_Products' );
		}
		if ( class_exists( 'WC_Widget_Recently_Viewed' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-recently-viewed.php' );
			register_widget( 'Lollum_WC_Widget_Recently_Viewed' );
		}
		if ( class_exists( 'WC_Widget_Recent_Products' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-recent-products.php' );
			register_widget( 'Lollum_WC_Widget_Recent_Products' );
		}
		if ( class_exists( 'WC_Widget_Top_Rated_Products' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-top-rated-products.php' );
			register_widget( 'Lollum_WC_Widget_Top_Rated_Products' );
		}
		if ( class_exists( 'WC_Widget_Recent_Reviews' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-recent-reviews.php' );
			register_widget( 'Lollum_WC_Widget_Recent_Reviews' );
		}
		if ( class_exists( 'WC_Widget_Price_Filter' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-price-filter.php' );
			register_widget( 'Lollum_WC_Widget_Price_Filter' );
		}
		if ( class_exists( 'WC_Widget_Layered_Nav' ) ) {
			include_once( LOLLUM_PATH . '/woocommerce/widgets/widget-layered-nav.php' );
			register_widget( 'Lollum_WC_Widget_Layered_Nav' );
		}

	}
}

/**
 * Filter tag cloud
 */

if (!function_exists('lollum_product_tag_cloud_args')) {
	function lollum_product_tag_cloud_args($args) {
		$args['largest'] = 11;
		$args['smallest'] = 11;
		$args['unit'] = 'px';
		return $args;
	}
}
add_filter('woocommerce_product_tag_cloud_widget_args', 'lollum_product_tag_cloud_args');