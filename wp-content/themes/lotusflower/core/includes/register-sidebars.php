<?php
/**
 * Lollum
 * 
 * Register widgetized area
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lollum_widgets_init')) {
	function lollum_widgets_init() {
		register_sidebar(array(
			'name' => __('Main Sidebar', 'lollum'),
			'id' => 'main-sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => "</aside>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		register_sidebar(array(
			'name' => __('Page Sidebar', 'lollum'),
			'id' => 'page-sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => "</aside>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		if (lollum_check_is_woocommerce()) {
			register_sidebar(array(
				'name' => __('Shop Sidebar', 'lollum'),
				'id' => 'shop-sidebar',
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget' => "</aside>",
				'before_title' => '<div class="widget-header"><h3 class="widget-title">',
				'after_title' => '</h3></div>',
			));
		}
		if (lollum_check_is_lollumframework()) {
			register_sidebar(array(
				'name' => __('Job Sidebar', 'lollum'),
				'id' => 'job-sidebar',
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget' => "</aside>",
				'before_title' => '<div class="widget-header"><h3 class="widget-title">',
				'after_title' => '</h3></div>',
			));
		}
		register_sidebar(array(
			'name' => __('Footer 1', 'lollum'),
			'id' => 'footer1',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => "</section>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		register_sidebar(array(
			'name' => __('Footer 2', 'lollum'),
			'id' => 'footer2',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => "</section>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		register_sidebar(array(
			'name' => __('Footer 3', 'lollum'),
			'id' => 'footer3',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => "</section>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		register_sidebar(array(
			'name' => __('Footer 4', 'lollum'),
			'id' => 'footer4',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => "</section>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		register_sidebar(array(
			'name' => __('Top Footer 1', 'lollum'),
			'id' => 'top-footer1',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => "</section>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		register_sidebar(array(
			'name' => __('Top Footer 2', 'lollum'),
			'id' => 'top-footer2',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => "</section>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
		register_sidebar(array(
			'name' => __('Top Footer 3', 'lollum'),
			'id' => 'top-footer3',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => "</section>",
			'before_title' => '<div class="widget-header"><h3 class="widget-title">',
			'after_title' => '</h3></div>',
		));
	}
}
add_action('init', 'lollum_widgets_init');