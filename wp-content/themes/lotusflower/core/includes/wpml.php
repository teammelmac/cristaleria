<?php
/**
 * Lollum
 * 
 * WPML functions and definitions
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

/**
 * Register WPML style
 */

if (!function_exists('lollum_register_wpml_css')) {
	function lollum_register_wpml_css() {
		if (!is_admin()) {
			wp_register_style('wpml-css', LOLLUM_URI . '/css/wpml-styles.css', '1.0');
			wp_enqueue_style('wpml-css');
		}
	}
}
add_action('wp_enqueue_scripts', 'lollum_register_wpml_css');

/**
 * Add a custom language switcher in #top-header
 */

if (!function_exists('lollum_languages_list_header')) {
	function lollum_languages_list_header(){
		$languages = icl_get_languages('skip_missing=0&orderby=code');
		if(!empty($languages)){
			echo '<div id="lol_header_language_list"><ul>';
			foreach($languages as $l){
				echo '<li>';
				if(!$l['active']) echo '<a href="'.$l['url'].'">';
				echo icl_disp_language($l['language_code'], 0);
				if(!$l['active']) echo '</a>';
				echo '</li>';
			}
			echo '</ul></div>';
		}
	}
}


/**
 * Add a custom language switcher in #footer-bottom
 */

if (!function_exists('lollum_languages_list_footer')) {
	function lollum_languages_list_footer(){
		$languages = icl_get_languages('skip_missing=0&orderby=code');
		if(!empty($languages)){
			echo '<div id="lol_footer_language_list"><ul>';
			foreach($languages as $l){
				echo '<li>';
				if(!$l['active']) echo '<a href="'.$l['url'].'">';
				echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
				if(!$l['active']) echo '</a>';
				if(!$l['active']) echo '<a href="'.$l['url'].'">';
				echo icl_disp_language($l['native_name'], 0);
				if(!$l['active']) echo '</a>';
				echo '</li>';
			}
			echo '</ul></div>';
		}
	}
}