<?php
/**
 * Lollum
 * 
 * WooCommerce custom styles function
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

/**
 * WooCommerce custom styles
 */

add_action('wp_head', 'lollum_print_woocommerce_custom_styles');
if (!function_exists('lollum_print_woocommerce_custom_styles')) {
	function lollum_print_woocommerce_custom_styles() { ?>

	<style type="text/css">

	.woocommerce-message .button:hover,
	#content .product-item a:hover,
	#content .product-category a:hover,
	#content .type-product .product_meta a:hover,
	#content .type-product .variations .reset_variations:hover,
	.woocommerce .star-rating,
	.woocommerce-page .star-rating,
	#content .cart-empty p .button:hover,
	#content table.shop_table.cart tbody a:hover,
	#content .login .lost_password:hover,
	#content .order-info mark,
	#content #lol-my-downloads .digital-downloads .icon-download-alt,
	#content #lol-my-downloads .digital-downloads a:hover,
	#sidebar .widget_shopping_cart .total .amount,
	#top-footer .widget_shopping_cart .total .amount,
	#sidebar .widget_layered_nav small,
	#top-footer .widget_layered_nav small,
	#sidebar .widget_product_categories .count,
	#top-footer .widget_product_categories .count,
	#footer .widget_shopping_cart .total .amount,
	#footer .widget_layered_nav small,
	#footer .widget_product_categories .count {
		color: <?php echo get_option('lol_primary_color');?>;
	}
	#content .product-item .icon-ok.added,
	#content div.quantity input.plus:hover,
	#content div.quantity input.minus:hover {
		background-color: <?php echo get_option('lol_primary_color');?>;
	}
	#top-footer .entry-product-thumb img {
		border: 1px solid <?php echo get_option('lol_primary_color');?>;
	}
	.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
	.woocommerce-page .widget_price_filter .ui-slider .ui-slider-range {
		background:<?php echo get_option('lol_primary_color');?> url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAADCAYAAABS3WWCAAAAFUlEQVQIHWP4//9/PRMDA8NzEPEMADLLBU76a5idAAAAAElFTkSuQmCC) top repeat-x;
		box-shadow:inset 0 0 0 1px <?php echo get_option('lol_primary_color');?>;
		-webkit-box-shadow:inset 0 0 0 1px <?php echo get_option('lol_primary_color');?>;
		-moz-box-shadow:inset 0 0 0 1px <?php echo get_option('lol_primary_color');?>;
	}
	#content .product-item .button,
	#content .type-product .single_add_to_cart_button,
	#content .add-to-cart-shortcode .add_to_cart_button,
	#sidebar .widget_product_tag_cloud a,
	#top-footer .widget_product_tag_cloud a,
	#footer .widget_product_tag_cloud a {
		color: <?php echo get_option('lol_primary_btn_txt');?>;
		background-color: <?php echo get_option('lol_primary_btn_bg');?>;
	}
	#content .product-item .button:hover,
	#content .type-product .single_add_to_cart_button:hover,
	#content .add-to-cart-shortcode .add_to_cart_button:hover,
	#sidebar .widget_product_tag_cloud a:hover,
	#top-footer .widget_product_tag_cloud a:hover,
	#footer .widget_product_tag_cloud a:hover {
		color: <?php echo get_option('lol_primary_btn_txt_h');?>;
		background-color: <?php echo get_option('lol_primary_btn_bg_h');?>;
	}
	.woocommerce-message,
	.woocommerce-message .button,
	#content .product-category h3,
	#content .type-product .variations .reset_variations,
	#content .type-product #tab-additional_information th,
	#content .type-product .woocommerce-tabs .commentlist .meta,
	#content .cart-empty p .button,
	#content table.shop_table.cart tbody a,
	#content .login .lost_password,
	#content #lol-my-downloads .digital-downloads a {
		color: <?php echo get_option('lol_body_text_color');?>;
	}
	#content .product-item .price,
	#content .type-product .entry-summary .stock,
	#content .type-product .woocommerce-tabs .tabs li a:hover,
	#content .type-product .woocommerce-tabs .tabs li.active a,
	#content .type-product .woocommerce-tabs .commentlist .meta strong,
	#content .cart-empty p,
	#content #checkout-process a:hover,
	#content #checkout-process .active a,
	#content #checkout-step .already-logged-message,
	#content .p-alt,
	#content .order-info,
	#sidebar .entry-product-meta .price,
	#top-footer .entry-product-meta .price {
		color: <?php echo get_option('lol_primary_text_color');?>;
	}
	#icon-cart-menu,
	#content .product-category a,
	#content .type-product .woocommerce-tabs .panel h2,
	#content .cart-empty .icon-suitcase,
	#content .lol-thankyou-message h2 {
		color: <?php echo get_option('lol_secondary_text_color');?>;
	}
	#content .type-product .product_meta,
	#content .type-product .product_meta a,
	#content .type-product .woocommerce-tabs .tabs li,
	#content .type-product .woocommerce-tabs .tabs li a,
	#content table.shop_table.cart .icon-remove:hover,
	#content #checkout-process,
	#content #checkout-process a,
	#sidebar .entry-product-meta .lol-review,
	#top-footer .entry-product-meta .lol-review,
	#footer .entry-product-meta .lol-review {
		color: <?php echo get_option('lol_light_text_color');?>;
	}
	@media (min-width: 992px) {
		#lol-mini-cart .cart-product-title a:hover {
			color: <?php echo get_option('lol_primary_color');?>;
		}
		#lol-mini-cart #header-cart,
		#lol-mini-cart .cart-product-title a,
		#lol-mini-cart .cart-product-price {
			color: <?php echo get_option('lol_primary_text_color');?>;
		}
		#lol-mini-cart .cart-product-quantity {
			color: <?php echo get_option('lol_light_text_color');?>;
		}
	}
	</style>

	<?php
	}
}