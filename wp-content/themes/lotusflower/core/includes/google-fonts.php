<?php
/**
 * Lollum
 * 
 * Register Google web fonts
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

add_action('wp_enqueue_scripts', 'lollum_google_fonts');
if (!function_exists('lollum_google_fonts')) {
	function lollum_google_fonts() {
		if (get_option('lol_check_external_font') == 'true') {
			$font_url = get_option('lol_external_font_url');
			wp_enqueue_style('external-font', $font_url);
		} else {
			global $google_fonts;
			$primary_font = get_option('lol_primary_font');
			if (in_array($primary_font, $google_fonts)) {
				$primary_font_web = true;
				$f_primary = str_replace(" ", "+", $primary_font);  
			}
			if (isset($primary_font_web)) {
				$protocol = is_ssl() ? 'https' : 'http';
				$w1 = get_option('lol_primary_font_weight');
				$w2 = get_option('lol_secondary_font_weight');
				$w3 = get_option('lol_bold_font_weight');
				$w4 = get_option('lol_light_font_weight');
				wp_enqueue_style('f-primary-300', $protocol.'://fonts.googleapis.com/css?family='.$f_primary.':'.$w4.'');
				wp_enqueue_style('f-primary-400', $protocol.'://fonts.googleapis.com/css?family='.$f_primary.':'.$w1.'');
				wp_enqueue_style('f-primary-400i', $protocol.'://fonts.googleapis.com/css?family='.$f_primary.':'.$w1.'italic');
				wp_enqueue_style('f-primary-600', $protocol.'://fonts.googleapis.com/css?family='.$f_primary.':'.$w2.'');
				wp_enqueue_style('f-primary-700', $protocol.'://fonts.googleapis.com/css?family='.$f_primary.':'.$w3.'');
			}
		}
	}
}