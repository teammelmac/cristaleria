<?php
/**
 * Lollum
 * 
 * Register general styles
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

if (!function_exists('lollum_register_css')) {
	function lollum_register_css() {
		if (!is_admin()) {
			global $wp_styles, $wp_version;
			if ($wp_version >= '3.6') {
				wp_deregister_style('mediaelement');
			}
			wp_register_style('grid-css', LOLLUM_URI . '/css/grid.css', '1.0');
			wp_register_style('font-awesome-css', LOLLUM_URI . '/css/font-awesome.min.css', '3.2.1');
			wp_register_style('lotusflower-css', get_stylesheet_uri(), '1.0');
			wp_register_style('ie8-css', LOLLUM_URI . '/css/ie8.css', '1.0');
			wp_register_style('mediaelement', LOLLUMCORE_URI . 'mediaelement/mediaelementplayer.min.css');

			wp_enqueue_style('grid-css');
			wp_enqueue_style('font-awesome-css');
			wp_enqueue_style('lotusflower-css');
			wp_enqueue_style('ie8-css');
			$wp_styles->add_data('ie8-css', 'conditional', 'lt IE 9');
		}
	}
}
add_action('wp_enqueue_scripts', 'lollum_register_css');