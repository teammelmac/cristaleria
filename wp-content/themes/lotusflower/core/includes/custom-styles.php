<?php
/**
 * Lollum
 * 
 * Custom styles function
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

/**
 * Custom styles
 */

add_action('wp_head', 'lollum_print_custom_styles');
if (!function_exists('lollum_print_custom_styles')) {
	function lollum_print_custom_styles() { ?>

	<style type="text/css">

	<?php if (get_option('lol_bg_image_check') == 'true') { // check bg body ?>
		<?php if (get_option('lol_background_img_type') == 'Default image') {
			$default_bg = get_option('lol_default_image_bg');
			$default_bg = strtolower(str_replace(' ', '_', $default_bg)); ?>

			body {
				background-image: url(<?php echo LOLLUMCORE_URI . 'admin/images/backgrounds/' . $default_bg . '.png'; ?>);
				background-repeat: repeat;
			}

		<?php } elseif (get_option('lol_custom_image_bg')) { ?>
			body {
			background-image: url(<?php echo get_option('lol_custom_image_bg');?>);
			background-repeat: <?php echo get_option('lol_custom_image_bg_repeat'); ?>;
			<?php if (get_option('lol_custom_image_bg_attachment') == 'true') { ?>
				background-attachment: fixed;
			<?php } ?>
			<?php if (get_option('lol_custom_image_bg_cover') == 'true') { ?>
				background-size: cover;
			<?php } ?>
			}
		<?php } ?>
	<?php } ?>
	
	body {
		background-color: <?php echo get_option('lol_body_bg_color');?>;
	}
	#wrap,
	#page,
	#main {
		background-color: <?php echo get_option('lol_page_bg_color');?>;
	}
	a,
	#content .entry-header a:hover,
	#content .meta-tags-wrap a:hover,
	#content .entry-meta a:hover,
	#content .entry-meta .meta-sticky,
	#content .meta-wrap .icon-pushpin,
	#content .format-link .entry-header h2,
	#content .format-chat .chat-author,
	#content .post-item .divider-sm h2,
	#content .post-item .divider-sm a:hover,
	#content .lol-item-portfolio-list .entry-meta .project-cats a:hover,
	#content .lol-item-service-column h2 a:hover,
	#content .lol-item-mini-service-column h2 a:hover,
	#content .lol-item-member .meta-member h3,
	#content .lol-item-member .member-links a:hover,
	#content .testimonial-meta span,
	#content #countdown .count-value,
	#content .job-list .entry-job h4 a:hover,
	#content .lol-love-wrap a:hover,
	#content .lol-love-wrap .loved,
	#content .portfolio-item h2 a:hover,
	.single-lolfmk-portfolio .project-meta .project-categories a:hover,
	#sidebar .widget a:hover,
	#top-footer .widget a:hover,
	#sidebar .widget_archive ul,
	#top-footer .widget_archive ul,
	#sidebar .widget_categories ul,
	#top-footer .widget_categories ul,
	#sidebar .widget_calendar table tbody tr td a,
	#sidebar .widget_calendar table tfoot tr td a,
	#top-footer .widget_calendar table tbody tr td a,
	#top-footer .widget_calendar table tfoot tr td a,
	#sidebar .lol-projects-widget .entry-meta .project-cats a:hover,
	#top-footer .lol-projects-widget .entry-meta .project-cats a:hover,
	#sidebar .lol-jobs-widget .entry-job a:hover,
	#top-footer .lol-jobs-widget .entry-job a:hover,
	#content ul.lol-list .icon-ok,
	#content .price-currency,
	#content .price-cost,
	#comments .commentlist .comment-meta a:hover,
	#content .lolfmk-job .job-meta .icon-map-marker,
	#content .lol-item-info .vcard a:hover,
	#content .lol-item-blockquote cite,
	#content .lol-faq-topics a:hover,
	#content .lol-item-mini-service-column .mini-service-icon {
		color: <?php echo get_option('lol_primary_color');?>;
	}
	#content .format-quote .entry-content,
	#content .lol-item-service-column .service-icon,
	#content .lol-item-blog-list .entry-thumbnail.no-thumb,
	#content .lol-item-portfolio-list .entry-thumbnail.no-thumb,
	#sidebar .lol-posts-widget .entry-thumbnail.no-thumb,
	#top-footer .lol-posts-widget .entry-thumbnail.no-thumb,
	#sidebar .lol-projects-widget .entry-thumbnail.no-thumb,
	#top-footer .lol-projects-widget .entry-thumbnail.no-thumb,
	#footer .lol-posts-widget .entry-thumbnail.no-thumb,
	#footer .lol-projects-widget .entry-thumbnail.no-thumb,
	.lol-skill .lol-bar,
	#back-top:hover,
	#content .lol-item-image-text .image-text-mask,
	#content .lol-item-text-banner {
		background-color: <?php echo get_option('lol_primary_color');?>;
	}
	.mejs-controls .mejs-time-rail .mejs-time-loaded {
		background: <?php echo get_option('lol_primary_color');?>!important;
	}
	#content .lol-item-text-banner-wrap {
		border-top: 3px solid <?php echo get_option('lol_primary_color');?>;
		border-bottom: 3px solid <?php echo get_option('lol_primary_color');?>;
	}
	body,
	.customSelect,
	.crumbs,
	.crumbs a,
	#content .divider-sm a,
	#content .lol-item-blog-list .entry-meta a,
	#content .lol-item-portfolio-list .entry-meta a,
	#content .lol-item-service-column h2,
	#content .lol-item-service-column h2 a,
	#content .lol-item-mini-service-column h2,
	#content .lol-item-mini-service-column h2 a,
	#content .lol-item-block-feature .feature-title h2,
	#content .lol-item-member .meta-member h2,
	#content .lol-item-member .member-links a,
	#content .chart span,
	.single-lolfmk-portfolio .projects-navigation,
	#sidebar .widget a,
	#top-footer .widget a,
	#sidebar .divider-wgt a,
	#top-footer .divider-wgt a,
	#sidebar .widget_archive a,
	#top-footer .widget_archive a,
	#sidebar .widget_categories a,
	#top-footer .widget_categories a,
	#sidebar .lol-jobs-widget .entry-job a,
	#top-footer .lol-jobs-widget .entry-job a,
	#comments .commentlist .comment-meta,
	#comments .commentlist .comment-meta a,
	#content .lol-faq-topics a,
	#content .lol-item-info .vcard a,
	#content .lol-item-info .vcard .icon {
		color: <?php echo get_option('lol_body_text_color');?>;
	}
	#top-header,
	#top-header a,
	#content .meta-tags-wrap a,
	#content .social-meta a,
	#content .entry-meta,
	#content .lol-item-heading p,
	#content .lol-item-heading-small p,
	#content .lol-item-blog-list .entry-meta span,
	#content .lol-item-portfolio-list .entry-meta .project-cats,
	#content .lol-item-portfolio-list .entry-meta .project-cats a,
	#content .lol-icon-toggle,
	#content .lol-icon-faq,
	#content .meta-job,
	#content .meta-job-location,
	.single-lolfmk-portfolio .project-meta .project-categories a,
	#sidebar .lol_widget_twitter .timestamp a,
	#top-footer .lol_widget_twitter .timestamp a,
	#sidebar .lol-posts-widget .entry-meta span,
	#top-footer .lol-posts-widget .entry-meta span,
	#sidebar .lol-projects-widget .entry-meta .project-cats,
	#sidebar .lol-projects-widget .entry-meta .project-cats a,
	#top-footer .lol-projects-widget .entry-meta .project-cats,
	#top-footer .lol-projects-widget .entry-meta .project-cats a,
	#sidebar .lol-jobs-widget .entry-job span,
	#top-footer .lol-jobs-widget .entry-job span,
	#content blockquote,
	#content .lol-item-blockquote blockquote {
		color: <?php echo get_option('lol_light_text_color');?>;
	}
	#top-header a:hover,
	#site-title,
	#page-title-wrap .page-title h1,
	#content .entry-header h1,
	#content .entry-header a,
	#content .lol-item-heading h2 ,
	#content .lol-item-heading-small h2 ,
	#content .testimonial-meta cite,
	#content .progress-circle,
	#content .job-list .entry-job h4 a,
	.single-lolfmk-portfolio .project-details span,
	#sidebar .lol_widget_twitter .timestamp a:hover,
	#top-footer .lol_widget_twitter .timestamp a:hover,
	.lol-skill,
	#comments .commentlist .comment-meta .fn,
	#comments .commentlist .comment-meta .fn a {
		color: <?php echo get_option('lol_primary_text_color');?>;
	}
	#content .social-meta a:hover,
	#content .entry-meta a,
	#content .divider h3,
	#content .divider h2,
	.divider h3,
	.divider h2,
	#content .about-author h4,
	#sidebar .widget-header .widget-title,
	#top-footer .widget-header .widget-title,
	#sidebar .widget_rss .widget-title a,
	#top-footer .widget_rss .widget-title a {
		color: <?php echo get_option('lol_secondary_text_color');?>;
	}
	button,
	input[type="submit"],
	.lol-button,
	#content .lol-button,
	#content .more-link,
	#content.attachment-template #image-navigation a,
	#sidebar .widget_tag_cloud a,
	#top-footer .widget_tag_cloud a,
	#footer .widget_tag_cloud a {
		color: <?php echo get_option('lol_primary_btn_txt');?>!important;
		background-color: <?php echo get_option('lol_primary_btn_bg');?>;
	}
	button:hover,
	input[type="submit"]:hover,
	.lol-button:hover,
	#content .lol-button:hover,
	#content .more-link:hover,
	#content.attachment-template #image-navigation a:hover,
	#sidebar .widget_tag_cloud a:hover,
	#top-footer .widget_tag_cloud a:hover,
	#footer .widget_tag_cloud a:hover {
		color: <?php echo get_option('lol_primary_btn_txt_h');?>!important;
		background-color: <?php echo get_option('lol_primary_btn_bg_h');?>;
	}
	#content .pagelink a,
	.single-lolfmk-portfolio .projects-navigation a,
	#content .pagination .current,
	#content .pagination a,
	#comments #comment-nav a {
		color: <?php echo get_option('lol_secondary_btn_txt');?>;
		background-color: <?php echo get_option('lol_secondary_btn_bg');?>;
	}
	#content .pagelink a:hover,
	.single-lolfmk-portfolio .projects-navigation a:hover,
	#content .pagination .current,
	#content .pagination a:hover,
	#comments #comment-nav a:hover {
		color: <?php echo get_option('lol_secondary_btn_txt_h');?>;
		background-color: <?php echo get_option('lol_secondary_btn_bg_h');?>;
	}
	body,
	#content .entry-header h1,
	#content .entry-header a,
	#content .meta-tags-wrap a,
	#content .pagelink a,
	#content .lol-item-heading h2,
	#content .lol-item-heading-small h2,
	#content .lol-item-heading-parallax h2,
	#content .lol-item-block-video h2,
	#content .lol-item-block-banner h2,
	#content .lol-item-portfolio-list .entry-meta .project-cats,
	#content .lol-item-portfolio-list .entry-meta .project-cats a,
	#content .lol-item-member .meta-member h3,
	#content #countdown h2,
	#content .newsletter-title h2,
	.single-lolfmk-portfolio .projects-navigation a,
	.single-lolfmk-portfolio .project-meta .project-categories a,
	#content .pagination .current,
	#content .pagination a,
	#sidebar .lol-jobs-widget .entry-job span,
	#top-footer .lol-jobs-widget .entry-job span,
	#footer .lol-jobs-widget .entry-job span,
	#content .price-currency,
	#comments #comment-nav a,
	#comments .pingback a,
	#content .lol-item-image-text h2,
	#content .lol-item-text-banner h2 {
		font-weight: <?php echo get_option('lol_primary_font_weight');?>;
	}
	#site-title,
	label,
	.mobile-select,
	#page-title-wrap .page-title h1,
	#content .meta-tags-wrap,
	#content .entry-meta a,
	#content .sticky .entry-meta .meta-sticky,
	#content .format-link .entry-header h2,
	#content .format-chat .chat-author,
	#content .pagelink,
	#content h1,
	#content h2,
	#content h3,
	#content h4,
	#content h5,
	#content h6,
	#content blockquote cite,
	#content cite,
	#content dt,
	#content .progress-circle,
	#content .chart span,
	#content #countdown .count-label,
	#content #countdown #count-end,
	#content .lol-toggle-header,
	#content .lol-faq-header,
	#content .job-list .entry-job h4,
	.single-lolfmk-portfolio .project-meta .project-categories,
	.single-lolfmk-portfolio .project-details span,
	#sidebar .divider-wgt a,
	#top-footer .divider-wgt a,
	#sidebar .widget_rss li .rsswidget,
	#top-footer .widget_rss li .rsswidget,
	#sidebar .widget_rss li cite,
	#top-footer .widget_rss li cite,
	#sidebar .widget_tag_cloud a,
	#top-footer .widget_tag_cloud a,
	#sidebar .widget_calendar table caption,
	#top-footer .widget_calendar table caption,
	#sidebar .widget_calendar table thead,
	#top-footer .widget_calendar table thead,
	#sidebar .widget_calendar table tbody tr td a,
	#sidebar .widget_calendar table tfoot tr td a,
	#top-footer .widget_calendar table tbody tr td a,
	#top-footer .widget_calendar table tfoot tr td a,
	#sidebar .lol_widget_twitter .timestamp,
	#top-footer .lol_widget_twitter .timestamp,
	#sidebar .lol-jobs-widget .entry-job a,
	#top-footer .lol-jobs-widget .entry-job a,
	#footer .divider-wgt a,
	#footer .widget_rss li .rsswidget,
	#footer .widget_rss li cite,
	#footer .widget_tag_cloud a,
	#footer .widget_calendar table caption,
	#footer .widget_calendar table thead,
	#footer .widget_calendar table tbody tr td a,
	#footer .widget_calendar table tfoot tr td a,
	#footer .lol_widget_twitter .timestamp,
	#footer .lol-jobs-widget .entry-job a,
	.lol-skill,
	#content .price-name,
	#content .price-cost,
	#content .lol-dropcap,
	#comments .comment-awaiting-moderation,
	#respond .comment-must-logged,
	#comments .nocomments,
	#comments .nopassword,
	#comments .pingback,
	#content .lolfmk-job .job-meta span {
		font-weight: <?php echo get_option('lol_secondary_font_weight');?>;
	}
	button,
	input[type="submit"],
	.lol-button,
	#content .lol-button,
	#content .format-quote .entry-content .quote-caption,
	#content .divider h3,
	#content .divider h2,
	.divider h3,
	.divider h2,
	#content .divider-sm a,
	#content .more-link,
	#content strong,
	#content.attachment-template #image-navigation a,
	#content .about-author h4,
	#content .lol-item-service-column h2,
	#content .lol-item-mini-service-column h2,
	#content .lol-item-block-feature .feature-title h2,
	#content .lol-item-member .meta-member h2,
	#content .testimonial-meta cite,
	#sidebar .widget-header .widget-title,
	#top-footer .widget-header .widget-title,
	#sidebar .widget_rss .widget-title a,
	#top-footer .widget_rss .widget-title a,
	#footer .widget-header .widget-title ,
	#footer .widget_rss .widget-title a,
	#comments .commentlist .comment-meta .fn,
	#comments .commentlist .comment-meta .fn a,
	#content .lol-faq-topics,
	#content .lol-faq-topic-title {
		font-weight: <?php echo get_option('lol_bold_font_weight');?>;
	}
	#content .lol-item-heading-parallax p,
	#content .lol-item-blockquote blockquote,
	#content .lol-item-text-banner p {
		font-weight: <?php echo get_option('lol_light_font_weight');?>;
	}
	.sf-menu > li.current_page_item a,
	.sf-menu > li.current_page_parent a,
	.sf-menu > li.sfHover a,
	.sf-menu > li:hover a {
		color: <?php echo get_option('lol_primary_text_color');?>;
		border-bottom: 4px solid <?php echo get_option('lol_primary_color');?>;
	}
	#top-header .top-header-nav .block-sep {
		background-color: <?php echo get_option('lol_light_text_color');?>;
	}
	.sf-menu a {
		color: <?php echo get_option('lol_light_text_color');?>;
	}
	#content .entry-date .month {
		color: <?php echo get_option('lol_secondary_text_color');?>;
	}
	#top-header .top-header-menu li,
	#nav-menu,
	.sf-menu a,
	.sf-menu ul a,
	#content .entry-date .day,
	#content .entry-date .month {
		font-weight: <?php echo get_option('lol_secondary_font_weight');?>;
	}
	</style>

	<?php
	if (get_option('lol_check_external_font') == 'true') { ?>
		<style>
		body {
			font-family: <?php echo get_option('lol_external_font_name');?>;
		}
		</style>
	<?php } else { ?>
		<style>
		body {
			font-family: <?php echo get_option('lol_primary_font');?>;
		}
		</style>
	<?php }
	}
}