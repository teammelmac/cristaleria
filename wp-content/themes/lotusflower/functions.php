<?php
/**
 * Lollum
 * 
 * Lotus Flower functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */

/**
 * Tell WordPress to run lollum_setup() when the 'after_setup_theme' hook is run.
 */
if (!function_exists('lollum_setup')) {
	function lollum_setup() {

		/* Make Lollum Framework available for translation.
		*  Translations can be added to the /languages/ directory.
		*/
		load_theme_textdomain('lollum', get_template_directory() . '/languages');

		$locale = get_locale();
		$locale_file = get_template_directory() . "/languages/$locale.php";
		if (is_readable($locale_file)) {
		  require_once($locale_file);
		}

		// Register the wp 3.0 Menus.
		register_nav_menu('primary', __('Menu', 'lollum'));
		register_nav_menu('header', __('Header', 'lollum'));
		register_nav_menu('footer', __('Footer', 'lollum'));

		// Add support for Post Formats
		add_theme_support('post-formats', array('aside', 'status', 'quote', 'image', 'gallery', 'video', 'audio', 'link', 'chat'));

		// Add post thumbnails support
		add_theme_support('post-thumbnails');
		add_image_size('widget-thumb', 150, 150, true); // Post thumbnails
		add_image_size('post-thumb', 870, 370, true); // Post thumbnails
		add_image_size('page-thumb', 1170, 495, true); // Page thumbnails
		add_image_size('square-thumb', 870, 580, true); // Square thumbnails
		add_image_size('project-thumb', 870, 9999); // Single Project thumbnails

		if (!isset($content_width)) {
		  $content_width = 870;
		}

		add_theme_support('automatic-feed-links');

		add_theme_support('woocommerce');

		$features = array(
			'Column' => 'yes',
			'Divider' => 'yes',
			'Space' => 'yes',
			'Line' => 'yes',
			'Heading' => 'yes',
			'Heading-Small' => 'yes',
			'Heading-Parallax' => 'yes',
			'Image' => 'yes',
			'Image-Parallax' => 'yes',
			'Image-Text' => 'yes',
			'Service-Column' => 'yes',
			'Mini-Service-Column' => 'yes',
			'Block-Feature' => 'yes',
			'Block-Video' => 'yes',
			'Block-Banner' => 'yes',
			'Block-Text-Banner' => 'yes',
			'Post' => 'yes',
			'Blog-Full' => 'yes',
			'Blog-List' => 'yes',
			'Project' => 'yes',
			'Portfolio-Full' => 'yes',
			'Portfolio-List' => 'yes',
			'Member' => 'yes',
			'Testimonial' => 'yes',
			'Progress-Circle' => 'yes',
			'Countdown' => 'yes',
			'Blockquote' => 'yes',
			'Toggle' => 'yes',
			'FAQs' => 'yes',
			'Brands' => 'yes',
			'Job-List' => 'yes',
			'Map' => 'yes',
			'Info' => 'yes',
			'Mailchimp' => 'yes'
		);
		add_option('lolfmk_supported_features', $features);
		add_option('lolfmk_support_page_builder', 'yes');
		add_option('lolfmk_load_shortcodes_scripts', 'no');

	}
}
add_action('after_setup_theme', 'lollum_setup');

if(!function_exists('lolfmk_remove_supported_blocks')) {
	function lolfmk_remove_supported_blocks() {
		delete_option('lolfmk_supported_features');
		delete_option('lolfmk_support_page_builder');
		delete_option('lolfmk_load_shortcodes_scripts');
	}
}
add_action('switch_theme', 'lolfmk_remove_supported_blocks');

/**
 * Load up core options
 */

require_once('core/core.php');