<?php
/**
 * Lollum
 * 
 * The Template for displaying all single jobs
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<?php get_header(); ?>

<div id="page-title-wrap">
	<div class="container">
		<!-- BEGIN row -->
		<div class="row">
			<!-- BEGIN col-12 -->
			<div class="col-12">
				<div class="page-title">
					<h1><?php the_title(); ?></h1>
					<?php lollum_breadcrumb(); ?>
				</div>
			</div>
			<!-- END col-12 -->
		</div>
		<!-- END row -->
	</div>
</div>

<!-- BEGIN #page -->
<div id="page" class="hfeed">

<!-- BEGIN #main -->
<div id="main" class="container">
	
	<!-- BEGIN row -->
	<div class="row">
		<!-- BEGIN col-9 -->
		<div class="col-9">
	
			<!-- BEGIN #content -->
			<div id="content" role="main">
				
				<?php // START the loop ?>
				<?php while (have_posts()) : the_post(); ?>

					<?php
					$job_location = get_post_meta($post->ID, 'lolfmkbox_job_location', true);
					$job_responsibilities = get_post_meta($post->ID, 'lolfmkbox_job_responsibilities', true);
					$job_skills_n = get_post_meta($post->ID, 'lolfmkbox_job_skills_n', true);
					$job_skills_d = get_post_meta($post->ID, 'lolfmkbox_job_skills_d', true);
					$job_form = get_post_meta($post->ID, 'lolfmkbox_job_form', true);
					$job_lat = get_post_meta($post->ID, 'lolfmkbox_job_lat', true);
					$job_lng = get_post_meta($post->ID, 'lolfmkbox_job_lng', true);
					$job_zoom = get_post_meta($post->ID, 'lolfmkbox_job_zoom', true);
					?>

					<!-- BEGIN #post -->
					<article id="post-<?php the_ID(); ?>" <?php post_class('single'); ?>>

						<?php if ($job_lat !='' && $job_lng !='') { ?>
							<div class="map-canvas-wrapper" data-lat="<?php echo $job_lat; ?>" data-lng="<?php echo $job_lng; ?>" data-zoom="<?php echo $job_zoom; ?>" data-uri="<?php echo LOLLUM_URI; ?>">
								<div class="map-canvas normal"></div>
							</div>

							<?php
							wp_enqueue_script('lolfmk-google-maps-api');
							wp_enqueue_script('lolfmk-google-maps');
							?>
						<?php } ?>

						<div class="post-wrap">

							<!-- BEGIN .entry-date -->
							<div class="entry-date">
								<span class="day"><?php the_time('d'); ?></span>
								<span class="month"><?php the_time('M'); ?></span>
							</div>
							<!-- END .entry-date -->

							<!-- BEGIN .entry-header -->
							<header class="entry-header">
								<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'lollum'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
							</header>
							<!-- END .entry-header -->

							<!-- BEGIN .entry-conent -->
							<div class="entry-content">
								<?php the_content(); ?>
							</div>
							<!-- END .entry-conent -->

							<?php if ($job_location != '') { ?>
								<div class="job-location job-meta">
									<span><?php _e('Location:', 'lollum'); ?></span>
									<i class="icon-map-marker"></i><?php echo $job_location; ?>
								</div>
							<?php } ?>

							<?php if ($job_responsibilities != '') { ?>
								<div class="job-meta">
									<span><?php _e('Responsibilities:', 'lollum'); ?></span>
									<?php echo do_shortcode($job_responsibilities); ?>
								</div>
							<?php } ?>

							<?php if ($job_skills_n != '') { ?>
								<div class="job-meta">
									<span><?php _e('Essentials Skills:', 'lollum'); ?></span>
									<?php echo do_shortcode($job_skills_n); ?>
								</div>
							<?php } ?>

							<?php if ($job_skills_d != '') { ?>
								<div class="job-meta">
									<span><?php _e('Desirable Skills:', 'lollum'); ?></span>
									<?php echo do_shortcode($job_skills_d); ?>
								</div>
							<?php } ?>

							<?php if ($job_form != '') { ?>
								<div class="job-form">
									<p class="job-form-description"><?php _e('To submit your application please complete the form below. Fields marhed with an asterisk <span>*</span> are required. When you have finished click <strong>Apply</strong> at the bottom of this form.', 'lollum'); ?></p>
									<?php echo do_shortcode(''.$job_form.''); ?>
								</div>
							<?php } ?>

						</div>

					</article>
					<!-- END #post -->

				<?php endwhile; ?>
				<?php // END the loop ?>

			</div>
			<!-- END #content -->
	
		</div>
		<!-- END col-9 -->

	<?php get_template_part('sidebar', 'job'); ?>

<!-- END #main -->
</div>

</div>
<!-- END #page -->

<?php get_footer(); ?>