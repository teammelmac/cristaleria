<?php
/**
 * Lollum
 * 
 * The sidebar containing the secondary widget area, displays on shop pages (Woocommerce)
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<!-- BEGIN #secondary -->
<div id="sidebar" class="col-3" role="complementary">
	<!-- BEGIN sidebar -->
	<?php if (!dynamic_sidebar('Shop Sidebar')) : ?>
		<aside>
			<?php get_product_search_form(); ?>
		</aside>
	<?php endif; ?>
	<!-- END sidebar -->
</div>
<!-- END #secondary -->