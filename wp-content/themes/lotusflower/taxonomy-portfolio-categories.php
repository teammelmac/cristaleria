<?php
/**
 * Lollum
 * 
 * The Template for displaying archive-type pages for projects
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<?php get_header(); ?>

<?php // START if have posts ?>
<?php if (have_posts()) : ?>

<div id="page-title-wrap">
	<div class="container">
		<!-- BEGIN row -->
		<div class="row">
			<!-- BEGIN col-12 -->
			<div class="col-12">
				<div class="page-title">
					<h1>
						<?php printf(__('Project Archive: %s', 'lollum'), '<span>' . single_cat_title('', false) . '</span>'); ?>
					</h1>
				</div>
			</div>
			<!-- END col-12 -->
		</div>
		<!-- END row -->
	</div>
</div>

<!-- BEGIN #page -->
<div id="page" class="hfeed template-portfolio four-columns">

<!-- BEGIN #main -->
<div id="main" class="container">

		<?php rewind_posts(); ?>

		<!-- BEGIN row -->
		<div class="row">
			<!-- BEGIN col-12 -->
			<div class="col-12">

				<!-- BEGIN #content -->
				<div id="content" role="main">

					<!-- BEGIN #post -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<div class="row">
							<div class="col-12">
								<div class="section-portfolio-items">

									<?php $count = 0; ?>

									<?php // START the loop ?>
									<?php while (have_posts()) : the_post();
									
									$open = !($count%4) ? '<div class="row">' : '';
									$close = !($count%4) && $count ? '</div>' : '';
									echo $close.$open;

									$portfolio_description = get_post_meta($post->ID, 'lolfmkbox_portfolio_desc', true); ?>

										<div class="portfolio-item col-3">
											<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="portfolio-mask">
												<div class="portfolio-mask-inner"></div>
												<div class="portfolio-mask-inner2"></div>
													<div class="portfolio-link">
														<i class="icon-search"></i>
													</div>
													<div class="project-thumb">
														<?php the_post_thumbnail('square-thumb'); ?>
													</div>
											</a>
											<div class="portfolio-meta">
												<div class="project-item-title">
													<div class="divider-sm">
														<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
													</div>
													<?php lolfmk_display_love_link($post->ID); ?>
												</div>
												<p><?php echo $portfolio_description; ?></p>
											</div>
										</div>

									<?php $count++; ?>

									<?php endwhile; ?>
									<?php // END the loop ?>

									<?php echo $count ? '</div>' : ''; ?>

								</div>
							</div>
						</div>

						<?php lollum_pagination(); ?>


					</article>
					<!-- END #post -->

				</div>
				<!-- END #content -->

			</div>
			<!-- END col-12 -->
		</div>
		<!-- END row -->

	<?php endif; ?>
	<?php // END if have posts ?>

<!-- END #main -->
</div>

</div>
<!-- END #page -->

<?php get_footer(); ?>