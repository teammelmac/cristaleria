<?php
/**
 * Lollum
 * 
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<?php if (get_option('lol_footer_top_check')  == 'true') { ?>

<!-- BEGIN #top-footer -->
<div id="top-footer" role="contentinfo">
	<div class="container">

		<!-- BEGIN row -->
		<div class="row footer-widgets">
			<!-- BEGIN col-4 -->
			<div class="col-4">
				<div class="footer-widget footer-widget-1">
					<?php dynamic_sidebar('Top Footer 1'); ?>
				</div>
			</div>

			<!-- END col-4 -->
			<!-- BEGIN col-4 -->
			<div class="col-4">
				<div class="footer-widget footer-widget-2">
					<?php dynamic_sidebar('Top Footer 2'); ?>
				</div>
			</div>
			<!-- END col-4 -->
			<!-- BEGIN col-4 -->
			<div class="col-4">
				<div class="footer-widget footer-widget-3">
					<?php dynamic_sidebar('Top Footer 3'); ?>
				</div>
			</div>
			<!-- END col-4 -->
		</div>
		<!-- END row -->

	</div>
</div>
<!-- END #top-footer -->

<?php } ?>

<!-- BEGIN #footer -->
<footer id="footer" role="contentinfo">
	<div class="container">

		<!-- BEGIN row -->
		<div class="row footer-widgets">
			<!-- BEGIN col-3 -->
			<div class="col-3">
				<div class="footer-widget footer-widget-1">
					<?php dynamic_sidebar('Footer 1'); ?>
				</div>
			</div>
			<!-- END col-3 -->
			<!-- BEGIN col-3 -->
			<div class="col-3">
				<div class="footer-widget footer-widget-2">
					<?php dynamic_sidebar('Footer 2'); ?>
				</div>
			</div>
			<!-- END col-3 -->
			<!-- BEGIN col-3 -->
			<div class="col-3">
				<div class="footer-widget footer-widget-3">
					<?php dynamic_sidebar('Footer 3'); ?>
				</div>
			</div>
			<!-- END col-3 -->
			<!-- BEGIN col-3 -->
			<div class="col-3">
				<div class="footer-widget footer-widget-4">
					<?php dynamic_sidebar('Footer 4'); ?>
				</div>
			</div>
			<!-- END col-3 -->
		</div>
		<!-- END row -->

	</div>
</footer>
<!-- END #footer -->

<?php if (get_option('lol_footer_bottom_check')  == 'true') { ?>

<!-- BEGIN #footer-bottom -->
<div id="footer-bottom" role="contentinfo">
	<div class="container">
<img src="http://i.imgur.com/KkEsNcS.jpg">
		<!-- BEGIN row -->
		<div class="row">

			<div class="<?php echo (get_option('lol_footer_bottom_menu')  == 'true') ? "col-6" : "col-12" ?>">
				<div class="footer-bottom-copy">
					<?php echo get_option('lol_footer_copy'); ?>
				</div>
			</div>

			<?php if (get_option('lol_footer_bottom_menu')  == 'true') { ?>

				<div class="col-6">
					<div class="footer-bottom-menu">
						<?php wp_nav_menu(array('theme_location' => 'footer', 'depth' => -1)); ?>
					</div>
				</div>

			<?php } ?>

		</div>
		<!-- END row -->

		<?php if (defined('ICL_SITEPRESS_VERSION') && get_option('lol_footer_lang_switcher') == 'true') : ?>

			<!-- BEGIN row -->
			<div class="row">

				<div class="col-12">
					<?php lollum_languages_list_footer(); ?>
				</div>

			</div>
			<!-- END row -->

		<?php endif; ?>

	</div>
</div>
<!-- END #footer-bottom -->
<?php } ?>

<a href="#" id="back-top"><i class="icon-angle-up"></i></a>

</div>
<!-- END wrap -->

<?php
// Analitycs
if ($g_analitycs = get_option('lol_google_analytics')) {
	echo stripslashes($g_analitycs);
} ?>

<?php wp_footer(); ?>
</body>
</html>