<?php
/**
 * Lollum
 * 
 * The template for displaying posts in the Status post format
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<!-- BEGIN #post -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-wrap">

		<!-- BEGIN .entry-date -->
		<div class="entry-date">
			<span class="day"><?php the_time('d'); ?></span>
			<span class="month"><?php the_time('M'); ?></span>
		</div>
		<!-- END .entry-date -->

		<!-- BEGIN .entry-conent -->
		<div class="entry-content">
			<div class="entry-avatar">
				<?php echo get_avatar(get_the_author_meta('email'), $size='100'); ?>
			</div>

			<div class="entry-status">
				<?php the_content('Read more'); ?>
			</div>
		</div>
		<!-- END .entry-conent -->

		<?php
		global $multipage, $more;
		$more = true;
		if ($multipage) {
			echo '<div class="pagelink">';
			wp_link_pages(array('next_or_number'=>'next', 'previouspagelink' => '&laquo;', 'nextpagelink'=>'&raquo;'));
			echo '</div>';
		}
		$more = 0;
		?>

	</div>

	<!-- BEGIN footer .entry-meta -->
	<?php lollum_footer_posts($post->ID); ?>
	<!-- END footer .entry-meta -->

</article>
<!-- END #post -->