<?php
/**
 * Lollum
 * 
 * The template for displaying posts in the Audio post format
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<!-- BEGIN #post -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- BEGIN .entry-audio -->
	<div class="entry-audio">
		<?php
		$embed = get_post_meta($post->ID, 'lolfmkbox_embed_audio', true);
		if(!empty($embed)) {
			echo "<div class='audio-frame'>";
			echo stripslashes(htmlspecialchars_decode($embed));
			echo "</div>";
		} else {
			wp_enqueue_script('mediaelement');
			wp_enqueue_script('wp-mediaelement');
			wp_enqueue_style('mediaelement');
			lollum_show_audio($post->ID);
		}
		?>
	</div>
	<!-- END .entry-audio -->

	<div class="post-wrap">

		<!-- BEGIN .entry-date -->
		<div class="entry-date">
			<span class="day"><?php the_time('d'); ?></span>
			<span class="month"><?php the_time('M'); ?></span>
		</div>
		<!-- END .entry-date -->

		<!-- BEGIN .entry-header -->
		<header class="entry-header">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'lollum'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		</header>
		<!-- END .entry-header -->

		<?php if (is_search()) : // Only display Excerpts for Search ?>

			<!-- BEGIN .entry-summary -->
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div>
			<!-- END .entry-summary -->

		<?php else : ?>

			<!-- BEGIN .entry-conent -->
			<div class="entry-content">
				<?php the_content('Read more'); ?>
			</div>
			<!-- END .entry-conent -->

			<?php
			global $multipage, $more;
			$more = true;
			if ($multipage) {
				echo '<div class="pagelink">';
				wp_link_pages(array('next_or_number'=>'next', 'previouspagelink' => '&laquo;', 'nextpagelink'=>'&raquo;'));
				echo '</div>';
			}
			$more = 0;
			?>

		<?php endif; ?>

	</div>

	<!-- BEGIN footer .entry-meta -->
	<?php lollum_footer_posts($post->ID); ?>
	<!-- END footer .entry-meta -->

</article>
<!-- END #post -->