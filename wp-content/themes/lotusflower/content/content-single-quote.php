<?php
/**
 * Lollum
 * 
 * The template for displaying posts in the Quote post format
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<!-- BEGIN #post -->
<article id="post-<?php the_ID(); ?>" <?php post_class('single'); ?>>

	<div class="post-wrap">

		<!-- BEGIN .entry-date -->
		<div class="entry-date">
			<span class="day"><?php the_time('d'); ?></span>
			<span class="month"><?php the_time('M'); ?></span>
		</div>
		<!-- END .entry-date -->

		<!-- BEGIN .entry-conent -->
		<div class="entry-content">
			<?php
			$ptype_author_quote = get_post_meta($post->ID, 'lolfmkbox_author_quote', true);
			$ptype_source_quote = get_post_meta($post->ID, 'lolfmkbox_source_quote', true);
			?>
			<figure class="quote">
				<blockquote>
					<?php the_content(); ?>
				</blockquote>
				<figcaption class="quote-caption">-&nbsp;
					<?php if ($ptype_source_quote != '') { ?>
						<a href="<?php echo $ptype_source_quote; ?>">
							<?php echo $ptype_author_quote; ?>
						</a>
					<?php } else { ?>
						<?php echo $ptype_author_quote; ?>
					<?php } ?>
				</figcaption>
			</figure>
		</div>
		<!-- END .entry-conent -->

	</div>

	<!-- BEGIN footer .entry-meta -->
	<?php lollum_footer_posts($post->ID); ?>
	<!-- END footer .entry-meta -->

</article>
<!-- END #post -->

<?php
if ((get_option('lol_check_author_bio') == 'true') && (get_post_meta($post->ID, 'lolbox_check_author_bio', true) != 'yes')) {

	lollum_author_bio();

} ?>