<?php
/**
 * Lollum
 * 
 * The default template for displaying content. Used for both single and index/archive/search
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
?>

<!-- BEGIN #post -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- BEGIN .entry-thumbnail -->
	<?php if (has_post_thumbnail()) : ?>

		<div class="entry-thumbnail">
		   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		   	<?php the_post_thumbnail('post-thumb'); ?>
		   </a>
		</div>

	<?php endif; ?>
	<!-- END .entry-thumbnail -->

	<div class="post-wrap">

		<!-- BEGIN .entry-date -->
		<div class="entry-date">
			<span class="day"><?php the_time('d'); ?></span>
			<span class="month"><?php the_time('M'); ?></span>
		</div>
		<!-- END .entry-date -->

		<!-- BEGIN .entry-header -->
		<header class="entry-header">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'lollum'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		</header>
		<!-- END .entry-header -->

		<?php if (is_search()) : // Only display Excerpts for Search ?>

			<!-- BEGIN .entry-summary -->
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div>
			<!-- END .entry-summary -->

		<?php else : ?>

			<!-- BEGIN .entry-conent -->
			<div class="entry-content">
				<?php the_content('Read more'); ?>
			</div>
			<!-- END .entry-conent -->

			<?php
			global $multipage, $more;
			$more = true;
			if ($multipage) {
				echo '<div class="pagelink">';
				wp_link_pages(array('next_or_number'=>'next', 'previouspagelink' => '&laquo;', 'nextpagelink'=>'&raquo;'));
				echo '</div>';
			}
			$more = 0;
			?>

		<?php endif; ?>

	</div>

	<!-- BEGIN footer .entry-meta -->
	<?php lollum_footer_posts($post->ID); ?>
	<!-- END footer .entry-meta -->

</article>
<!-- END #post -->