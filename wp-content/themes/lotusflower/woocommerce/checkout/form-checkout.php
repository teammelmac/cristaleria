<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$woocommerce->show_messages();

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
do_action( 'woocommerce_before_checkout_form', $checkout );

wp_enqueue_script('lolfmk-checkout');

wp_enqueue_script('prettyPhoto', LOLLUM_URI . '/woocommerce/js/jquery.prettyPhoto.min.js', array('jquery'), '1.0', 1);
wp_enqueue_style('woocommerce_prettyPhoto_css', LOLLUM_URI . '/woocommerce/css/prettyPhoto.css');

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', $woocommerce->cart->get_checkout_url() ); ?>

<ul id="checkout-process">
	<li class="<?php echo is_user_logged_in() ? '' : 'active'; ?>" data="step1"><a href="#sign-in"><span>1</span><?php _e('Sign In', 'lollum'); ?></a></li>
	<li class="<?php echo is_user_logged_in() ? 'active' : ''; ?>" data="step2"><a href="#billing"><span>2</span><?php _e('Billing & Shipping', 'lollum'); ?></a></li>
	<li data="step3"><a href="#review-payment"><span>3</span><?php _e('Review & Payment', 'lollum'); ?></a></li>
	<li><span>4</span><?php _e('Confirmation', 'lollum'); ?></li>
</ul>

<div id="checkout-step">

	<?php if ($checkout->enable_signup && !is_user_logged_in()) { ?>
		<div id="sign-in" class="step active">

			<div class="row">

				<div class="col-6">

					<div class="divider">
						<h3><?php _e('Login', 'lollum'); ?></h3>
					</div>

					<?php
						echo woocommerce_checkout_login_form(
							array(
								'message'  => '',
								'redirect' => get_permalink( woocommerce_get_page_id( 'checkout') ),
								'hidden'   => false
							)
						);
					?>

				</div>

				<div class="col-6">

					<div class="divider">
						<h3><?php printf( __('First time on %s?', 'lollum'), get_bloginfo()); ?></h3>
					</div>

					<p class="create-account-message"><?php echo get_option( 'woocommerce_lollum_create_account_text', 1 ); ?></p>
					
					<div>
						<a id="create-account-btn" href="#create-account" class="lol-button small"><?php _e('Create account', 'lollum'); ?></a>
					</div>

					<?php if ($checkout->enable_guest_checkout): ?>
						<a id="checkout-next" href="#billing" class="lol-button small next"><?php _e('Checkout as Guest', 'lollum'); ?></a>
					<?php endif ?>

				</div>

			</div>

		</div>

	<?php } else { ?>

		<div id="sign-in" class="step">

			<div class="row">

				<div class="col-12">

					<p class="already-logged-message"><?php _e('You are already logged in, please continue to the next step.', 'lollum'); ?></p>

					<div class="continue-checkout">
						<a id="logged-next" href="#billing" class="lol-button small next"><?php _e('Continue', 'lollum'); ?></a>
					</div>

				</div>

			</div>

		</div>

	<?php } ?>

	<form name="checkout" method="post" class="checkout" action="<?php echo esc_url( $get_checkout_url ); ?>">

		<div id="create-account-wrapper">
			<div id="create-account">

				<p class="form-row form-row-hide">
					<input class="input-checkbox" id="createaccount" <?php checked($checkout->get_value('createaccount'), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
				</p>

				<div class="divider">
					<h3><?php _e('Create an account', 'lollum'); ?></h3>
				</div>

				<?php foreach ($checkout->checkout_fields['account'] as $key => $field) : ?>

					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

				<?php endforeach; ?>
				
				<p class="create-account-pp-btns">
					<a id="create-account-cancel" href="#" class="lol-button small"><?php _e('Cancel', 'lollum'); ?></a>
					<a id="create-account-done" href="#" class="lol-button small"><?php _e('Done!', 'lollum'); ?></a>
				</p>

			</div>
		</div>

		<div id="billing" class="step <?php echo is_user_logged_in() ? 'active' : ''; ?>">

			<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

			<div class="row" id="customer_details">

				<div class="col-6">

					<?php do_action( 'woocommerce_checkout_billing' ); ?>

				</div>

				<div class="col-6">

					<?php do_action( 'woocommerce_checkout_shipping' ); ?>

				</div>

			</div>

			<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

			<div class="continue-checkout">
				<?php if (!is_user_logged_in()) { ?>
					<a id="login-prev" href="#sign-in" class="lol-button small prev"><?php _e('Previous', 'lollum'); ?></a>
				<?php } ?>
				<a id="review-next" href="#review-payment" class="lol-button small next"><?php _e('Continue', 'lollum'); ?></a>
			</div>

		</div>

		<div id="review-payment" class="step">
		
			<div class="divider">
				<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
			</div>

			<?php do_action( 'woocommerce_checkout_order_review' ); ?>

		</div>

	</form>

</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>