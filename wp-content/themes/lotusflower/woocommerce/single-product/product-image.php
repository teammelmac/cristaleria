<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.3
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $woocommerce, $product;

?>
<div class="images">

	<?php
	if ( has_post_thumbnail() ) {

		$image = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
		$image_title = esc_attr( get_the_title( get_post_thumbnail_id() ) );
		$image_link = wp_get_attachment_url( get_post_thumbnail_id() );
		$attachment_count = count( $product->get_gallery_attachment_ids() );
		$attachment_ids = $product->get_gallery_attachment_ids();

		if ( $attachment_count > 0 ) {
			$gallery = '[product-gallery]';
		} else {
			$gallery = '';
		}
		
		if ($attachment_ids) {

			echo '<div class="flexslider flex-product">';
			echo '<ul class="slides">';
			echo '<div class="preloader"></div>';
			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li><a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s"  rel="prettyPhoto' . $gallery . '">%s</a></li>', $image_link, $image_title, $image ), $post->ID );
			
			foreach ( $attachment_ids as $attachment_id ) {
				$image = wp_get_attachment_image_src($attachment_id, 'single_product_large_thumbnail_size');
				$image_title = esc_attr(get_the_title($attachment_id));
				$image_html = '<img src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'">';

				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li><a href="%s" itemprop="image" title="%s" rel="prettyPhoto[product-gallery]">%s</a></li>', $image[0], $image_title, $image_html ), $post->ID );

			}

			echo '</ul>';
			echo '</div>';

			?>

			<div class="thumbnails row">
				<nav class="thumbnails-nav">
					<ul>
						<?php
						// $image_title = esc_attr(get_the_title($post->ID ));
						$image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
						?>
						<li><a href="<?php echo $image_url; ?>" title="<?php echo $image_title; ?>" rel="thumbnails"><?php the_post_thumbnail('shop_thumbnail'); ?></a></li>
						<?php do_action( 'woocommerce_product_thumbnails' ); ?>
			</div>

		<?php } else {
			echo apply_filters( 'woocommerce_single_product_image_html', sprintf('<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s"  rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image ), $post->ID );
		}
	}
	?>
</div>