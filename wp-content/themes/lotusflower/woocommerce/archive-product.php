<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$shop_sidebar_type = 'left';

if (isset($_GET["sidebar"])) {
	$shop_sidebar = $_GET["sidebar"];
	switch ($shop_sidebar) {
		case 'right-sidebar':
			$shop_sidebar_type = 'right';
			break;

		case 'left-sidebar':
			$shop_sidebar_type = 'left';
			break;

		case 'no-sidebar':
			$shop_sidebar_type = false;
			break;
	}
}

get_header(); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		//do_action('woocommerce_before_main_content');
	?>

	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

		<div id="page-title-wrap">
			<div class="container">
				<!-- BEGIN row -->
				<div class="row">
					<!-- BEGIN col-12 -->
					<div class="col-12">
						<div class="page-title">
							<h1><?php woocommerce_page_title(); ?></h1>
							<?php
							if (get_option('lol_check_breadcumbs')  == 'true') {
								do_action('show_woo_breadcrumb');
							}
							?>
						</div>
					</div>
					<!-- END col-12 -->
				</div>
				<!-- END row -->
			</div>
		</div>

	<?php endif; ?>

	<!-- BEGIN #page -->
	<div id="page" class="hfeed">

		<!-- BEGIN #main -->
		<div id="main" class="container <?php echo $shop_sidebar_type ? 'sidebar-'.$shop_sidebar_type : ''; ?>">

			<!-- BEGIN row -->
			<div class="row">

				<!-- BEGIN col-12 -->
				<div class="col-12">

					<div class="woocommerce-ordering-wrap">

						<?php
							/**
							 * woocommerce_before_shop_loop hook
							 *
							 * @hooked woocommerce_result_count - 20
							 * @hooked woocommerce_catalog_ordering - 30
							 */
							do_action( 'woocommerce_before_shop_loop' );
						?>

					</div>

				</div>
				<!-- END col-12 -->

			</div>
			<!-- END row -->

			<!-- BEGIN row -->
			<div class="row">

				<!-- BEGIN #content -->
				<div id="content" class="col-<?php echo($shop_sidebar_type ? '9' : '12'); ?>" role="main">

					<?php do_action( 'woocommerce_archive_description' ); ?>

					<?php if ( have_posts() ) : ?>
							
						<?php woocommerce_product_loop_start(); ?>

							<?php global $woocommerce_loop; ?>

							<?php
							$count = 0;
							$columns = $shop_sidebar_type ? '3' : '4';
							?>

							<?php woocommerce_product_subcategories(); ?>

							<?php echo ($woocommerce_loop['loop']) ? '</div>' : ''; ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php
								$open = !($count%$columns) ? '<div class="row">' : '';
								$close = !($count%$columns) && $count ? '</div>' : '';
								echo $close.$open;
								?>

								<div class="product-item col-<?php echo($shop_sidebar_type ? '4' : '3'); ?>">

								<?php woocommerce_get_template_part( 'content', 'product' ); ?>

								</div>

								<?php $count++; ?>

							<?php endwhile; // end of the loop. ?>

							<?php echo $count ? '</div>' : ''; ?>

						<?php woocommerce_product_loop_end(); ?>

						<?php
							/**
							 * woocommerce_after_shop_loop hook
							 *
							 * @hooked woocommerce_pagination - 10
							 */
							do_action( 'woocommerce_after_shop_loop' );
						?>

					<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

						<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

					<?php endif; ?>

				</div>
				<!-- END #content -->

				<?php
					/**
					 * woocommerce_after_main_content hook
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					//do_action('woocommerce_after_main_content');
				?>

			<?php if ($shop_sidebar_type == "left" || $shop_sidebar_type == "right") { ?>

				<?php
					/**
					 * woocommerce_sidebar hook
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					do_action('woocommerce_sidebar');
				?>

			<?php } ?>

			<!-- END row -->
			</div>

		<!-- END #main -->
		</div>

	</div>
	<!-- END #page -->

<?php get_footer('shop'); ?>