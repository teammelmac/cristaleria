(function($) {
/* lollum checkout */

	$(document).ready(function() {

		$('form.login, .create-account').show();

		var checkoutSteps = $('#checkout-step').find('.step');

		checkoutSteps.not('.active').hide();


		$('#checkout-process a').on('click', function(e) {
			e.preventDefault();
			var targetStep = $(this).attr('href');
			$('#checkout-process li').removeClass('active');
			$(this).parent().addClass('active');
			checkoutSteps.not(targetStep).hide();
			$(targetStep).show();
			$('body,html').animate({
				scrollTop: $('.woocommerce').offset().top
			}, 300);
		});

		$('#checkout-next, #logged-next').on('click', function(e) {
			e.preventDefault();
			var targetStep = $(this).attr('href');
			$('#checkout-process li').removeClass('active');
			$('#checkout-process li[data="step2"]').addClass('active');
			checkoutSteps.not(targetStep).hide();
			$(targetStep).show();
			$('body,html').animate({
				scrollTop: $('.woocommerce').offset().top
			}, 300);
		});

		$('#login-prev').on('click', function(e) {
			e.preventDefault();
			var targetStep = $(this).attr('href');
			$('#checkout-process li').removeClass('active');
			$('#checkout-process li[data="step1"]').addClass('active');
			checkoutSteps.not(targetStep).hide();
			$(targetStep).show();
			$('body,html').animate({
				scrollTop: $('.woocommerce').offset().top
			}, 300);
		});

		$('#review-next').on('click', function(e) {
			e.preventDefault();
			var targetStep = $(this).attr('href');
			$('#checkout-process li').removeClass('active');
			$('#checkout-process li[data="step3"]').addClass('active');
			checkoutSteps.not(targetStep).hide();
			$(targetStep).show();
			$('body,html').animate({
				scrollTop: $('.woocommerce').offset().top
			}, 300);
		});

		$('#review-payment').on('click', '#billing-prev', function(e) {
			e.preventDefault();
			var targetStep = $(this).attr('href');
			$('#checkout-process li').removeClass('active');
			$('#checkout-process li[data="step2"]').addClass('active');
			checkoutSteps.not(targetStep).hide();
			$(targetStep).show();
			$('body,html').animate({
				scrollTop: $('.woocommerce').offset().top
			}, 300);
		});

		$('#create-account-btn').on('click', function(e) {
			e.preventDefault();
			$('input#createaccount').attr('checked', true);
		});

		$('#create-account-btn').prettyPhoto({
			social_tools: false,
			theme: 'pp_woocommerce',
			horizontal_padding: 40,
			opacity: 0.9,
			deeplinking: false
		});

		$(document).on('click', '#create-account-done', function(e) {
			e.preventDefault();
			$('input#createaccount').attr('checked', true);
			$.prettyPhoto.close();

		});
		$(document).on('click', '#create-account-cancel', function(e) {
			e.preventDefault();
			$('input#createaccount').attr('checked', false);
			$.prettyPhoto.close();
		});
		
	});

})(jQuery);