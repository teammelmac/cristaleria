<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked woocommerce_show_messages - 10
	 */
	 do_action( 'woocommerce_before_single_product' );
?>

<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- BEGIN row -->
	<div class="row">

		<!-- BEGIN col-5 -->
		<div class="col-5">

			<div class="product-single-image">

			<?php
				/**
				 * woocommerce_show_product_images hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
			?>

			</div>

		</div>
		<!-- END col-5 -->

		<!-- BEGIN col-7 -->
		<div class="col-7">

			<div class="summary entry-summary">

				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					// do_action( 'woocommerce_single_product_summary' );
				?>

					<div class="product-title">
						<?php woocommerce_template_single_title(); ?>
						<?php woocommerce_template_single_price(); ?>
					</div>

					<div class="product-description">
						<?php woocommerce_template_single_excerpt(); ?>
					</div>

					<?php woocommerce_template_single_add_to_cart(); ?>
					<?php woocommerce_template_single_meta(); ?>

					<?php if (get_option('lol_check_sharer_products')  == 'true') { ?>
					
						<div class="product-share">
							<i class="icon-share"></i><span><?php _e('Share Product', 'lollum'); ?></span>
							<ul class="social-meta">
								<li><a class="facebook-share" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>"><i class="icon-facebook"></i></a></li>
								<li><a class="twitter-share" href="https://twitter.com/share?url=<?php echo get_permalink(); ?>"><i class="icon-twitter"></i></a></li>
								<li><a class="google-share" href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>"><i class="icon-google-plus"></i></a></li>
							</ul>
						</div>

					<?php } ?>

					<?php woocommerce_template_single_sharing(); ?>

			</div><!-- .summary -->

		</div>
		<!-- END col-7 -->

	</div>
	<!-- END row -->

	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>