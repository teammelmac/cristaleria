<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce; ?>

<?php $woocommerce->show_messages(); ?>

<?php do_action('woocommerce_before_customer_login_form'); ?>

<div class="row">

	<div class="col-6" id="customer_login">
		
		<div class="divider">
			<h2><?php _e( 'Login', 'woocommerce' ); ?></h2>
		</div>
		<form method="post" class="login">
			<p class="form-row">
				<label for="username"><?php _e( 'Username or email', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="input-text" name="username" id="username" />
			</p>
			<p class="form-row">
				<label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="input-text" type="password" name="password" id="password" />
			</p>

			<p class="form-row">
				<?php $woocommerce->nonce_field('login', 'login') ?>
				<input type="submit" class="button small" name="login" value="<?php _e( 'Login', 'woocommerce' ); ?>" />
				<a class="lost_password" href="<?php

				$lost_password_page_id = woocommerce_get_page_id( 'lost_password' );

				if ( $lost_password_page_id )
					echo esc_url( get_permalink( $lost_password_page_id ) );
				else
					echo esc_url( wp_lostpassword_url( home_url() ) );

				?>"><?php _e( 'Lost Password?', 'woocommerce' ); ?></a>
			</p>
		</form>

	</div>

	<?php if (get_option('woocommerce_enable_myaccount_registration')=='yes') : ?>

		<?php
		wp_enqueue_script('lolfmk-my-account');
		wp_enqueue_script('prettyPhoto', LOLLUM_URI . '/woocommerce/js/jquery.prettyPhoto.min.js', array('jquery'), '1.0', 1);
		wp_enqueue_style('woocommerce_prettyPhoto_css', LOLLUM_URI . '/woocommerce/css/prettyPhoto.css');
		?>

		<div class="col-6" id="customer_create_account">
			
			<div class="divider">
				<h3><?php printf( __('First time on %s?', 'lollum'), get_bloginfo()); ?></h3>
			</div>

			<p class="create-account-message"><?php echo get_option( 'woocommerce_lollum_create_account_text', 1 ); ?></p>
			
			<div>
				<a id="create-account-btn" href="#create-account" class="lol-button small"><?php _e('Create account', 'lollum'); ?></a>
			</div>

			<div id="create-account-wrapper">
				<div id="create-account">

					<div class="divider">
						<h3><?php _e('Create an account', 'lollum'); ?></h3>
					</div>

					<form method="post" class="register">

						<?php if ( get_option( 'woocommerce_registration_email_for_username' ) == 'no' ) : ?>

							<p class="form-row">
								<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
								<input type="text" class="input-text" name="username" id="reg_username" value="<?php if (isset($_POST['username'])) echo esc_attr($_POST['username']); ?>" />
							</p>

							<p class="form-row">

						<?php else : ?>

							<p class="form-row">

						<?php endif; ?>

							<label for="reg_email"><?php _e( 'Email', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="email" class="input-text" name="email" id="reg_email" value="<?php if (isset($_POST['email'])) echo esc_attr($_POST['email']); ?>" />
						</p>

						<p class="form-row">
							<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="password" class="input-text" name="password" id="reg_password" value="<?php if (isset($_POST['password'])) echo esc_attr($_POST['password']); ?>" />
						</p>
						<p class="form-row">
							<label for="reg_password2"><?php _e( 'Re-enter password', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if (isset($_POST['password2'])) echo esc_attr($_POST['password2']); ?>" />
						</p>

						<!-- Spam Trap -->
						<div style="left:-999em; position:absolute;"><label for="trap">Anti-spam</label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

						<?php do_action( 'register_form' ); ?>

						<p class="form-row">
							<?php $woocommerce->nonce_field('register', 'register') ?>
							<input type="submit" class="button small" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
						</p>

					</form>

				</div>
			</div>

		</div>

	<?php endif; ?>

</div>

<?php do_action('woocommerce_after_customer_login_form'); ?>