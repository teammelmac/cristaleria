<?php
/**
 * Lollum
 * 
 * The Template for displaying pages with sidebar right
 *
 * @package WordPress
 * @subpackage Lollum Themes
 * @author Lollum <support@lollum.com>
 *
 */
/*
Template Name: Template Page Sidebar (right)
*/
?>

<?php get_header(); ?>

<?php
if(function_exists('putRevSlider')) {
	if (get_post_meta($post->ID, 'lolfmkbox_slider_rev_alias', true)) {
	$slider_selected = get_post_meta($post->ID, 'lolfmkbox_slider_rev_alias', true); ?>
	
	<div class="page-slider header-slider">
		<?php putRevSlider(''.$slider_selected.''); ?>
	</div>

	<?php
	} 
} ?>

<?php if (!get_post_meta($post->ID, 'lolfmkbox_headline_check', true) == 'yes') { ?>

	<div id="page-title-wrap">
		<div class="container">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-12 -->
				<div class="col-12">
					<div class="page-title">
						<h1><?php the_title(); ?></h1>
						<?php lollum_breadcrumb(); ?>
					</div>
				</div>
				<!-- END col-12 -->
			</div>
			<!-- END row -->
		</div>
	</div>

<?php } ?>

<!-- BEGIN #page -->
<div id="page" class="hfeed">

<!-- BEGIN #main -->
<div id="main" class="container sidebar-right">

	<?php // START the loop ?>
	<?php while (have_posts()) : the_post(); ?>
	
	<!-- BEGIN row -->
	<div class="row">

		<!-- BEGIN #content -->
		<div id="content" class="col-9" role="main">

			<!-- BEGIN #post -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if (has_post_thumbnail()) : ?>

					<!-- BEGIN .entry-thumbnail -->
					<div class="entry-thumbnail">
						<?php the_post_thumbnail('post-thumb'); ?>
					</div>
					<!-- END .entry-thumbnail -->

				<?php endif; ?>

				<!-- BEGIN .entry-conent -->
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				<!-- END .entry-conent -->

			</article>
			<!-- END #post -->

		</div>
		<!-- END #content -->

	<?php endwhile; ?>
	<?php // END the loop ?>

		<!-- BEGIN #secondary -->
		<div id="sidebar" class="col-3" role="complementary">
			<!-- BEGIN sidebar -->
			<?php if (!dynamic_sidebar('Page Sidebar')) : ?>
				<aside id="search" class="widget widget_search">
					<?php get_search_form(); ?>
				</aside>
			<?php endif; ?>
			<!-- END sidebar -->
		</div>
		<!-- END #secondary -->

	<!-- END row -->
	</div>

<!-- END #main -->
</div>

</div>
<!-- END #page -->

<?php get_footer(); ?>