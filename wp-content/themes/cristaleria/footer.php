

		</div>
	</div>



<!-- FOOTER -->
        <div id="content-footer">
            <div id="footer">
                <?php wp_footer(); ?>
                <img src="<?php echo bloginfo('template_directory').'/img/logo-cristaleria-la-paz-footer.png'; ?>" alt="Logo Cristalerí La Paz Footer">
                <div class="horarios-atencion">
                    HORARIO DE ATENCIÓN<br>
                    Lunes a viernes 9:00 a 19:00 hrs.<br>
                    Sábados 10:00 a 14:30 hrs.<br>
                    <a href="http://www.cristaleria.cl/wp-admin" target="_blank">Administrador</a>
                </div>
                <div class="datos-tienda">
                    Av. La Paz 126, Independencia, Santiago<br>
                    FONOS +56 2 273 74 326<br>
                    +56 2 273 55 693<br>
                    FAX +56 2 277 71 665<br>
                    EMAIL contacto@cristalerialapaz.cl<br>
                </div>
                
            </div>
        </div>
        <!-- FIN FOOTER -->
        <script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52450731-1', 'auto');
  ga('send', 'pageview');

</script>
        
 		<div id="popup-contrasena" >
 			<span class="button b-close"><span>X</span></span>
 			<div class="content" style="height: auto; width: auto;"></div>
 		</div>       
        
    </body>
</html>
