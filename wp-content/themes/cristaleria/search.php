<?php
/**
 * The search result template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package cristaleria
 * @subpackage cristaleria_lapaz
 * @since cristaleria la paz 1.0
 */
$tituloForzado = 'Resultados de la búsqueda';
get_header();  ?>

<?php require_once(dirname(__FILE__) . '/promo-destacada.php'); ?>

<?php require_once(dirname(__FILE__) . '/templates/loop-simple.php'); ?>

<?php get_footer(); ?>