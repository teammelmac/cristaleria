<?php
/**
 * Template Name: simple Page login
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package cristaleria
 * @subpackage cristaleria_lapaz
 * @since cristaleria la paz 1.0
 */
 
get_header(); 

	require_once(dirname(__FILE__) . '/../promo-destacada.php'); 
	
	while ( have_posts() ) : the_post(); 
	
	the_content(); 
	
	endwhile; // end of the loop. 
		  
		  
	/**
	 *	Logica de validacion de los formularios
	 */
	$mensajeError = false;
	 
	if (isset($_POST['accion']) ) {
		
		foreach ( $_POST as $key => $data ) 
			$_POST[$key] = Fn::limpiaParametros( $data );
		
		if (isset($_POST['step']))
			$_GET['step'] = $_POST['step'];
		
		
		// Validacion de un usuario nuevo
		if ($_POST['accion'] == 2) {
			
			$arrComponentes = array('email' 		=> 'txt_new_email',
									'usuario' 		=> 'txt_user',
									'clave' 		=> 'txt_cont',
					
									'razon' 		=> 'txt_razon',
									'rut' 			=> 'txt_rut',
									'giro' 			=> 'txt_giro',
					
									'nombre' 		=> 'txt_nombre',
									'apellido' 		=> 'txt_ape',
									'direccion1' 	=> 'txt_dir1',
									'direccion2' 	=> 'txt_dir2',
					
									'region' 		=> 'cmb_region',
									'provincia'		=> 'cmb_provincia',
									'comuna' 		=> 'cmb_comuna',
					
									'codigo' 		=> 'txt_post',
									'telefono' 		=> 'txt_tel',
									'celular' 		=> 'txt_cel');
			
			if (!$mensajeError && ( !isset($_POST['txt_new_email']) || $_POST['txt_new_email'] == '') )
				$mensajeError = 'Debe ingresar su e-mail';
			if (!$mensajeError && !Fn::validaEmail($_POST['txt_new_email']))
				$mensajeError = 'El e-mail ingresado no es válido';
			if (!$mensajeError) {
				$query 	= 'select * from ct_user where email = "' . $_POST['txt_new_email'] . '" ;';
				$data 	= $wpdb->get_results($query);
				if (count($data) > 0)
					$mensajeError = 'El e-mail ingresado ya está registrado';				
			}

			if (!$mensajeError && (!isset($_POST['txt_user']) || $_POST['txt_user'] == ''))
				$mensajeError = 'Debe ingresar su nombre de usuario';
			if (!$mensajeError) {
				$query 	= 'select * from ct_user where usuario = "' . $_POST['txt_user'] . '" ;';
				$data 	= $wpdb->get_results($query);
				if (count($data) > 0)
					$mensajeError = 'El usuario ingresado ya está registrado';				
			}				
				
			if (!$mensajeError && (!isset($_POST['txt_cont']) || $_POST['txt_cont'] == ''))
				$mensajeError = 'Debe ingresar la contraseña';
			if (!$mensajeError && (!isset($_POST['txt_cont2']) || $_POST['txt_cont2'] == ''))
				$mensajeError = 'Debe confirmar contraseña';
			if (!$mensajeError && ($_POST['txt_cont'] != $_POST['txt_cont2'])) 
				$mensajeError = 'Las contraseñas no coinciden';

			
			if (!$mensajeError && $_POST['txt_razon'] != '') {
				if (!$mensajeError && $_POST['txt_rut'] == '' )
					$mensajeError = 'Debe ingresar el rut de la empresa';
				if (!$mensajeError && !Fn::validaRut($_POST['txt_rut']))
					$mensajeError = 'El rut ingresado no es válido';
				if (!$mensajeError && $_POST['txt_giro'] == '')
					$mensajeError = 'Debe ingresar el Giro de su empresa';
			}
			
			
			if (!$mensajeError && (!isset($_POST['txt_nombre']) || $_POST['txt_nombre'] == ''))
				$mensajeError = 'Debe ingresar su nombre';
			if (!$mensajeError && (!isset($_POST['txt_ape']) || $_POST['txt_ape'] == ''))
				$mensajeError = 'Debe ingresar su apellido';
			if (!$mensajeError && (!isset($_POST['txt_dir1']) || $_POST['txt_dir1'] == ''))
				$mensajeError = 'Debe ingresar su direccion';
			
			
			if (!$mensajeError && !isset($_POST['rd_acepto']))
				$mensajeError = 'Debe aceptar las condiciones del servicio';
				
			if (!$mensajeError) {
				$campos 	= array();
				$valores 	= array();
				
				foreach($arrComponentes as $key => $values) {
					$campos[] 	= $key;
					$valores[] 	= "'" . $_POST[$values] . "'";
				}
				
				// nuevo usuario registrado	
				$query = 'insert into ct_user (' . implode(',', $campos) . ') values (' . implode(',', $valores) . ');';
				$wpdb->query($query);
				
				// Esto es para activar la sesion del nuevo usuario
				$_POST['accion'] 		= 1;
				$_POST['txt_log_user'] 	= $_POST['txt_user'];
				$_POST['txt_log_pass'] 	= $_POST['txt_cont'];
			}	
		}
		
		if ($_POST['accion'] == 1) {
			if (isset($_POST['txt_log_user']) && isset($_POST['txt_log_pass'])) {
				$query 	= "select * from ct_user where usuario = '" . $_POST['txt_log_user'] . "' and clave='" . $_POST['txt_log_pass'] . "';";
				$data 	= $wpdb->get_results($query);
				if (count($data) == 1) {
					$_SESSION['usuario'] = $data;
					if (isset($_POST['step'])) {
						echo "<script>window.location.replace('" . site_url() . "/carro-de-compras/');</script>";					
					} else {
						echo "<script>window.location.replace('" . site_url() . "');</script>";					
					}
					
				} else {
					$mensajeError = 'No existe la combinación usuario y clave, por favor revise los datos';
				}				
			}
		}
		
	}
	?>
	
	<div class="contacto">
	
		<?php if ($mensajeError != '') { ?>
			<div class="mensajeError"><?php echo $mensajeError ?></div>
		<?php } ?>
	
	    <div class="contactanos">
	       
	       <h2>Ingresa</h2>
	       <p>Ya eres cliente.</p>
	       
	       <form action="" method="post">
	           <p>USUARIO</p><input name="txt_log_user" value="<?php echo (isset($_POST['txt_log_user']) ? $_POST['txt_log_user'] : '') ?>" class="cortos" type="text"><br>
	           <p>CONTRASEÑA</p><input type="password" name="txt_log_pass" class="cortos" type="text"><br>
	           <p class="recordar-contrasena"><a href="javascript:popRecuperaContrasena();">Recordar contraseña</a></p>
	           <input type="submit" class="boton-enviar" value="INGRESAR">
	           <input type="hidden" name="accion" value="1">
	           <?php if (isset($_GET['step']) && $_GET['step']==substr(md5(session_id()), 0,8)){ ?>
	           <input type="hidden" name="step" value="<?= $_GET['step'] ?>">
	           <?php } ?>
	       </form>
	       
	    </div>
	    
	    
	    <div class="ubicacion">
	       <h2>Regístrate</h2>
	       <p>Si eres nuevo, ingresa tus datos para poder cotizar.</p>
	       
	       <form action="" method="post">
	           <h3>Dato Usuario</h3>
	           <p>E-MAIL*</p><input name="txt_new_email" id="txt_new_email" value="<?php echo (isset($_POST['txt_new_email'])? $_POST['txt_new_email'] : '') ?>"  class="cortos" type="text"><br>
	           <p>USUARIO*</p><input name="txt_user" id="txt_user" value="<?php echo (isset($_POST['txt_user'])? $_POST['txt_user'] : '') ?>" class="cortos" type="text"><br>
	           <p>CONTRASEÑA*</p><input name="txt_cont" id="txt_cont" class="cortos" value="<?php echo (isset($_POST['txt_cont'])? $_POST['txt_cont'] : '') ?>" type="password"><br>
	           <p>CONFIRMAR CONTRASEÑA*</p><input name="txt_cont2" id="txt_cont2" value="<?php echo (isset($_POST['txt_cont2'])? $_POST['txt_cont2'] : '') ?>" class="cortos" type="password"><br>
	            
	           <?php 
	           $cmbRegion 		= isset($_POST['cmb_region'])? $_POST['cmb_region'] : '';
	           $cmbProvincia 	= isset($_POST['cmb_provincia'])? $_POST['cmb_provincia'] : '';
	           $cmbComuna 		= isset($_POST['cmb_comuna'])? $_POST['cmb_comuna'] : '';
	           ?>
	           <p>NOMBRE*</p><input name="txt_nombre" id="txt_nombre" value="<?php echo (isset($_POST['txt_nombre'])? $_POST['txt_nombre'] : '') ?>" class="cortos" type="text"><br>
	           <p>APELLIDOS*</p><input name="txt_ape" id="txt_ape" value="<?php echo (isset($_POST['txt_ape'])? $_POST['txt_ape'] : '') ?>" class="cortos" type="text"><br>
	           <p>DIRECCIÓN 1*</p><input name="txt_dir1" id="txt_dir1" value="<?php echo (isset($_POST['txt_dir1'])? $_POST['txt_dir1'] : '') ?>" class="cortos" type="text"><br>
	           <p>DIRECCIÓN 2</p><input name="txt_dir2" id="txt_dir2" value="<?php echo (isset($_POST['txt_dir2'])? $_POST['txt_dir2'] : '') ?>" class="cortos" type="text"><br>
	           <p>REGIÓN</p><label><select name="cmb_region" id="cmb_region" class="cmb-cortos" ><?php echo Fn::lstRegiones($cmbRegion) ?></select></label><br>
	           <p>PROVINCIA</p><select name="cmb_provincia" id="cmb_provincia" class="cmb-cortos" ><?php echo Fn::lstProvincias($cmbRegion, $cmbProvincia) ?></select><br>
	           <p>COMUNA</p><select name="cmb_comuna" id="cmb_comuna" class="cmb-cortos" ><?php echo Fn::lstComunas($cmbProvincia, $cmbComuna) ?></select><br>
	           
	           <p>CÓDIGO POSTAL</p><input name="txt_post" id="txt_post" value="<?php echo (isset($_POST['txt_post'])? $_POST['txt_post'] : '') ?>" class="cortos" type="text"><br>
	           <p>TELÉFONO</p><input name="txt_tel" id="txt_tel" value="<?php echo (isset($_POST['txt_tel'])? $_POST['txt_tel'] : '') ?>" class="cortos" type="text"><br>
	           <p>CELULAR</p><input name="txt_cel" id="txt_cel" value="<?php echo (isset($_POST['txt_cel'])? $_POST['txt_cel'] : '') ?>" class="cortos" type="text"><br>
	           
				<h3>Datos Facturación</h3>
	           <p>RAZÓN SOCIAL / COMPAÑIA</p><input name="txt_razon" id="txt_razon" value="<?php echo (isset($_POST['txt_razon'])? $_POST['txt_razon'] : '') ?>" class="cortos" type="text"><br>
	           <p>RUT</p><input name="txt_rut" id="txt_rut" value="<?php echo (isset($_POST['txt_rut'])? $_POST['txt_rut'] : '') ?>" class="cortos" type="text"><br>
	           <p>GIRO</p><input name="txt_giro" id="txt_giro" value="<?php echo (isset($_POST['txt_giro'])? $_POST['txt_giro'] : '') ?>" class="cortos" type="text"><br>
	           <h3>&nbsp;</h3>
	           

	           <input type="radio" id="rd_acepto" name="rd_acepto" <?php echo (isset($_POST['rd_acepto']) ? "checked" : "") ?> ><span class="condiciones"> Estoy de acuerdo con las condiciones de servicio<br><a href="#">(VER CONDICIONES DE SERVICIO)*</a></span>
	           
	           <input type="submit" class="boton-enviar" value="ENVIAR REGISTRO">
	           <input type="hidden" name="accion" value="2">
	           <?php if (isset($_GET['step']) && $_GET['step']==substr(md5(session_id()), 0,8)){ ?>
	           <input type="hidden" name="step" value="<?= $_GET['step'] ?>">
	           <?php } ?>
	           
	       </form>
	   </div>
	</div>

	<style>
		.cmb-cortos {
			padding:3px;
		    margin: 0px 0px 5px 0px;
		    width:246px;
		    
		    -webkit-border-radius:0px;
		    -moz-border-radius:0px;
		    border-radius:0px;
		    /*
		    -webkit-border-radius:4px;
		    -moz-border-radius:4px;
		    border-radius:4px;
		    -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
		    -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
		    box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
		    color:#888;
		    */
		    background: #e6e7f0;
		    color:#000;
		    border:none;
		    outline:none;
		    display: inline-block;
		    -webkit-appearance:none;
		    -moz-appearance:none;
		    appearance:none;
		    cursor:pointer;
		}
		
	</style>
	
	<script type="text/javascript">

		$(document).ready(function(){
			$('#cmb_region').change(cargaProvincias);
			$('#cmb_provincia').change(cargaComunas);
		});
		
		function cargaProvincias () {
			$('#cmb_provincia').html( '<option value="">Cargando...</option>' );
			$('#cmb_comuna').html( '<option value="">Cargando...</option>' );
			$.ajax({
				url  : "<?php echo site_url() . '/wp-admin/admin-ajax.php?action=ajaxregioncomunaciudad' ?>",
				data : {"source" : 'region', "id" : $('#cmb_region').val()},
				type : 'POST'
			}).done(function( data ){
				$('#cmb_provincia').html( data );
				$('#cmb_comuna').html( '<option value="">Seleccione...</option>' );
			});
		}
		
		function cargaComunas () {
			$('#cmb_comuna').html( '<option value="">Cargando...</option>' );
			$.ajax({
				url  : "<?php echo site_url() . '/wp-admin/admin-ajax.php?action=ajaxregioncomunaciudad' ?>",
				data : {"source" : 'provincia', "id" : $('#cmb_provincia').val()},
				type : 'POST'
			}).done(function( data ){
				$('#cmb_comuna').html( data );
			});
		}

</script>
	
	

<?php get_footer(); ?>