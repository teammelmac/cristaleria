<div class="titulo-buscador">
	<h2>
	<?php 
	if (isset($tituloForzado) && trim($tituloForzado) != '') { 
		echo $tituloForzado;
	} else { 
		echo (single_cat_title( '', false ) == null) ? the_title() : single_cat_title( '' , false);
	}?>
	</h2>
    <div id="buscador"><?php include TEMPLATEPATH . '/searchform.php';?></div>
</div>
<div id="muestra-productos">
<?php
echo is_page();
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			if (function_exists('htmlBoxDestacado'))
				echo htmlBoxDestacado (get_the_ID(), 
									   get_the_post_thumbnail(get_the_ID(), array(236,226)), 
									   get_the_title(), 
									   get_post_meta(get_the_ID(),'prod_codigo', true));	
		}
	} else { 
		echo '<div style="text-align:center;"><br>No hay productos para mostrar<br>&nbsp;<br>&nbsp;</div>';
	} ?>

	<div class="paginador-stock">
	    <p>*PRODUCTOS SUJETOS A VERIFICACIÓN DE STOCK</p>
	    <div class="paginador">
			<?php next_posts_link('Siguiente'); ?>
			<?php previous_posts_link('Anterior'); ?>
	    </div>
	</div>	
</div>
