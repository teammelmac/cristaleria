<?php
/**
 * Template Name: simple Page Vidrieria
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package cristaleria
 * @subpackage cristaleria_lapaz
 * @since cristaleria la paz 1.0
 */
 
get_header(); ?>


	<!--<?php require_once(dirname(__FILE__) . '/../promo-destacada.php'); ?>-->
	<div class="titulo-buscador">
	<h2>
	<?php 
	if (isset($tituloForzado) && trim($tituloForzado) != '') { 
		echo $tituloForzado;
	} else { 
		echo (single_cat_title( '', false ) == null) ? the_title() : single_cat_title( '' , false);
	}?>
	</h2>
    <div id="buscador"><?php include TEMPLATEPATH . '/searchform.php';?></div>
</div>

	<?php while ( have_posts() ) : the_post(); ?>
	    
    	<?php the_content(); ?>
        
	<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>