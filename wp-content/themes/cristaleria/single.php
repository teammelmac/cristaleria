<?php
/**
 * The simple page template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package cristaleria
 * @subpackage cristaleria_lapaz
 * @since cristaleria la paz 1.0
 */

get_header(); ?>
<div class="titulo-buscador">
	<h2>
	<?php 
	if (isset($tituloForzado) && trim($tituloForzado) != '') { 
		echo $tituloForzado;
	} else { 
		echo (single_cat_title( '', false ) == null) ? the_title() : single_cat_title( '' , false);
	}?>
	</h2>
    <div id="buscador"><?php include TEMPLATEPATH . '/searchform.php';?></div>
</div>
<div id="muestra-productos-interior">
<?php
echo is_page();
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			if (function_exists('htmlProductoDetalle'))
				echo htmlProductoDetalle (get_the_ID(), 
										  get_the_post_thumbnail(get_the_ID(), array(236,226)), 
										  get_the_title(), 
										  get_post_meta(get_the_ID(),'prod_codigo', true),
										  get_the_content());	
		}
	} else { 
		echo '<div style="text-align:center;"><br>No hay productos para mostrar<br>&nbsp;<br>&nbsp;</div>';
	} ?>

	<div class="paginador-stock">
	    <p>*PRODUCTOS SUJETOS A VERIFICACIÓN DE STOCK</p>
	</div>	
</div>

<?php get_footer(); ?>