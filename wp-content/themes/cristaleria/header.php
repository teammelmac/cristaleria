<?php
if (isset($_GET['c']) && $_GET['c'] == '1')
	$_SESSION['usuario'] = null;
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
        <meta name="generator" content="Wordpress <?php bloginfo('version'); ?> "/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
        <meta property="og:image" content="http://www.cristaleria.cl/wp-content/themes/cristaleria/img/logo-cristaleria-fb.png"/>
        <meta property="og:description" content="Cristalería La Paz, somos una empresa con 60 años en el comercio, especialista en el mercado menajero, atendiendo a hoteles, casinos, restoranes, hospitales, Fuerzas Armadas y público en general, manejando un promedio de 10 mil a 11 mil productos dentro del área, posicionándonos como una de las más surtidas y económicas del mercado menajero."/>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" />
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/normalize.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css"/>
        <script src="<?php bloginfo('template_directory'); ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
         <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
         <script>window.jQuery || document.write('<script src="<?php bloginfo('template_directory'); ?>/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
         
         <script src="<?php bloginfo('template_directory'); ?>/js/vendor/jquery.bpopup.min.js"></script>
         <style> 
         
         #popup-contrasena { 
         	display : none;
			background-color: #fff;
			border-radius: 10px 10px 10px 10px;
			box-shadow: 0 0 25px 5px #999;
			color: #111;
			min-width: 250px;
			padding: 25px;
         	}
         	
         .button.b-close, .button.bClose {
			border-radius: 7px 7px 7px 7px;
			box-shadow: none;
			font: bold 131% sans-serif;
			padding: 0 6px 2px;
			position: absolute;
			right: -7px;
			top: -7px;
			background-color: #2b91af;
			color: #fff;
			cursor: pointer;
			display: inline-block;
			text-align: center;
			text-decoration: none;
			}
         
         </style>
         <script>
			function popRecuperaContrasena () {
				$("#popup-contrasena").bPopup({
					 	content:'iframe',
			            contentContainer:'.content',
			            loadUrl:'http://www.cristaleria.cl/recupera-contrasena.php'
					});
			}
        </script>
        
    <?php wp_head();?>
    </head>
    <?php echo is_single() ? "<body class='single'>" : "<body>"; ?>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <!-- HEADER -->
        <div id="content-header">
            <div id="header">
                <a href="<?php bloginfo('url'); ?>"><img src="<?php echo bloginfo('template_directory').'/img/logo-cristaleria-la-paz.jpg'; ?>" alt="Logo Cristalería La Paz"></a>
                <div class="sesion">
                	<?php if (!is_array($_SESSION['usuario']) || !isset($_SESSION['usuario']) || $_SESSION['usuario'] == null) { ?>
	                   <div class="sesiones">
                            <a href="<?php echo site_url('login') ?>">INICIAR SESIÓN </a> | <a href="<?php echo site_url('login') ?>"> REGÍSTRATE AQUÍ</a><br>
	                       <a class="recordar-pass" href="#" onclick="popRecuperaContrasena()">Recordar Contraseña</a>
                        </div>
                    <?php } else { ?>
                    	Hola!, <?php echo strtoupper($_SESSION['usuario'][0]->nombre) ?> | <a href="<?php echo site_url() . '?c=1' ?>">Cierra Sesión </a><br>&nbsp;
                    <?php } ?>
                    <div class="facebook"><a href="https://www.facebook.com/CristaleriaLaPaz" target="_blank">Facebook cristaleria</a></div>
                    <div class="carrito"><a href="<?php echo site_url('carro-de-compras') ?>">Carro de Compras (<span class="nroItems" >0</span>)</a></div>
                </div>
                <div id="menu-principal">   
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
                </div>
            </div>
            
        </div>
        <!-- FIN HEADER -->
        
        
        
        <!-- CONTENIDO -->
        <div id="content">
            <div id="content-center">
            
                <div id="menu-secundario">
                     <ul>
                     
                        <li class="primer-menu">Hogar
                            <ul>
                                <li class="sub-menu"><a href="#">MESA</a>
                                    <ul>
                                        <div class="content-column">
                                            <div class="column">
                                                <!--<li><a href="#">ARTÍCULOS DE PLÁSTICO</a>
                                                	<ul><?php wp_list_categories( Fn::subCategorias('articulos-de-plastico') ); ?></ul>
                                                </li>-->
                                                <li><a href="#">ACCESORIOS</a>
                                                	<ul><?php wp_list_categories( Fn::subCategorias('accesorios') ); ?></ul>
                                                </li>
                                            </div>
                                            <div class="column">
                                                <li><a href="#">CUCHILLERÍA</a>
                                                	<ul><?php wp_list_categories( Fn::subCategorias('cuchilleria') ); ?></ul>
                                                </li>
                                                <li><a href="#">CRISTALERÍA</a>
                                                    <ul><?php wp_list_categories( Fn::subCategorias('cristaleria') ); ?></ul>
                                                </li>
                                            </div>
                                            <div class="column">
                                                <li><a href="#">VAJILLERÍA</a>
                                                    <ul><?php wp_list_categories( Fn::subCategorias('vajilleria') ); ?></ul>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                
                                <li class="sub-menu">COCINA
                                    <ul>
                                        <div class="content-column">
	                                        <div class="column">
	                                            <li>ACCESORIOS
	                                                <ul><?php wp_list_categories( Fn::subCategorias('accesorios-cocina') ); ?></ul>
	                                            </li>
	                                            <li>ARTÍCULOS DE MADERA
	                                                <ul><?php wp_list_categories( Fn::subCategorias('articulos-de-madera') ); ?></ul>
	                                            </li>
	                                        </div>
	                                        <div class="column">
	                                            <li>CUCHILLERÍA
	                                                <ul><?php wp_list_categories( Fn::subCategorias('cuchilleria-cocina') ); ?></ul>
	                                            </li>
	                                            <li>MENAJE
	                                                <ul><?php wp_list_categories( Fn::subCategorias('menaje') ); ?></ul>
	                                            </li>
	                                        </div>
	                                        <div class="column">
	                                            <li>UTENSILIOS
	                                                <ul><?php wp_list_categories( Fn::subCategorias('utensilios') ); ?></ul>
	                                            </li>
	                                        </div>
	                                    </div>
                                    </ul>
                                </li>
                                
                                <li class="sub-menu">BAR CAFETERÍA
                                    <ul>
                                        <div class="content-column">
                                        <div class="column ancha-dos">
                                            <li>BAR CAFETERÍA
                                                <ul><?php wp_list_categories( Fn::subCategorias('bar-cafeteria') ); ?></ul>
                                            </li>
                                        </div>
                                    </div>
                                    </ul>
                                </li>
                                
                                <li class="sub-menu">DECORACIÓN
                                    <ul>
                                        <div class="content-column">
                                        <div class="column ancha-dos">
                                            <li>DECORACIÓN
                                                <ul><?php wp_list_categories( Fn::subCategorias('decoracion') ); ?></ul>
                                            </li>
                                        </div>
                                    </div>
                                    </ul>
                                </li>
                                
                                <li class="sub-menu">ELECTRODOMESTICOS
                                    <ul>
                                        <div class="content-column">
                                        <div class="column ancha">
                                            <li>ELECTRODOMESTICOS
                                                <ul><?php wp_list_categories( Fn::subCategorias('electrodomesticos') ); ?></ul>
                                            </li>
                                        </div>
                                    </div>
                                    </ul>
                                </li>
                                
                                <li class="sub-menu">REPOSTERIA
                                    <ul>
                                        <div class="content-column">
                                        <div class="column ancha-dos">
                                            <li>REPOSTERIA
                                                <ul><?php wp_list_categories( Fn::subCategorias('reposteria') ); ?></ul>
                                            </li>
                                        </div>
                                    </div>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="segundo-menu">Línea Profesional
                            <ul>
                                <li>COCINA
                                    <ul>
                                        <div class="content-column">
                                            <div class="column">
                                                <li><a href="#">COCINA</a>
                                                <?php wp_list_categories( Fn::subCategorias('cocina-hotel-restaurant-casino') ); ?>
                                                </li>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <li>VAJILLA HOTELERA
                                    <ul>
                                        <div class="content-column">
                                            <div class="column">
                                                <li><a href="#">VAJILLA HOTELERA</a></li>
                                                <?php wp_list_categories( Fn::subCategorias('vajilla-hotelera') ); ?>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <li>UTENSILIOS HOTELEROS
                                    <ul>
                                        <div class="content-column">
                                            <div class="column ancha-dos">
                                                <li><a href="#">UTENSILIOS HOTELEROS</a></li>
                                                <?php wp_list_categories( Fn::subCategorias('utensilios-hoteleros') ); ?>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="tercer-menu">Oportunidades
                            <ul>
                                <li><a href="<?php bloginfo('url'); ?>">DESTACADOS</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/category/especiales/outlet/">OUTLET</a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>