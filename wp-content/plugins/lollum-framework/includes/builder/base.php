<?php
/**
 * Lollum
 * 
 * A guide for integrate the page builder in your theme
 *
 * @package WordPress
 * @subpackage Lollum Framework
 * @author Lollum <support@lollum.com>
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); }


/***************************************************
* add theme support after activation
* (copy in functions.php)
***************************************************/

if (!function_exists('your_add_support_function')) {
	function your_add_support_function() {

		$features = array(
			'Column' => 'yes',
			'Divider' => 'yes',
			'Space' => 'yes',
			'Line' => 'yes',
			'Heading' => 'yes',
			'Heading-Small' => 'yes',
			'Heading-Parallax' => 'yes',
			'Image' => 'yes',
			'Image-Parallax' => 'yes',
			'Image-Text' => 'yes',
			'Service-Column' => 'yes',
			'Mini-Service-Column' => 'yes',
			'Block-Feature' => 'yes',
			'Block-Video' => 'yes',
			'Block-Banner' => 'yes',
			'Block-Text-Banner' => 'yes',
			'Post' => 'yes',
			'Blog-Full' => 'yes',
			'Blog-List' => 'yes',
			'Project' => 'yes',
			'Portfolio-Full' => 'yes',
			'Portfolio-List' => 'yes',
			'Member' => 'yes',
			'Testimonial' => 'yes',
			'Progress-Circle' => 'yes',
			'Countdown' => 'yes',
			'Blockquote' => 'yes',
			'Toggle' => 'yes',
			'FAQs' => 'yes',
			'Brands' => 'yes',
			'Job-List' => 'yes',
			'Map' => 'yes',
			'Info' => 'yes',
			'Mailchimp' => 'yes'
		);
		add_option('lolfmk_supported_features', $features);
		add_option('lolfmk_support_page_builder', 'yes');
		add_option('lolfmk_load_shortcodes_scripts', 'no');

	}
}
add_action('after_setup_theme', 'your_add_support_function');

/***************************************************
* remove theme support after deactivation
* (copy in functions.php)
***************************************************/

if(!function_exists('your_remove_support_function')) {
	function your_remove_support_function() {
		delete_option('lolfmk_supported_features');
		delete_option('lolfmk_support_page_builder');
		delete_option('lolfmk_load_shortcodes_scripts');
	}
}
add_action('switch_theme', 'your_remove_support_function');

/***************************************************
* calculate size items and pass your markup
* (copy in functions.php)
***************************************************/

if (!function_exists('lolfmk_size_items')) {
	function lolfmk_size_items($item_args) {
		global $lolfmk_full_size;
		$lolfmk_full_size = (empty($lolfmk_full_size)) ? 0 : $lolfmk_full_size;

		$id = ($item_args['id'] != '') ? 'id="'.$item_args['id'].'"' : '';
		$class = (isset($item_args['class']) && $item_args['class'] != '') ? $item_args['class'] : '';
		$image = (isset($item_args['image']) && $item_args['image'] != '') ? 'background-image:url('.$item_args['image'].');' : '';
		$color = (isset($item_args['color']) && $item_args['color'] != '') ? 'background-color:'.$item_args['color'].';' : '';

		$style = ($image == '' && $color == '') ? '' : 'style="'.$image.$color.'"';

		if ($lolfmk_full_size >= 1 || ($lolfmk_full_size + .24) >= 1) {
			echo '</div>' . "\n";
			echo '</div>' . "\n";
			echo '</div>' . "\n";
			echo '<!-- END page-row -->' . "\n" . "\n";
			$lolfmk_full_size = 0;
		}
		if ($lolfmk_full_size == 0) {
			echo '<!-- BEGIN page-row -->' . "\n";
			echo '<div class="page-row '.$class.'" '.$style.'>' . "\n";
			echo '<div class="container">' . "\n";
			echo '<div class="row">' . "\n";
		}

		switch($item_args['size_item']) {
			case '1-4':
				echo '<div class="col-3 lol-page-item" '.$id.'>';
				$lolfmk_full_size += 1/4;
				break;
			case '1-3':
				echo '<div class="col-4 lol-page-item " '.$id.'>';
				$lolfmk_full_size += 1/3;
				break;
			case '1-2':
				echo '<div class="col-6 lol-page-item" '.$id.'>';
				$lolfmk_full_size += 1/2;
				break;
			case '2-3':
				echo '<div class="col-8 lol-page-item" '.$id.'>';
				$lolfmk_full_size += 2/3;
				break;
			case '3-4':
				echo '<div class="col-9 lol-page-item" '.$id.'>';
				$lolfmk_full_size += 3/4;
				break;
			case '1-1':
				echo '<div class="col-12 lol-page-item" '.$id.'>';
				$lolfmk_full_size += 1;
				break;
		}
	}
}


/***************************************************
* column block example
* (copy in functions.php)
***************************************************/

if (!function_exists('lolfmk_print_column')) {
	function lolfmk_print_column($item) {
		$header_text = lolfmk_find_xml_value($item, 'header-text');
		$text = lolfmk_find_xml_value($item, 'text-column');

		if ($header_text != '') {
			echo '<h3>'.$header_text.'</h3>';
		}

		echo '<div class="lol-item-column">'.do_shortcode($text).'</div>';
	}
}


/***************************************************
* output your content
* (copy somewhere in page.php)
***************************************************/

global $lolfmk_full_size;
$lolfmk_full_size = 0;
$page_xml = get_post_meta($post->ID, 'page-xml-val', true);
if ($page_xml = get_post_meta($post->ID, 'page-xml-val', true)) {
	if (class_exists('DOMDocument')) {
		$xml = new DOMDocument();
		$xml->loadXML($page_xml);
		foreach ($xml->documentElement->childNodes as $item) {
			switch($item->nodeName) {
				case 'Column':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_column($item);
					echo '</div>';
					break;
				case 'Divider':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_divider($item);
					echo '</div>';
					break;
				case 'Space':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_space($item);
					echo '</div>';
					break;
				case 'Line':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_line($item);
					echo '</div>';
					break;
				case 'Heading':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_heading($item);
					echo '</div>';
					break;
				case 'Heading-Small':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_heading_small($item);
					echo '</div>';
					break;
				case 'Heading-Parallax':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id'),
						'class' => 'full-img '.lolfmk_find_xml_value($item, 'parallax-effect'),
						'image' => lolfmk_find_xml_value($item, 'image-src'),
						'color' => lolfmk_find_xml_value($item, 'bg-color')
					);
					lolfmk_size_items($args);
					lolfmk_print_heading_parallax($item);
					echo '</div>';
					break;
				case 'Image':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_image($item);
					echo '</div>';
					break;
				case 'Image-Parallax':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id'),
						'class' => 'full-img '.lolfmk_find_xml_value($item, 'parallax-effect'),
						'image' => lolfmk_find_xml_value($item, 'image-src')
					);
					lolfmk_size_items($args);
					lolfmk_print_image_parallax($item);
					echo '</div>';
					break;
				case 'Image-Text':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_image_text($item);
					echo '</div>';
					break;
				case 'Service-Column':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_service_column($item);
					echo '</div>';
					break;
				case 'Mini-Service-Column':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_mini_service_column($item);
					echo '</div>';
					break;
				case 'Block-Feature':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_block_feature($item);
					echo '</div>';
					break;
				case 'Block-Video':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id'),
						'class' => 'full-img '.lolfmk_find_xml_value($item, 'parallax-effect'),
						'image' => lolfmk_find_xml_value($item, 'image-src'),
						'color' => lolfmk_find_xml_value($item, 'bg-color')
					);
					lolfmk_size_items($args);
					lolfmk_print_block_video($item);
					echo '</div>';
					break;
				case 'Block-Banner':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id'),
						'class' => 'full-img '.lolfmk_find_xml_value($item, 'parallax-effect'),
						'image' => lolfmk_find_xml_value($item, 'bg-src'),
						'color' => lolfmk_find_xml_value($item, 'bg-color')
					);
					lolfmk_size_items($args);
					lolfmk_print_block_banner($item);
					echo '</div>';
					break;
				case 'Block-Text-Banner':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_block_text_banner($item);
					echo '</div>';
					break;
				case 'Post':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_post($item);
					echo '</div>';
					break;
				case 'Blog-Full':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_blog_full($item);
					echo '</div>';
					break;
				case 'Blog-List':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_blog_list($item);
					echo '</div>';
					break;
				case 'Project':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_project($item);
					echo '</div>';
					break;
				case 'Portfolio-Full':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_portfolio_full($item);
					echo '</div>';
					break;
				case 'Portfolio-List':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_portfolio_list($item);
					echo '</div>';
					break;
				case 'Member':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_member($item);
					echo '</div>';
					break;
				case 'Testimonial':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_testimonial($item);
					echo '</div>';
					break;
				case 'Progress-Circle':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_progress_circle($item);
					echo '</div>';
					break;
				case 'Countdown':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id'),
						'class' => 'full-img '.lolfmk_find_xml_value($item, 'parallax-effect'),
						'image' => lolfmk_find_xml_value($item, 'image-src'),
						'color' => lolfmk_find_xml_value($item, 'bg-color')
					);
					lolfmk_size_items($args);
					lolfmk_print_countdown($item);
					echo '</div>';
					break;
				case 'Blockquote':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_blockquote($item);
					echo '</div>';
					break;
				case 'Toggle':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_toggle($item);
					echo '</div>';
					break;
				case 'FAQs':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_faqs($item);
					echo '</div>';
					break;
				case 'Brands':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_brands($item);
					echo '</div>';
					break;
				case 'Map':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_map($item);
					echo '</div>';
					break;
				case 'Info':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_info($item);
					echo '</div>';
					break;
				case 'Mailchimp':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_mailchimp($item);
					echo '</div>';
					break;
				case 'Job-List':
					$args = array(
						'size_item' => lolfmk_find_xml_value($item, 'size'),
						'id' => lolfmk_find_xml_value($item, 'element-id')
					);
					lolfmk_size_items($args);
					lolfmk_print_job_list($item);
					echo '</div>';
					break;
			}
		}
		if ($xml->documentElement->childNodes->length > 0) {
			echo '</div>' . "\n";
			echo '</div>' . "\n";
			echo '</div>' . "\n";
			echo '<!-- END page-row -->' . "\n";
		}
	} else {
		echo '<div class="container">';
		echo '<div class="row">';
		echo '<div class="col-12">';
		echo __('Please enable the DOM extension in your PHP configuration', 'lollum');
		echo '</div>';
		echo '</div>';
		echo '</div>';
	}
}