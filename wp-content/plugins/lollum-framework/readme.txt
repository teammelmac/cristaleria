===Lollum Framework===

Contributors: Lollum (lollum.com)
Version 1.0
Requires at least: 3.4.0
Tested up to: 3.7

==Description==

Lollum Framework extends functionality to Lollum themes. It provides some post types, the page builder, shortcodes, post formats meta boxes and the 'love' functionality.

==Frequently Asked Questions==

= What is this plugin and why do I need it? =

Lollum Framework provides extra functionality to the collection of Lollum themes. The plugin is not a requirement to use Lollum themes, but it will extend the themes to function as you see them in the demos.

= Can I use this plugin with other themes? =

Lollum Framework was developed to extend the functionality of Lollum themes specifically, however parts of it may be useful to other themes. If using your own theme, you may have to provide some extra styling or modifications to customize it to your needs.

Please note: I don't provide support for these customizations.

==Changelog==

= v1.0 - Oct 30, 2013 =
* Original Release.