<?php 
/**
 *	Administrador de cotizador
 */
//define('WP_DEBUG', true);

// registro el tipo de post personalizado
add_action( 'admin_head', 'add_menu_icons_styles' );		// inicio la funcionalidad del icono a la izquierda
add_action( 'init' 		, 'create_post_gsproduct' , 0);		// Inicio la funcionalidad de los Custom Post


// Funcion que especifica el icono del menu
function add_menu_icons_styles () { ?> 
	<style> 
		#adminmenu .menu-icon-events div.wp-menu-image:before { content: "\f312";
																content: "\f163";
																content: "\f106"; 
																content: "\f176"; } 
	</style><?php 
}


// declara los productos y sus clasificaciones
function create_post_gsproduct () {
	$etiquetasCustomPost = array (  'name' 					=> __('Productos'),
									'singular_name'			=> __('Producto'),
									'menu_name'				=> __(CGCOTIZADORPLUGIN_PAGE_TITLE),
									'add_new'				=> __('Nuevo'),
									'add_new_item'			=> __('Agrega un nuevo producto'),
									'edit_item'				=> __('Edita producto'),
									'view_item'				=> __('Ver producto'),
									'all_items'				=> __('Productos'),
									'not_found'				=> __('No hay productos'),
									'not_found_in_trash'	=> __('No hay productos en el basurero'));
									
	$supportsCustomPost	= array (	'title',
									'editor',
									'thumbnail',
									'excerpt',
									'custom-fields',
									'page-attributes',
									'categories' );

	$newPostType = array (	'labels' 				=> $etiquetasCustomPost ,
							'supports'				=> $supportsCustomPost ,
							'public'				=> true,
							'has_archive' 			=> true,
							'menu_position' 		=> CGCOTIZADORPLUGIN_MENU_POSIC,
							'publicly_queryable' 	=> true,
							'query_var' 			=> true,
							'hierarchical' 			=> true,
							'capability_type' 		=> 'post',
							'taxonomies' 			=> array( 'post_tag','category' ),
							'menu_icon'				=> 'dashicons-products',
							'show_ui'				=> true,
							'show_in_menu'			=> true,
							'show_in_nav_menus'		=> true,
							//'exclude_from_search'	=> true,
							'map_meta_cap'			=> true,
							'rewrite'           	=> true);

	register_post_type( CGCOTIZADORPLUGIN_CUSTOMPOST_NAME, $newPostType);
}
// METABOX de los precios
add_action('add_meta_boxes'	, 'addMetaBoxs');
add_action('save_post'		, 'meta_customBox_save' );

// Configuracion de los metaBox adicionales
function addMetaBoxs () {
	add_meta_box(CGCOTIZADORPLUGIN_CUSTOMPOST_NAME . '_id',
				 'Adicional',
				 'producto_customBox',
				 CGCOTIZADORPLUGIN_CUSTOMPOST_NAME,
				 'normal',
				 'high');	
}

// Display de la caja de precios
function producto_customBox ( $post ) {
	// Obtengo la informacion del POST
	$values = get_post_custom( $post->ID);
	// Es destacado ?
	$txt_prod_destacado 	= isset($values['prod_destacado']) ? $values['prod_destacado'][0] : '';
	$txt_prod_codigo 		= isset($values['prod_codigo']) ? $values['prod_codigo'][0] : '';
	// Para la accion de guardar
	wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' ); ?>
	<label for="prod_destacado" >Producto destacado</label> &nbsp;
	<input type="checkbox" name="prod_destacado" id="prod_destacado" value="1" <?= ($txt_prod_destacado == '1') ? 'checked' : ''; ?> /><br>
	<label for="prod_codigo" >Código producto</label> &nbsp;<br>
	<input type="input" name="prod_codigo" id="prod_codigo" value="<?= $txt_prod_codigo ?>" /><?php
}

// Acciones de guardar la informacion adicional
function meta_customBox_save ( $post_id ) {
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return; 
	if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
	if( !current_user_can( 'edit_post' ) ) return;
    update_post_meta( $post_id, 'prod_destacado',  ( isset($_POST['prod_destacado']) ? '1' : '' ) );
    update_post_meta( $post_id, 'prod_codigo',  $_POST['prod_codigo']); 
}
