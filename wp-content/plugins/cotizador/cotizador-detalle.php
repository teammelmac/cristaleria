<?php
/**
 * Administrador de detalles de las cotizaciones enviadas por los clientes
 * de cristaleria.cl
 *
 */


add_action( 'wp_ajax_check_product', 'check_product_callback' );
/**
 * Entrega información del producto recibiendo el codigo
 *
 * @param string $codigo
 *
 * @return JSON{'exists' => boolean, 'productPost' => ARRAY_A, 'productStock => ARRAY_A}
 */
function check_product_callback() {
	global $wpdb;

	if ( ! is_numeric( $_POST['codigo'] ) ) {
		echo json_encode( array(
			'exists'        => false,
			'productPost'   => null,
			'productStock'  => null,
			'reqStock'      => $_POST['reqStock'],
			'codigoNoStock' => $_POST['codigoNoStock']
		) );

		die();
	}

	$codigo        = intval( $_POST['codigo'] );
	$reqStock      = intval( $_POST['reqStock'] );
	$codigoNoStock = intval( $_POST['codigoNoStock'] );

	$productPost = $wpdb->get_row(
		$wpdb->prepare(
			"
			SELECT wp_posts.ID, post_title FROM wp_postmeta, wp_posts
			WHERE wp_postmeta.meta_value = %d AND wp_postmeta.post_id = wp_posts.ID
			",
			intval( $codigo ) ),
		ARRAY_A );

	$productStock = $wpdb->get_row(
		$wpdb->prepare(
			"
			SELECT codigo, precio, stock FROM ct_productos
			WHERE codigo = %d",
			intval( $codigo ) ),
		ARRAY_A );

	echo json_encode( array(
		'exists'        => (bool) $productStock,
		'existsPost'    => (bool) $productPost,
		'productPost'   => $productPost,
		'productStock'  => $productStock,
		'reqStock'      => $reqStock,
		'codigoNoStock' => $codigoNoStock
	) );

	die();
}

/**
 * Busca productos similares cuando no hay stock.
 *
 * @param string $codigo
 *
 * @return array('num_rows' => integer, 'products' => ARRAY_A)
 */
function _buscaProductosSimilares( $codigo = '', $cantidad = 1 ) {
	global $wpdb;

	// Obtenemos el post que corresponde al código del producto
	$post = $wpdb->get_row(
		$wpdb->prepare(
			"
			SELECT wp_posts.ID, post_title FROM wp_postmeta, wp_posts
			WHERE wp_postmeta.meta_value = %d
				AND wp_postmeta.post_id = wp_posts.ID
			",
			intval( $codigo )
		), ARRAY_A
	);

	// Obtenemos las categorías del producto
	$categories = $wpdb->get_col(
		$wpdb->prepare(
			"
			SELECT DISTINCT term_taxonomy_id
			FROM wp_posts, wp_term_relationships, wp_terms
			WHERE wp_posts.ID = %d
      			AND post_type = %s
          		AND post_status = %s
              	AND (wp_posts.ID = wp_term_relationships.object_id)
				AND (wp_terms.term_id = term_taxonomy_id)
			",
			intval( $post['ID'] ), 'cotizadorp', 'publish'
		)
	);

	// Buscamos productos similares con stock y similares en nombre
	$product_words      = explode( ' ', trim( $post['post_title'] ) );
	$product_first_term = $product_words[0] . '%';
	$products           = $wpdb->get_results(
		$wpdb->prepare(
			"
			SELECT DISTINCT
					wp_posts.ID,
					wp_posts.post_title,
					wp_term_relationships.term_taxonomy_id,
					wp_terms.slug,
					ct_productos.stock,
					ct_productos.codigo,
					ct_productos.precio
			FROM wp_posts, wp_term_relationships, wp_terms, wp_postmeta, ct_productos
			WHERE post_type = %s
				AND post_status = %s
				AND (wp_posts.ID = wp_term_relationships.object_id)
				AND (wp_terms.term_id = term_taxonomy_id)
				AND (wp_terms.term_id IN (" . implode( ',', array_filter( $categories, 'is_numeric' ) ) . "))
				AND (
					wp_postmeta.post_id = wp_posts.ID AND
					ct_productos.codigo = wp_postmeta.meta_value AND
					ct_productos.stock >= $cantidad
				)
				AND (wp_posts.post_title LIKE %s)
			GROUP BY
				wp_posts.ID,
				wp_posts.post_title",
			'cotizadorp', 'publish', $product_first_term
		), ARRAY_A
	);

	return array( 'num_rows' => $wpdb->num_rows, 'products' => $products );
}

/**
 * Funcion que reemplaza una variable impresa en un HTML por su valor en el
 * sistema
 *
 * @param String $cadena
 * @param Array $arrParametros
 */
function _evaluaParametrosHtml( $cadena = '', $arrParametros = array() ) {
	foreach ( $arrParametros as $key => $data ) {
		$cadena = implode( $data, explode( '{' . $key . '}', $cadena ) );
	}

	return $cadena;
}

/**
 * Retorna el valor del precio del producto y su stock
 * Las posibles respuestas son:
 * a) >0, tiene productos en Stock y el numero es su precio
 * b) =0, El producto tiene precio pero no hay existencias en Stock
 * c) -1, El producto no está registrado en el maestro de productos
 *
 * @param string $codigo
 */
function _buscaValoresProducto( $codigo, $stock = 0 ) {
	global $wpdb;
	$query   = "SELECT * FROM ct_productos WHERE codigo='$codigo';";
	$retorno = $wpdb->get_results( $query, ARRAY_A );

	if ( count( $retorno ) == 1 ) {
		$retorno = $retorno[0];
		if ( $retorno['stock'] == 0 ) {
			// return 0;
			// retornar precio de todas formas
			return intval( $retorno['precio'], 10 );
		} else {
			if ( ( intval( $retorno['stock'], 10 ) - $stock ) < 0 ) {
				return 0;
			}

			return intval( $retorno['precio'], 10 );
		}
	} else {
		return - 1;
	}
}

/**
 * Busca la data del producto en la tabla de maestros
 *
 * @param string $codigo
 * @param number $stock
 */
function _buscaMaestroProducto( $codigo = '' ) {
	global $wpdb;

	return $wpdb->get_row( "SELECT * FROM ct_productos WHERE codigo='$codigo'", ARRAY_A );
}


// Se agrega la pagina a la plataforma de WordPress
add_action( 'admin_menu', 'inicializa_cotizacion_datalle_page' );
function inicializa_cotizacion_datalle_page() {
	add_submenu_page(
		null,
		'Detalle cotización',
		'Detalle cotización',
		'administrator',
		'cotizacion-detalle',
		'cotizacion_detalle_callback'
	);
}

add_action( 'admin_enqueue_scripts', 'safely_add_stylesheet_to_admin' );

function safely_add_stylesheet_to_admin() {
	wp_enqueue_style( 'cotizador-chosen', plugins_url( 'chosen.min.css', __FILE__ ) );
	wp_enqueue_style( 'cotizador-animate', plugins_url( 'animate.css', __FILE__ ) );
}

/**
 * visualiza el formulario de del detalle de cada cotización y
 * disponibiliza las acciones de confirmar y rechazar la cotización
 */
function cotizacion_detalle_callback() {
	global $wpdb;

	$mensajeAccion = '';
	$retorno       = array();

	if ( ! isset ( $_GET ['id'] ) || ! Fn::validaNumero( $_GET ['id'] ) ) {
		die ( '<div class="wrap"><h2>Upps!! hay un problema con lo que intentas hacer :X</h2></div>' );
	}

	$idCotizacion = $_GET ['id'];

	// Acciones seteados por el formulario
	if ( isset ( $_POST ['action'] ) && Fn::validaNumero( $_POST ['action'] ) ) {
		switch ( $_POST ['action'] ) {
			case 1 : // Caso respuesta a cotizacion

				// Obtengo los datos del usuario
				$query = "SELECT
                        l.id,
                        l.stamp,
                        DATE_FORMAT( l.stamp, '%d/%m/%Y %H:%i') as fecha,
                        l.usuario,
                        l.solicitud,
                        l.fecha_respuesta,
                        l.estado,
                        l.respuesta_txt,
                        l.reemplazo,
                        u.nombre,
                        u.apellido,
                        u.razon,
                        u.rut,
                        u.giro,
                        u.region,
                        u.provincia,
                        u.comuna,
                        concat (u.direccion1, ' ' , u.direccion2) as direccion,
                        u.codigo,
                        u.email,
                        u.telefono,
                        u.celular
                    FROM
                        ct_log l
                        LEFT JOIN
                        ct_user u on l.usuario = u.usuario
                    WHERE
                        l.id = '$idCotizacion';";

				$retorno = $wpdb->get_results( $query, ARRAY_A );
				if ( count( $retorno ) != 1 ) {
					$mensajeAccion = 'Error rescatando la información, por favor contacte al administrador del sistema.';
					break;
				}
				$retorno = $retorno [0];

				// Update ct_log with the replacement products, if any.
				$reemplazosString = stripslashes( $_POST['reemplazos'] );
				$reemplazos       = json_decode( $reemplazosString, true );
				if ( isset( $_POST['reemplazos'] ) ) {

					$replacementsUpdateStatus = $wpdb->update(
						'ct_log',
						array( 'reemplazos' => $reemplazosString ),
						array( 'usuario' => $retorno['usuario'] )
					);

				}

				/*
				 * START Notificación email
				 */

				// Creo la tabla de detalle con sus valores
				$jsonSolicitud = json_decode( $retorno ['solicitud'] );
				$htmlTablaMail = '';
				$cont          = 0;
				$totalP        = 0;

				foreach ( $jsonSolicitud as $key => $data ) {
					$estilo = ( $cont ++ % 2 == 0 ) ? '#CED1E3' : '#B5B8D4';
					$htmlTablaMail .= '<tr>';
					$htmlTablaMail .= '<td style="background-color:' . $estilo . '";>' . $data->code . '</td>';
					$htmlTablaMail .= '<td style="background-color:' . $estilo . '";>' . $data->desc . '</td>';
					$htmlTablaMail .= '<td style="backgroun-color:' . $estilo . '";>' . $data->cantidad . '</td>';


					// Reviso que la cotización se encuentre integra
					$productoMaestro = _buscaMaestroProducto( trim( $data->code ) );
					if ( $productoMaestro == null ) {
						$subTotal = 'No existe producto';
					} else {
						$stock = intval( $productoMaestro['stock'], 10 ) - intval( intval( $data->cantidad, 10 ), 10 );
						if ( $stock < 0 ) {
							$subTotal = 'Sin Stock';
						} else {
							$jsonSolicitud->$key->precio = $productoMaestro['precio'];
							$totalP += intval( $productoMaestro['precio'], 10 ) * intval( $data->cantidad, 10 );
							$subTotal       = '$ ' . intval( $productoMaestro['precio'], 10 ) * intval( $data->cantidad, 10 );
							$precioProducto = '$ ' . $productoMaestro['precio'];
						}
					}

					$htmlTablaMail .= '	<td style="background-color: ' . $estilo . ';">' . $subTotal . '</td>';
					$htmlTablaMail .= '</tr>';

					/*
					 * La cotización está respondida, revisar si este producto ha sido reemplazado
					 * e insertar el reemplazo en una fila debajo del producto sin stock.
					 */
					if ( isset( $reemplazos[ strval( $data->code ) ] ) ) {
						$reemplazoCodigo   = $reemplazos[ strval( $data->code ) ]['codigo'];
						$reemplazoCantidad = $reemplazos[ strval( $data->code ) ]['cantidad'];
						$reemplazoPrecio   = $reemplazos[ strval( $data->code ) ]['precio'];
						$reemplazoNombre   = $reemplazos[ strval( $data->code ) ]['nombre'];
						$reemplazoSubTotal = intval( $reemplazoPrecio ) * intval( $reemplazoCantidad );
						$htmlTablaMail .= '<tr style="background-color:#e0e2eb;">'
						                  . '<td>' . $reemplazoCodigo . ' (reemplazo)</td>'
						                  . '<td>' . $reemplazoNombre . '</td>'
						                  . '<td>' . $reemplazoCantidad . '</td>'
						                  . '<td>$ ' . $reemplazoSubTotal . '</td></tr>';
					}
				}
				$retorno['tbl_html_productos'] = $htmlTablaMail;
				$retorno['txt_comentario']     = htmlentities( $_POST ['txt_comentario'], ENT_QUOTES );
				if ( isset( $reemplazos['total'] ) ) {
					$totalR                        = intval( $reemplazos['total'] );
					$retorno['totalCotizacion']    = '$ ' . $totalR;
					$retorno['totalIva']           = '$ ' . number_format( $totalR * 1.19 - $totalR, 0 );
					$retorno['totalCotizacioniva'] = '$ ' . number_format( $totalR * 1.19, 0 );
				} else {
					$retorno['totalCotizacion']    = '$ ' . $totalP;
					$retorno['totalIva']           = '$ ' . number_format( $totalP * 1.19 - $totalP, 0 );
					$retorno['totalCotizacioniva'] = '$ ' . number_format( $totalP * 1.19, 0 );
				}

				// Obtengo el template para reemplazar las variables sus valores finales
				$file         = fopen( dirname( __FILE__ ) . '/templatemail.html', 'r' );
				$htmlTemplate = '';
				while ( ! feof( $file ) ) {
					$htmlTemplate .= _evaluaParametrosHtml( fgets( $file ), $retorno );
				}
				fclose( $file );


				// Creacion del MAIL con formato HTML
				$to      = $retorno ['email'];
				$subject = 'Respuesta a cotización Cristalería La Paz';
				$message = $htmlTemplate;

				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
				$headers .= "To:  " . $retorno['nombre'] . " <" . $retorno ['email'] . ">\r\n";
				$headers .= "From: Cristaleria la Paz <contacto@cristaleria.cl>\r\n";

				if ( mail( $to, $subject, $message, $headers ) ) {
					$txtComentario = htmlentities( $_POST ['txt_comentario'], ENT_QUOTES );
					$query         = "UPDATE ct_log SET estado=2, respuesta_txt='"
					                 . $retorno['txt_comentario']
					                 . "', solicitud='"
					                 . json_encode( $jsonSolicitud )
					                 . "' where id='$idCotizacion';";
					$wpdb->query( $query );
					$mensajeAccion = 'La cotización ha sido enviada al cliente correctamente, <a href="'
					                 . admin_url()
					                 . 'admin.php?page=cotizador/cotizador-grid.php">volver a la lista de cotizaciones.</a>';
				} else {
					$mensajeAccion = 'Error en el envío de la cotización, por favor inténtelo mas tarde '
					                 . 'o contacte al administrador de sistemas.';
				}
				break;

			case 2 : // Caso rechazado
				$query = "UPDATE ct_log set estado=3 where id='$idCotizacion';";
				$wpdb->query( $query );
				$mensajeAccion = 'La cotización ha sido rechazada correctamente, <a href="'
				                 . admin_url()
				                 . 'admin.php?page=cotizador/cotizador-grid.php">volver a la lista de cotizaciones.</a>';
				break;

			/*
			 * END Notificación email
			 */
		}
	}

	// Consulta a la base de datos de la cotizacion seleccionada
	$query = "SELECT
                l.id,
                l.stamp,
                DATE_FORMAT( l.stamp, '%d/%m/%Y %H:%i') as fecha,
                l.usuario,
                l.solicitud,
                l.reemplazos,
                l.fecha_respuesta,
                l.estado,
                l.respuesta_txt,
                l.reemplazo,
                u.nombre,
                u.apellido,
                u.razon,
                u.rut,
                u.giro,
                u.region,
                r.re_nombre,
                u.provincia,
                p.pr_nombre,
                u.comuna,
                c.co_descripcion,
                concat (u.direccion1, ' ' , u.direccion2) as direccion,
                u.codigo,
                u.email,
                u.telefono,
                u.celular
            FROM
                ct_log l
                LEFT JOIN ct_user u on l.usuario = u.usuario
                LEFT JOIN region r on u.region = r.re_id
                LEFT JOIN provincia p on p.pr_id = u.provincia
                LEFT JOIN comuna c on c.co_id = u.comuna
            WHERE
                l.id = '$idCotizacion';";

	$retorno = $wpdb->get_results( $query, ARRAY_A );
	if ( count( $retorno ) != 1 ) {
		die ( '<div class="wrap"><h2>Upps!! hay un problema con lo que intentas hacer :X</h2></div>' );
	}

	$retorno        = $retorno [0];
	$jsonSolicitud  = json_decode( $retorno ['solicitud'] );
	$jsonReemplazos = json_decode( $retorno['reemplazos'], true );

	?>

	<style>
		.form-table th, .form-table td {
			padding: 0px 10px 20px 10px !important;
		}

		.manual-container,
		.suggested-container {
			margin-left: 12px;
		}

		.suggested-container {
			margin-bottom: 12px;
		}

		.suggested-container label {
			margin-right: 81px;
		}

		.manual-container {
			margin-bottom: 6px;
		}

		.manual-container label {
			margin-right: 24px;
		}

		.replacement-error {
			margin-left: 12px;
			font-weight: bold;
			padding: 4px;
			border: 1px solid red;
		}
	</style>

	<div class="wrap">
	<h2>Detalle de la cotización</h2>
	<?php if ( $mensajeAccion != '' ) {
		?>
		<div id="message" class="updated below-h2">
			<p><?php echo $mensajeAccion ?></p>
		</div>
	<?php } ?>

	<p>Fecha de envío de la cotización : <?php echo $retorno['stamp'] ?><br>&nbsp;
	</p>

	<h3>Datos del cliente</h3>

	<div class="postbox ">
		<table class="form-table">
			<tbody>
			<tr valign="top">
				<th scope="row"><label>Nombre</label></th>
				<td><?php echo $retorno['nombre'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Apellido</label></th>
				<td><?php echo $retorno['apellido'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Razón social</label></th>
				<td><?php echo $retorno['razon'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>RUT</label></th>
				<td><?php echo $retorno['rut'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Giro</label></th>
				<td><?php echo $retorno['giro'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Región</label></th>
				<td><?php echo $retorno['re_nombre'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Provincia</label></th>
				<td><?php echo $retorno['pr_nombre'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Comuna</label></th>
				<td><?php echo $retorno['co_descripcion'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Dirección </label></th>
				<td><?php echo $retorno['direccion'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Código postal</label></th>
				<td><?php echo $retorno['codigo'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>E-mail</label></th>
				<td><?php echo $retorno['email'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Teléfono</label></th>
				<td><?php echo $retorno['telefono'] ?></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label>Celular</label></th>
				<td><?php echo $retorno['celular'] ?></td>
			</tr>
			</tbody>
		</table>
	</div>

	<p>&nbsp;</p>

	<h3>Detalle de los productos solicitados</h3>
	<?php
	if ( $retorno['reemplazo'] == '1' ) {
		?>
		<p><strong>El cliente permite reemplazo de artículos sin Stock.</strong></p>
	<?php } ?>
	<table class="wp-list-table widefat fixed" cellspacing="0">
	<thead>
	<tr>
		<th scope="col" class="manage-column">COD</th>
		<th scope="col" class="manage-column">Nombre</th>
		<th scope="col" class="manage-column">Cantidad</th>
		<th scope="col" class="manage-column">Precio unitario</th>
		<th scope="col" class="manage-column">Sub-total</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$cont   = 0;
	$totalP = 0;
	foreach ( $jsonSolicitud as $key => $data ) {

		$estilo   = ( $cont ++ % 2 == 0 ) ? 'class="alternate"' : '';
		$subTotal = '-';

		/*
		 * @estado:
		 *      1 => pendiente
		 *      2 => respondido
		 *      3 => rechazado
		 */
		if ( $retorno['estado'] == 2 ) {
			/*
			 * Cotización enviada, sólo lectura.
			 */

			if ( ! isset( $data->precio ) || $data->precio == null ) {
				$precioProducto = 'Sin Stock';
				$subTotal       = '-';
			} else {
				$precioProducto = '$ ' . $data->precio;
				$subTotal       = '$ ' . ( intval( $data->precio, 10 ) * intval( $data->cantidad, 10 ) );
				$totalP += intval( $data->precio, 10 ) * intval( $data->cantidad, 10 );
			}


		} else {
			/*
			 * La cotización está pendiente, revisar stock y si no hay buscar
			 * productos similares.
			 */
			$cod             = trim( $data->code );
			$productoMaestro = _buscaMaestroProducto( trim( $data->code ) );


			$stock = intval( $productoMaestro['stock'], 10 ) - intval( intval( $data->cantidad, 10 ), 10 );


			if ( $productoMaestro == null ) {
				$subTotal       = '';
				$precioProducto = 'Producto no existe';
			} elseif ( $stock < 0 ) {
				// No hay stock, buscar similares
				$codigosSimilares = _buscaProductosSimilares( $cod, intval( $data->cantidad ) );
				$subTotal         = '';
				$precioProducto   = 'Sin Stock';
			} else {
				$totalP += intval( $productoMaestro['precio'], 10 ) * intval( $data->cantidad, 10 );
				$subTotal       = '$ ' . intval( $productoMaestro['precio'], 10 ) * intval( $data->cantidad, 10 );
				$precioProducto = '$ ' . $productoMaestro['precio'];
			}
		}
		?>


		<tr <?php echo $estilo ?>>
			<td><?php echo $data->code ?></td>
			<td><?php echo $data->desc ?></td>
			<td><?php echo $data->cantidad ?></td>
			<td>
				<?php echo $precioProducto ?>
			</td>
			<td>
				<?php
				echo $subTotal;
				if ( isset( $stock ) && $stock < 0 && $productoMaestro && isset( $codigosSimilares ) && $retorno['reemplazo'] == '1' ) {
					echo "<button class='similar button button-small' data-codigo='$cod'>" .
					     "Reemplazar producto</button>";
				}
				?>
			</td>
		</tr>


		<?php
		/*
		 * La cotización está respondida, revisar si este producto ha sido reemplazado
		 * e insertar el reemplazo en una fila debajo del producto sin stock.
		 */
		if ( isset( $jsonReemplazos[ strval( $data->code ) ] ) ) {
			$reemplazoCodigo   = $jsonReemplazos[ strval( $data->code ) ]['codigo'];
			$reemplazoCantidad = $jsonReemplazos[ strval( $data->code ) ]['cantidad'];
			$reemplazoPrecio   = $jsonReemplazos[ strval( $data->code ) ]['precio'];
			$reemplazoNombre   = $jsonReemplazos[ strval( $data->code ) ]['nombre'];
			?>
			<tr style="background-color:#e0e2eb;">
				<td><?php echo $reemplazoCodigo . " (reemplazo)" ?></td>
				<td><?php echo $reemplazoNombre ?></td>
				<td><?php echo $reemplazoCantidad ?></td>
				<td>
					<?php echo '$' . $reemplazoPrecio ?>
				</td>
				<td>
					<?php
					echo '$' . intval( $reemplazoPrecio ) * intval( $reemplazoCantidad );
					?>
				</td>
			</tr>
		<?php
		}
		?>


		<?php
		if ( isset( $stock ) && $stock < 0 && $productoMaestro && isset( $codigosSimilares ) && $retorno['reemplazo'] == '1' ) {
			/*
			 * Si la cotización está pendiente, incluir tablas ocultas para reemplazar
			 * los productos por sugerencias o código manual.
			 */
			?>
			<tr class="similar-table" data-codigo="<?= $cod ?>" data-cantidad="<?= $data->cantidad ?>">

				<td colspan="5" style="overflow: visible;">
					<?php if ( intval( $codigosSimilares['num_rows'] ) > 0 ) { ?>
						<div class="suggested-container">
							<label>
								<input type="radio"
								       class="suggested-code-radio"
								       data-codigo="<?= $cod ?>"
								       name="replacement-mode-<?= $cod ?>" checked> Productos sugeridos
							</label>

							<select data-codigo="<?= $cod ?>"
							        class="chosen suggested-label"
							        data-placeholder="Elegir producto&hellip;">
								<option></option>
								<?php
								$subtotal = null;
								foreach ( $codigosSimilares['products'] as $codigo ) {
									$subtotal = intval( $codigo['precio'] ) * $data->cantidad;
									echo "<option value='" . $codigo['codigo']
									     . "' data-nombre='" . $codigo['post_title']
									     . "' data-precio='" . $codigo['precio']
									     . "' data-cantidad='" . $data->cantidad
									     . "'>"
									     . "$codigo[codigo]\t$codigo[post_title]\t$$codigo[precio]"
									     . "\t(x$data->cantidad $$subtotal)</option>";
								}
								?>
							</select>
						</div>

						<div class="manual-container">
							<label>
								<input type="radio"
								       class="manual-code-radio"
								       data-codigo="<?= $cod ?>"
								       name="replacement-mode-<?= $cod ?>"/> Ingresar código manualmente
							</label>
							<input class="manual-code-input" data-codigo="<?= $cod ?>" type="text" disabled
							       style="width:96px;"/>
							<button class="manual-code-verify button" data-codigo="<?= $cod ?>" disabled>
								Verificar
							</button>
						</div>


					<?php } else { ?>
						<div class="suggested-container">
							<label class="suggested-label">
								<input type="radio" name="replacement-mode-<?= $cod ?>" disabled> No hay productos
								sugeridos
							</label>
						</div>

						<div class="manual-container">
							<label>
								<input type="radio"
								       class="manual-code-radio"
								       data-codigo="<?= $cod ?>"
								       name="replacement-mode-<?= $cod ?>" checked> Ingresar código
								manualmente
							</label>

							<input class="manual-code-input" data-codigo="<?= $cod ?>" type="text"
							       style="width:96px;"/>

							<button class="manual-code-verify button" data-codigo="<?= $cod ?>">Verificar
							</button>
						</div>

					<?php } ?>
				</td>
			</tr>
			<tr class="verify-message hidden" data-codigo="<?= $cod ?>">
			</tr>
		<?php } ?>
	<?php } ?>
	</tbody>
	<tfoot>


	<?php /*
			<tr>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column">Sub-total</th>
				<th scope="col" class="manage-column">$ <?php echo $totalP ?></th>
			</tr>
			<tr>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column">IVA</th>
				<th scope="col" class="manage-column">$ <?php echo number_format($totalP * 1.19 - $totalP, 0) ?></th>
			</tr> 
			<tr>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column"></th>
				<th scope="col" class="manage-column"><strong>Total</strong></th>
				<th scope="col" class="manage-column"><strong>$ <?php echo number_format($totalP * 1.19, 0) ?></strong></th>
			</tr> */
	?>
	<tr>
		<th scope="col" class="manage-column"></th>
		<th scope="col" class="manage-column"></th>
		<th scope="col" class="manage-column"></th>
		<th scope="col" class="manage-column"><strong>Total</strong></th>

		<?php
		if ( isset( $jsonReemplazos['total'] ) ) {
			?>
			<th scope="col" class="manage-column" data-total="<?= $jsonReemplazos['total'] ?>">
				<strong>
					$ <?php echo $jsonReemplazos['total'] ?>
				</strong>
			</th>
		<?php } else { ?>

			<th scope="col" class="manage-column" data-total="<?= $totalP ?>">
				<strong>
					$ <?php echo $totalP ?>
				</strong>
			</th>
		<?php } ?>

	</tr>
	</tfoot>
	</table>

	<p>&nbsp;</p>

	<form method="post" id="frm-cotizacion-respuesta" action=""
	      onsubmit="return false;">
		<table class="form-table">

			<tbody>
			<tr valign="top">
				<th scope="row"><label>Comentario</label></th>

				<td>
					<?php if ( $retorno['estado'] == 1 ) { ?>
						<textarea name="txt_comentario" rows="10" cols="50"
						          id="txt_comentario"
						          class="large-text code"><?php echo $retorno['respuesta_txt'] ?></textarea>
					<?php } else { ?>
						<p style="background-color:white; border:1px lightgrey solid; padding:6px;">
							<?= $retorno['respuesta_txt'] ?>
						</p>
					<?php } ?>
				</td>
			</tr>

			<tr valign="top">
				<th></th>
				<td align="right">
					<?php if ( $retorno['estado'] == 1 ) {
						?>
						<input type="submit" id="btn-cotizacion-rechaza" name="submit" class="button"
						       value="Rechaza cotización">&nbsp;&nbsp;&nbsp;
						<input type="submit" id="btn-cotizacion-responde" name="submit"
						       class="button button-primary" value="Responde cotización">&nbsp;&nbsp;&nbsp;
					<?php } ?>
					<?php if ( $retorno['estado'] == - 1 ) {
						?>
						<input type="submit" id="btn-cotizacion-rechaza" name="submit" class="button"
						       value="Rechaza cotización">&nbsp;&nbsp;&nbsp;
						<input type="submit" id="btn-cotizacion-cierra" name="submit"
						       class="button button-primary" value="Volver a lista">&nbsp;&nbsp;&nbsp;
					<?php } ?>
					<?php if ( $retorno['estado'] > 1 ) {
						?>
						<input type="submit" id="btn-cotizacion-cierra" name="submit"
						       class="button button-primary" value="Volver a lista">&nbsp;&nbsp;&nbsp;
					<?php } ?>
				</td>
			</tr>
			</tbody>

		</table>
		<input type="hidden" id="hdn-action" name="action" value="0"/>
	</form>

	</div>

<?php
}

// Funciones adicionales de la aplicacion
add_action( 'admin_head', 'cotizador_detalle_js' );
add_action( 'admin_head', 'check_product_js' );
function cotizador_detalle_js() {
	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="http://cristaleria.cl/wp-content/themes/cristaleria/js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="<?= plugins_url( 'chosen.jquery.min.js', __FILE__ ) ?>"></script>
	<script>
		var totalOriginal;
		var reemplazos = {};
		var uxCotizacion = {

			responde: function () {
				if (confirm("¿Desea responder la cotización?")) {
					$("#hdn-action").val('1');
					uxCotizacion.common();
				}
			},

			rechaza: function () {
				if (confirm("¿Desea rechazar esta cotización?")) {
					$("#hdn-action").val('2');
					uxCotizacion.common();
				}
			},

			common: function () {
				/*
				 * Append the replacements JSON string, if any.
				 */
				var $form = $("#frm-cotizacion-respuesta");
				$form.attr('onsubmit', '');
				reemplazos['total'] = $('[data-total]').attr('data-total');
				var input = $("<input>").attr({
					type: "hidden",
					name: "reemplazos",
					id: "reemplazos",
					value: JSON.stringify(reemplazos)
				}).appendTo($form);
				$("#frm-cotizacion-respuesta").submit();
			},

			actualizaTotal: function () {
				/*
				 * Cycles through the replacements objects and updates the total.
				 */
				var nuevoTotal = totalOriginal;
				for (var property in reemplazos) {
					if (reemplazos.hasOwnProperty(property)) {
						console.log(reemplazos[property]);
						nuevoTotal += parseInt(reemplazos[property]['precio'], 10) * parseInt(reemplazos[property]['cantidad']);
					}
				}
				$('[data-total]').attr('data-total', nuevoTotal);
				$('[data-total] strong').text("$ " + nuevoTotal);
			},

			clearReplacement: function (codigo) {
				/*
				 * Clears the table row and removes the product replacement
				 * and updates the total price
				 */
				var $replacementRow = $('.verify-message[data-codigo="' + codigo + '"]');
				$replacementRow.addClass('animated bounceOutUp').html('').removeClass('animated bounceOutUp');
				delete reemplazos[codigo];
				uxCotizacion.actualizaTotal();
				console.log(reemplazos);
			},

			setReplacement: function (codigo_viejo, codigo_nuevo, nombre, cantidad, precio) {
				/*
				 * Sets the table row and adds the product replacement
				 * and updates the total price
				 */
				var $replacementRow = $('.verify-message[data-codigo="' + codigo_viejo + '"]');
				var $backgroundColor = $(".similar-table[data-codigo='" + codigo_viejo + "']").css('background-color');

				var html = '<td class="verify-cod"><strong>' + codigo_nuevo
					+ '</strong> (reemplaza a ' + codigo_viejo + ')</td>'
					+ '<td class="verify-nombre"><strong>' + nombre + '</strong></td>'
					+ '<td class="verify-cantidad"><strong>' + cantidad + '</strong></td>'
					+ '<td class="verify-unidad"><strong>$ ' + precio + '</strong></td>'
					+ '<td class="verify-subtotal"><strong>$ '
					+ parseInt(precio, 10) * parseInt(cantidad, 10)
					+ '</td>';

//				$replacementRow.hide().html(html).css('background-color', $backgroundColor);
				$replacementRow.hide().html(html).css('background-color', '#e0e2eb');
				$replacementRow.show().addClass('animated bounceInDown').removeClass('animated bounceInDown');

				reemplazos[codigo_viejo] = {
					codigo: parseInt(codigo_nuevo, 10),
					precio: parseInt(precio, 10),
					cantidad: parseInt(cantidad, 10),
					nombre: nombre
				};
				uxCotizacion.actualizaTotal();
				console.log(reemplazos);
			},

			setReplacementError: function (codigo, message) {
				/*
				 * Sets the table row with the proper error message and clears any code's replacement
				 */
				var $replacementRow = $('.verify-message[data-codigo="' + codigo + '"]');
				var $backgroundColor = $(".similar-table[data-codigo='" + codigo + "']").css('background-color');

				uxCotizacion.clearReplacement(codigo);
				$replacementRow
					.hide()
					.html('<td colspan="5"><p class="replacement-error">' + message + '</p></td>')
					.css('background-color', '#e0e2eb');
//					.css('background-color', $backgroundColor);

				$replacementRow.show().addClass('animated bounceInDown').removeClass('animated bounceInDown');
				window.setTimeout(function () {
					$replacementRow.html('');
					$('.manual-code-input[data-codigo="' + codigo + '"]').val('');
				}, 3000);
				uxCotizacion.actualizaTotal();
				console.log(reemplazos);
			}
		};


		$(document).ready(function () {
			totalOriginal = parseInt($('[data-total]').attr('data-total'), 10);

			$('#btn-cotizacion-rechaza').click(function () {
				uxCotizacion.rechaza();
			});
			$('#btn-cotizacion-responde').click(function () {
				uxCotizacion.responde();
			});
			$('#btn-cotizacion-cierra').click(function () {
				window.location.href = "<?php echo admin_url() . 'admin.php?page=cotizador/cotizador-grid.php'; ?>";
			});

			// Initialise chosen binding behavior for stock-related product replacement
			$(".chosen").chosen({
				disable_search_threshold: 10,
				allow_single_deselect: true
			}).change(function (e) {
				// Add or remove the selected suggested product
				var $that = $(this);
				var dataCodigo = $that.attr('data-codigo');
				var $selected = $('select.chosen[data-codigo="' + dataCodigo + '"]').find(":selected");

				// DEBUG
//				console.log("selected value (codigo nuevo) : ", $selected.attr('value'));
//				console.log("that[0].value (codigo nuevo) : ", $that[0].value);
//				console.log("selected.attr(data-nombre): ", $selected.attr('data-nombre'));
//				console.log("selected.attr(data-precio): ", $selected.attr('data-precio'));
//				console.log("selected.attr(data-cantidad): ", $selected.attr('data-cantidad'));

				if ($that[0].value === "") {
					// If no option selected, clear everything
					uxCotizacion.clearReplacement(dataCodigo);
				} else {
					uxCotizacion.setReplacement(
						dataCodigo,
						$selected.attr('value'),
						$selected.attr('data-nombre'),
						$selected.attr('data-precio'),
						$selected.attr('data-cantidad')
					);
				}
			});

			// Avoid 0px-width Chosen elements, hiding the <select> after Chosen initialisation. Match table colors.
			var $similarTable = $(".similar-table");
			$similarTable.hide();
			$similarTable.each(function () {
				$(this).css('background-color', $(this).prev().css('background-color'));
			});

			// Show alternative products with current stock
			$('.similar').on('click', function (e) {
				var $this = $(this);
				var codigoSinStock = $this.attr('data-codigo');
				if ($this.html() == 'Reemplazar producto') {
					$this.html('No reemplazar producto');
				} else {
					$this.html('Reemplazar producto');
					uxCotizacion.clearReplacement(codigoSinStock);
					$('.manual-code-input[data-codigo="' + codigoSinStock + '"]').val('');
				}
				$('tr[data-codigo=' + codigoSinStock + ']').toggle();
			});
		});

	</script>
<?php
}

/*
 * Fires an AJAX call to the check_product function when replacing out-of-stock .
 * Handles disabled state of the input box according to the selected radio value.
 */
function check_product_js() {
	?>
	<script>
		jQuery(document).ready(function ($) {

			// Enable manual code input when its radio is selected and disable suggestions
			$(".manual-code-radio").on('change', function () {
				var codigo = $(this).attr('data-codigo');
				var isChecked = $(this).prop('checked');
				var $relatedInput = $("input.manual-code-input[data-codigo=" + codigo + "]");
				var $relatedButton = $("button.manual-code-verify[data-codigo=" + codigo + "]");
				var $relatedSuggestions = $(".chosen[data-codigo=" + codigo + "]");

				if (isChecked) {
					// Enable text input and button
					$relatedInput.prop("disabled", false);
					$relatedButton.prop("disabled", false);

					// Clear and disable chosen <select>,
					$relatedSuggestions.val('').prop('disabled', true).trigger("chosen:updated");
					uxCotizacion.clearReplacement(codigo);
				}
			});

			// Disable manual code input and enable sugestions when its radio is selected
			$(".suggested-code-radio").on('change', function () {
				var codigo = $(this).attr('data-codigo');
				var isChecked = $(this).prop('checked');
				var $relatedInput = $("input.manual-code-input[data-codigo=" + codigo + "]");
				var $relatedButton = $("button.manual-code-verify[data-codigo=" + codigo + "]");
				var $relatedSuggestions = $(".chosen[data-codigo=" + codigo + "]");

				if (isChecked) {
					$relatedSuggestions.prop("disabled", false).trigger("chosen:updated");
					$relatedInput.prop("disabled", true).val('');
					$relatedButton.prop("disabled", true);
					uxCotizacion.clearReplacement(codigo);
				}
			});

			// Send AJAX request for the product code and add it
			$('.manual-code-verify').on('click', function () {
				var codigoNoStock = $(this).attr('data-codigo');
				var codigoConsulta = $('input.manual-code-input[data-codigo=' + codigoNoStock + ']').val().trim();
				var reqStock = $(".similar-table[data-codigo=" + codigoNoStock + "]").attr("data-cantidad");

				var data = {
					'action': 'check_product',
					'codigo': codigoConsulta,
					'reqStock': reqStock,
					'codigoNoStock': codigoNoStock
				};

				console.log("AJAX data:\t", data);

				// If response is successful add it to replacements object
				$.post(ajaxurl, data, function (response) {

					var jsonRes = JSON.parse(response);
					var resStock = null;
					var reqStock = +jsonRes['reqStock'];
					var codigoNoStock,
						codigoReplacement = '',
						nombreReplacement = '',
						precioReplacement = '';

					console.log(jsonRes);

					if (jsonRes['productStock']) {
						resStock = parseInt(jsonRes['productStock']['stock'], 10);
					}

					if (jsonRes['existsPost'] && jsonRes['productStock']) {
						codigoReplacement = jsonRes['productStock']['codigo'];
						nombreReplacement = jsonRes['productPost']['post_title'];
						precioReplacement = jsonRes['productStock']['precio'];
					}

					if (!jsonRes['exists']) {

						uxCotizacion.setReplacementError(
							jsonRes['codigoNoStock'],
							"Producto inexistente"
						);

					} else if (jsonRes['exists'] && resStock >= reqStock) {

						uxCotizacion.setReplacement(
							jsonRes['codigoNoStock'],
							codigoReplacement,
							nombreReplacement,
							jsonRes['reqStock'],
							precioReplacement
						);

					} else if (jsonRes['exists'] && resStock < reqStock) {

						uxCotizacion.setReplacementError(
							jsonRes['codigoNoStock'],
							"Stock insuficiente, por favor elija otro producto"
						);
					}
				});
			});
		});
	</script>
<?php
}
