<?php 
/**
 * Plugin Name: Cotizador Productos
 * Plugin URI: http://cesarg.cl/WPPlugins/cotizador
 * Description: Sistema de cotizador de productos para ser preguntados enviados al proveedor
 * Version: 1.0
 * Author: Cesar Gonzalez Molina
 * Author URI: http://cesarg.cl
 * License: GPL2
 */
 
 /*  Copyright 2014 Cesar Gonzalez  (email : cesar@cesarg.cl)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Octubre 2014 Cristian Yanez (email : cristian@melmac.cl)
    Agregada UI para el reemplazo de productos sin stock.
*/

// Fuerzo el inicio de la session en el sistema para mantener la informacion durante el usuario está en la aplicacion
@session_start(); 


// Constantes de la aplicacion
define('CGCOTIZADORPLUGIN_PAGE_TITLE', 		'Productos');
define('CGCOTIZADORPLUGIN_MENU_TITLE', 		'Productos');
define('CGCOTIZADORPLUGIN_MENU_PAGE' , 		'cotizador_page');
define('CGCOTIZADORPLUGIN_MENU_POSIC', 		29 );
define('CGCOTIZADORPLUGIN_CUSTOMPOST_NAME', 'cotizadorp');
define('EMAILCOTIZACION', 'contacto@cristalerialapaz.cl');
//define('EMAILCOTIZACION', 'cesarg.cl@gmail.com');

define('TAGCARRO', 'cotizacionCliente' );


if ($_SESSION[TAGCARRO] == null || !is_array( $_SESSION[TAGCARRO] ))
	$_SESSION[TAGCARRO] = array();


// No puede ser invocado directamente
if ( !function_exists( 'add_action' ) )
	exit('Saludos!,<br>lamentablemente no puedes invocar este archivo de forma directa, por favor hazlo a través del administrador de WordPress.');


require_once dirname(__FILE__) . '/fn.php';

// Cargo la configuracion del administrador
if ( is_admin() )
	require_once dirname( __FILE__ ) . '/administrador.php';
	
// Cargo el administrador de cotizaciones
if ( is_admin() ) {	
	require_once dirname( __FILE__ ) . '/cotizador-grid.php';
	require_once dirname( __FILE__ ) . '/cotizador-detalle.php';
}


if ( is_admin() ) {
	//require_once dirname( __FILE__ ) . '/upload-data.php';
}

// Muestro los custom post en el loop de elementos
add_action( 'pre_get_posts', 'add_my_post_types_to_query' );


// Permito la carga de POST en el catalogo
function add_my_post_types_to_query( $query ) {
	if ( !is_admin() && !is_page() && $query->is_main_query() )
		$query->set( 'post_type', array( CGCOTIZADORPLUGIN_CUSTOMPOST_NAME ) );	
	return $query;
}







/*********************************
 *  SHORTCODES de la aplicacion  *
 *********************************/
 
// ** Imprime la lista de los productos destacados en alguna pagina
function imprimeProductosDestacados () {
	$strRetorno = '';
	
	// Loop de los destacados
	$args 		= array( 'post_type' 			=> 'cotizadorp',
					     'ignore_sticky_posts' 	=> 1,
					     'orderby' 				=> 'rand',
					     'meta_key' 			=> 'prod_destacado',
					     'meta_compare' 		=> '=',
					     'meta_value' 			=> '1',
					     'orderby' 				=> 'title', 
					     'order' 				=> 'ASC' );
	query_posts( $args );
	
	// formateo los resultados
	if (have_posts()) {
		$strRetorno .= '<div id="muestra-productos">';
		while (have_posts()) {
			the_post();
			$strRetorno .= htmlBoxDestacado (get_the_ID(), 
											 get_the_post_thumbnail(get_the_ID(), array(236,226)), 
											 get_the_title(), 
											 get_post_meta(get_the_ID(),'prod_codigo', true));	
		}
		$strRetorno .= '</div>';
	} else {
		$strRetorno = 'No hay productos destacados';
	}
	wp_reset_query();
	
	return $strRetorno;
}
// Formato de las cajas de los destacados
function htmlBoxDestacado ( $id, $imagen, $titulo, $codigo) {

	$htmlStr  = '<div class="producto"><a href="' . get_permalink($id) . '">' . $imagen . '</a>';
	$htmlStr .= '    <h3><a href="' . get_permalink($id) . '">' . $titulo . '</a></h3>';
	$htmlStr .= '</div>';

	return $htmlStr;
}

// Detalle del producto interno
function htmlProductoDetalle ($id, $imagen, $titulo, $codigo, $texto='') {

	$campoAdicional = '';
	$dataMetaAdicional = get_post_meta( $id );
	if (isset($dataMetaAdicional['sub_item']) && is_array($dataMetaAdicional['sub_item'])) {
		$codigo = '';
		$campoAdicional = '<select name="suboption" id="suboption">';
		foreach ($dataMetaAdicional['sub_item'] as $value) {
			$temp = explode('|', $value);
			if ($codigo == '') $codigo = $temp[0];
			$campoAdicional .= '<option value="' . $temp[0] . '" >' . $temp[1] . '</option>';
		}
		$campoAdicional .= '</select>';
	}

	$permalink = get_permalink($id);
	$urlCarroC = site_url('carro-de-compras');

	$htmlStr = <<<HTMLDETAILED

	<div class="producto_detalle">
		<table>
			<tr>
				<td>$imagen</td>
				<td>
					<h3><a href="$permalink">$titulo</a></h3>
					<p>Código: <span id="codigoProd" >$codigo</span></p>
					<p>$texto</p>
					<div class="ultimos-campos">
						<div class="btn-cotizar"><a id="cotiza_$id" href="#" lnk="$urlCarroC" >COTIZAR</a></div>
						<div class="cantidad">Cant. <input type="text" id="txt_cotiza_$id" value="1"></div>
						$campoAdicional
					</div>
				</td>
			</tr>
		</table>
	</div>
	
HTMLDETAILED;
	
	return $htmlStr;
}

// Funciones JS adicionales de validacion
function cotizadorFnJs () {
	?>
	<script>
		
		$(document).ready(function(){
		
			$('#suboption').change(function(){
				$("#codigoProd").html( $('#suboption').val() );
			});
		
		
			$(".btn-cotizar a").click( function() {
				var idPost, tmp = $(this).attr("id").split("_");
				if (tmp.length == 2)
					idPost = tmp[1];	
				else
					return false;			
				
				var cantidad = parseInt( $("#txt_cotiza_" + idPost).val() , 10);
				if (cantidad < 1) {
					alert("Para cotizar el producto debe ingresar un valor mayor que 0");
					return false;						
				}
				
				var cadenaRed = $(this).attr("lnk") + '?id=' + idPost + '&cant=' + cantidad;
				
				if ($('#suboption').val() != undefined)
					cadenaRed += '&sub=' + $('#suboption').val();
				
				window.location.replace( cadenaRed );
				return false;
			});
			
		});
	</script>
	<?php
}
add_action( 'wp_head', 'cotizadorFnJs' );
add_shortcode( 'cg_prod_cotizador', 'imprimeProductosDestacados');








// ** Tabla del carro de compras
function imprimeTablaCarroCotizacion () {

	$listaProductos = '';
	$totalProductos = 0;
	$tablaHtml		= '';
	
	// Funcionalidad de agregar productos al carro de cotizacion
	if (isset($_GET['id']) && 
		isset($_GET['cant']) &&
		Fn::validaNumero( $_GET['id'] ) && 
		Fn::validaNumero( $_GET['cant'] ) ) {


		$subProducto = (isset( $_GET['sub'] )) ? Fn::limpiaParametros($_GET['sub']) : null;
		$id 		= $_GET['id'];
		$cantidad	= $_GET['cant'];
		$prodData 	= get_post( $id , ARRAY_A);
		$prodCode 	= get_post_meta($id, 'prod_codigo', true);//prod_codigo
		$titulo 	= $prodData['post_title'];

		// En caso de ser un sub-producto
		if ($subProducto != null) {
			$dataMetaAdicional = get_post_meta( $_GET['id'] );
			if (isset($dataMetaAdicional['sub_item']) && is_array($dataMetaAdicional['sub_item'])) {
				foreach ($dataMetaAdicional['sub_item'] as $value) {
					$temp = explode('|', $value);
					if ($temp[0] == $_GET['sub']) {
						$titulo    .=  ', ' . $temp[1];
						$prodCode 	= $temp[0];
					}
				}
			}
		}

		$itemChart = $_SESSION[TAGCARRO][$prodCode];
		if ($itemChart == null) {
			$_SESSION[TAGCARRO][$prodCode] = array('code' 	=> $prodCode, 
											 'desc' 	=> $titulo, 
											 'cantidad' => $cantidad);
		} else {
			$preTotal = intval($itemChart['cantidad'], 10) +  intval($cantidad, 10);
			$_SESSION[TAGCARRO][$prodCode]['cantidad'] = $preTotal;
		}	
	}
	
	
	// Funcionalidad de eliminar elemento desde el carro de compras
	if (isset($_GET['del']))
		unset($_SESSION[TAGCARRO][Fn::limpiaParametros($_GET['del'])]);
	
	
	// Loop de registros para ser desplegado en la pagina
	if ( $_SESSION[TAGCARRO] != null && is_array($_SESSION[TAGCARRO])) {
		foreach ( $_SESSION[TAGCARRO] as $key => $data ) {
			$listaProductos .= rowTableProducto($key, $data['code'], $data['desc'], $data['cantidad']);
			$totalProductos += intval($data['cantidad'], 10);
		}
		if (count($_SESSION[TAGCARRO]) == 0)
			$listaProductos = '<tr><td colspan="4"><p>No hay productos en el carro</p></td></tr>';
	} else {
		$listaProductos = '<tr><td colspan="4"><p>No hay productos en el carro</p></td></tr>';
	}
	
	
	$tablaHtml .= '<form>
					<input type="checkbox" id="chk_criterio" name="acepto" value="1"> En caso de no existir producto, deseo sustitución según criterio de Cristaleria La Paz.
				   </form>';
	
	// Tabla de elementos agregados
	$tablaHtml .= '<table><tbody>
							<tr>
								<td><p class="titulo">PRODUCTO</p></td>
								<td><p class="titulo">CÓDIGO</p></td>
								<td><p class="titulo">CANTIDAD</p></td>
								<td><p class="titulo">ELIMINAR</p></td>
							</tr>' . $listaProductos . '
				   </tbody></table>';
	
	// resumen de la cantidad de productos
	$tablaHtml .= '<div class="resumen-compra">
                        <div class="total-productos">TOTAL PRODUCTOS</div>
                        <div class="total-cantidad"><span class="nroItems" >' . $totalProductos . '</span></div>
                   </div>';
    
    // Botones para seguir cotizando
    $tablaHtml .= '<div class="botones">
                        <div class="boton"><a href="' . site_url() .'">SEGUIR COTIZANDO</a></div>
                        <div class="boton"><a href="' . site_url('cotizacion') .'">ENVIAR COTIZACIÓN</a></div>
                   </div>';
                   
    $tablaHtml .= '<script>$(document).ready(function () {
						$(".nroItems").html(' . Fn::totalizadorProductos() . ');
					});</script>';

	return $tablaHtml;	
}
// Funcion interna de impresion de fila
function rowTableProducto ( $id = 0, $code = '', $title = '', $cantidad = 0 ) {
	$rowCotizacionHtml = '<tr>
							<td><p>' . $title . '</p></td>
							<td><p>' . $code . '</p></td>
							<td><input type="text" class="txt_cant" id="txtcant|' . $id . '" value="' . $cantidad . '"></td>
							<td><a href="javascript:eliminaItem(\'' . $code . '\',\'' . $title . '\')"><img src="' . get_bloginfo('template_directory') . '/img/ico-eliminar.png" alt="Eliminar producto"></a></td>
						  </tr>';  
	return $rowCotizacionHtml;
}
add_shortcode('tablacotizar', 'imprimeTablaCarroCotizacion');


function tablaProductosJs () {
	?>
	<script>
	
	
		$(document).ready(function () {
			$(".nroItems").html(<?php echo Fn::totalizadorProductos() ?>);
			$(".txt_cant").keypress(function( e ){
				if (e.which == 13)
					actualizaItem ( $(this) );
			});
			$(".txt_cant").blur(function(){
				actualizaItem( $(this) );
			});
			$("#chk_criterio").click(function(){
				$.ajax({
					url:"<?php echo site_url() . '/wp-admin/admin-ajax.php?action=actualiza_check' ?>",
					data : {"checkop" : (($("#chk_criterio").is(":checked"))?1:0)},
					type : "POST"
				}).done(function (data){
					console.log(data);
				});
			});
			
			
		});
		
		function actualizaItem ( elem ) {
			if ( fn.validaEntero($(elem).val()) ) {
			
				var vals = $(elem).attr("id").split('|');
				var id	 = vals[1];
				var cant = $(elem).val();
				
				$.ajax({
					url  : "<?php echo site_url() . '/wp-admin/admin-ajax.php?action=actualiza_carro' ?>",
					data : {"id" : id, "cant" : cant},
					type : 'POST'
				}).done(function( data ){
					if (data != "error") 
						$(".nroItems").html( data );
				});
			} else {
				alert("Debe ingresar un valor numerico mayor que 0");
				$(elem).focus();
			}
					
		}

		function eliminaItem ( id, title ) {
			if (confirm("Desea eliminar el producto " + title))
				window.location.replace("<?php echo site_url('carro-de-compras') . '?del=' ?>" + id); 
		}
		
		var fn = {
			validaEntero : function ( value ) {
		    	var regEmail = /^([0-9])+$/i;
		    	return regEmail.test(value);
		  	}
		}
		
		
	</script>
	<?php
}
add_action( 'wp_head', 'tablaProductosJs' );



// http://www.cristaleria.cl/wp-admin/admin-ajax.php?action=actualiza_carro
function actualiza_cantidad_producto_ () {
	
	// invoco librerias y objetos necesarios
	global $wpdb,$_POST;

	if (!isset($_POST['cant']) || !isset($_POST['cant']) || !Fn::validaNumero( $_POST['cant']))
		die("error");

	if ($_POST['cant'] < 1)
		die("error");

	if ($_SESSION[TAGCARRO][$_POST['id']] == null)
		die("error");
		
	$_SESSION[TAGCARRO][$_POST['id']]['cantidad'] = $_POST['cant'];

	$total = 0;
	foreach ($_SESSION[TAGCARRO] as $key => $data) {
		$total +=  intval($data['cantidad'],10);
	}

	echo $total;
	die();
}
add_action('wp_ajax_actualiza_carro', 'actualiza_cantidad_producto_');
add_action('wp_ajax_nopriv_actualiza_carro', 'actualiza_cantidad_producto_');


// http://www.cristaleria.cl/wp-admin/admin-ajax.php?action=actualiza_check
function actualizaCheck_ () {
	global $_POST;
	$_SESSION['checkop'] = (isset($_POST['checkop']) && $_POST['checkop'] == '1') ? true : false;
	die();
}
add_action('wp_ajax_actualiza_check', 'actualizaCheck_');
add_action('wp_ajax_nopriv_actualiza_check', 'actualizaCheck_');




// Envia el listado de los elementos y sus cantidades al contacto encargado
function envioCotizacion (  ) {
	
	global $wpdb;
	
	if ($_SESSION['usuario'] == null || !isset($_SESSION['usuario']))
		return 'Debe iniciar la <strong><a href="' . site_url('login') . '?step=' . substr(md5(session_id()), 0,8) . '">sesión o registrarse</a></strong> antes de poder enviar una cotización';
	
	if ( $_SESSION[TAGCARRO] != null && is_array($_SESSION[TAGCARRO])) {
		
		$datosUsuario = $_SESSION['usuario'][0];
		
		$htmlCotizacion =  '<p><strong>' . strtoupper($datosUsuario->nombre . ' ' . $datosUsuario->apellido) . '</strong> ha solicitado una cotización</p>';

		$htmlCotizacion .= '<p>&nbsp;</p><p><table>';
		$htmlCotizacion .= '<tr><td><b>Código</b></td><td><b>Descripción</b></td><td><b>Cantidad</b></td></tr>';
		
		// Teniendo el usuario y los productos los envio
		foreach ( $_SESSION[TAGCARRO] as $key => $data )
			$htmlCotizacion .= '<tr><td>' . $data['code'] . '&nbsp;</td><td>' . $data['desc'] . '&nbsp;</td><td>' . $data['cantidad'] . '&nbsp;</td></tr>';
		
		$htmlCotizacion .= '</table></p>';

		$htmlCotizacion .= '<p>&nbsp;</p><p>Fecha cotización : ' . date('d-m-Y H:i:s') . '</p>';
		$htmlCotizacion .= '<p>E-mail : ' . $datosUsuario->email . '</p>';
		if ($datosUsuario->razon != '')
			$htmlCotizacion .= '<p>Razon Social : ' . $datosUsuario->razon . '</p>';
		
		if ($datosUsuario->rut != '')
			$htmlCotizacion .= '<p>RUT : ' . $datosUsuario->rut . '</p>';
		
		if ($datosUsuario->giro != '')
			$htmlCotizacion .= '<p>Giro : ' . $datosUsuario->giro . '</p>';
		
		if ($datosUsuario->region != '')			
			$htmlCotizacion .= '<p>Región : ' . Fn::getRegionById($datosUsuario->region) . '</p>';

		if ($datosUsuario->provincia != '')
			$htmlCotizacion .= '<p>Provincia : ' . Fn::getProvinciaById($datosUsuario->provincia) . '</p>';
		
		if ($datosUsuario->comuna != '')
			$htmlCotizacion .= '<p>Comuna : ' . Fn::getComunaById($datosUsuario->comuna) . '</p>';

		$htmlCotizacion .= '<p>Direccion 1 : ' . $datosUsuario->direccion1 . '</p>';
		if ($datosUsuario->direccion2 != '')
			$htmlCotizacion .= '<p>Direccion 2 : ' . direccion2 . '</p>';
		
		if ($datosUsuario->codigo != '')
			$htmlCotizacion .= '<p>Código postal : '.$datosUsuario->codigo.'</p>';
			
		if ($datosUsuario->telefono != '')
			$htmlCotizacion .= '<p>Teléfono : '.$datosUsuario->telefono.'</p>';
			
		if ($datosUsuario->celular != '')
			$htmlCotizacion .= '<p>Celular : '.$datosUsuario->celular.'</p>';
		
		$datosUsuario->checkop 	= $_SESSION['checkop'];
		$_SESSION['checkop'] 	= 0;
		
		if ($datosUsuario->checkop == '1')
			$htmlCotizacion .= '<p>Desea sustitución en caso de no existir Stock</p>';
		
		
		
		// Envio de la cotizacion
		$emailContacto = EMAILCOTIZACION;
		
		// Para enviar un correo HTML mail, la cabecera Content-type debe fijarse
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		
		// Cabeceras adicionales
		$cabeceras .= 'To: La Paz <' . $emailContacto . '>' . "\r\n";
		$cabeceras .= 'From: ' . strtoupper($datosUsuario->nombre . ' ' . $datosUsuario->apellido) . ' <' . $datosUsuario->email . '>' . "\r\n";
		
		$query = "insert into ct_log (usuario, solicitud, reemplazo) values ('" . $datosUsuario->usuario . "','" . json_encode($_SESSION[TAGCARRO]) . "', '" . $datosUsuario->checkop . "');";
		$wpdb->query($query);
		
		// Mail it
		mail($emailContacto, 'Nueva cotización', $htmlCotizacion, $cabeceras);
		
		$_SESSION[TAGCARRO] = array();
		
		return 'Su cotización ha sido enviada, nos pondremos en contacto con Usted a la brevedad.';	
	}

	
	return 'Para enviar una cotización, debe seleccionar algunos productos';
}
add_shortcode('enviocotizacion', 'envioCotizacion');






// Actualizacion de los precios y stock de los productos
// http://www.cristaleria.cl/wp-admin/admin-ajax.php?action=actualiza_productos
function actualizacionProductosFile_ () {
	global $wpdb;
	
	$newDate = new DateTime( @date() , new DateTimeZone('America/Santiago'));
	$fileTxt = dirname(__FILE__) . '/../../../maestro/productos' . date_format( $newDate, 'Ymd' ) . '.txt';
	$handle = fopen( $fileTxt, 'r' );
	
	$contador = 0;
	if ($handle) {
		while (($line = fgets($handle)) !== false) {
			$chunk = explode(';', $line);
			if (count($chunk) == 3) {
				$contador++;
				$query = "INSERT INTO ct_productos ( codigo, stock, precio ) VALUES ('" . $chunk[0] . "','" . $chunk[1] . "','" . $chunk[2] . "') 
						  ON DUPLICATE KEY UPDATE stock='" . $chunk[1] . "', precio='" . $chunk[2] . "';";
				$wpdb->query( $query );
			}
		}
		echo 'Datos actualizados ' . $contador;
	}
	die();
}
add_action('wp_ajax_actualiza_productos', 'actualizacionProductosFile_');
add_action('wp_ajax_nopriv_actualiza_productos', 'actualizacionProductosFile_');





// http://www.cristaleria.cl/wp-admin/admin-ajax.php?action=ajaxregioncomunaciudad 
function ajaxRegionComunaCiudad_ () {
	if ($_POST['source'] == 'region')
		echo Fn::lstProvincias( $_POST['id'] );
	if ($_POST['source'] == 'provincia')
		echo Fn::lstComunas( $_POST['id'] );
	die();
}
add_action('wp_ajax_ajaxregioncomunaciudad', 'ajaxRegionComunaCiudad_');
add_action('wp_ajax_nopriv_ajaxregioncomunaciudad', 'ajaxRegionComunaCiudad_');



// TODO: ESTILOS ADICIONALES DEBO SACARLOS o cambiarlos al CSS oficial
function aditionalCssTemporal () { ?>
	<style>
		#content-header #header { padding-top : 0px !important; width : 968px; }
		.current_page_item { border-bottom : 12px solid #b4b8d3; }
		.mensajeError { color: red; text-align: center; background-color: #fae5e4; padding-top: 10px;padding-bottom: 10px; }
		.cat-item a { color: #fff !important;}
	</style><?php
}
add_action( 'wp_head', 'aditionalCssTemporal' );




// Habilitando la opcion de imagen destacada
add_theme_support( 'post-thumbnails' ); 
// habilita el menu superior
register_nav_menu( 'primary', 'Primary Menu' );
