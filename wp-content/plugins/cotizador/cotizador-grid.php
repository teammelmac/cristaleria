<?php 

/*********************************
 * ADMINISTRADOR DE COTIZACIONES *
 ********************************* 

/*
 *	Administrador de cotizaciones arribadas para ser evaludas y retornadas con valores 
 *	finales al cliente que ha solicitado la cotización, las siguientes funciones son las
 *	que habilitan el poder realizarlo adecuadamente, integrado con el framework de wordpress
 */
 
add_action( 'admin_menu' , 'inicializa_cotizacion_grid_page');		// Inicio la funcionalidad de los Custom Post
function inicializa_cotizacion_grid_page () {
	add_menu_page('cotizaciones', 'Cotizaciones', 'administrator', __FILE__, 'cotizacion_grid_page', 'dashicons-list-view', 30);
}

// Clase de la tabla de cotizaciones
// Libreria de dependencias
if ( ! class_exists( 'WP_List_Table' ) )
	require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');

// Clase de contruccion de la grilla
class Cotizaciones_List_Table extends WP_List_Table {

	// Define las columnas de la tabla
	public function get_columns() {
		$columns = array (
			'id'		=> 'ID',
			'stamp'		=> 'Stamp',
			'arribo'	=> 'Fecha recepción',
			'cliente'	=> 'Cotizante',
			'email'		=> 'E-mail',
			'estado'	=> 'Estado'
		);
		return $columns;
	}

	
	public function prepare_items() {
	
		$columns 	= $this->get_columns();
		$hidden 	= $this->get_hidden_columns();
		$sortable 	= $this->get_sortable_columns();
		
		$data 		= $this->table_data();
		// Ordenamiento de la data
		usort( $data, array( &$this, 'sort_data' ) );
		// Paginacion
		$perPage 		= 20;
        $currentPage 	= $this->get_pagenum();
        $totalItems 	= count($data);

        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ));

        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
		
		$this->_column_headers 	= array( $columns, $hidden, $sortable );
		$this->items 			=  $data;
	}

	// Define las columnas ocultas
	public function get_hidden_columns () {
		return array('id','stamp');
	}
	
	public function get_sortable_columns () {
		return array('arribo' 	=> array('arribo' , false), 
					 'cliente' 	=> array('cliente', false),
					 'estado' 	=> array('estado' , false));
	}
	
	public function table_data() {
		global $wpdb, $_POST;
		
		$retorno 	= array();
		$filtro 	= '';
		
		if (isset($_POST['s'])) {
			$queryFiltro = Fn::limpiaParametros( $_POST['s'] );
			$filtro .= " and ( u.nombre like '%$queryFiltro%' or email like'%$queryFiltro%' )";
		}
		
		if (isset($_GET['f']) && ($_GET['f']== '1' || $_GET['f']=='2'))
			$filtro .= " and (l.estado ='" . $_GET['f'] . "' )";
		
		$query = "select l.id, 
						 l.stamp, 
						 DATE_FORMAT(l.stamp, '%d/%m/%Y %r') as arribo,
						 CONCAT(u.nombre, ' ', u.apellido) as cliente, 
						 u.email, 
						 l.fecha_respuesta, 
						 l.estado
				  from ct_log l 
				  left join ct_user u 
						on l.usuario = u.usuario 
				  where 1 and estado < 3 $filtro";
		
		$retorno = $wpdb->get_results( $query, ARRAY_A );
				  
		return $retorno;
	}
	
	public function column_id($item) {
		return $item['id'];
	}
	
	private function sort_data( $a, $b ) {
        // Set defaults
        $orderby 	= 'stamp';
        $order 		= 'desc';
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby'])) $orderby = $_GET['orderby'];
        // If order is set use this as the order
        if(!empty($_GET['order'])) $order = $_GET['order'];
		// Datos ordenados
        $result = strcmp( $a[$orderby], $b[$orderby] );
		
        if($order === 'asc') return $result;

        return -$result;
    }
	
	
	public function column_default ( $item, $column_name) {
		$urlLink = 	site_url() . '/wp-admin/admin.php?page=cotizacion-detalle&id=' . $item['id'];
		switch ($column_name) {
			case 'id':
			case 'stamp':
			case 'arribo':
			case 'cliente':
			case 'email':
			case 'fecha_respuesta':
				return '<a href="' . $urlLink . '" class="">' . $item[ $column_name ] . '</a>';
			case 'estado':
				$stringMsg = '';
				switch ($item[ $column_name ]) {
					case '1':
						$stringMsg = 'Pendiente';
						break;
					case '2':
						$stringMsg = 'Respondido';
						break;
					case '3':
						$stringMsg = 'Rechazado';
						break;
				}
				return '<a href="' . $urlLink . '" class="">' . $stringMsg . '</a>';
				
			default :
				return print_r( $item, true);
		}
	}
}


// Pagina con la grilla de las cotizaciones
function cotizacion_grid_page () {
	$tablaCotizaciones = new Cotizaciones_List_Table();
	$tablaCotizaciones->prepare_items();
	?>
	<script>
	function accionFiltro ( val ) {
		window.location.href="<?php echo admin_url() . 'admin.php?page=cotizador/cotizador-grid.php&f='; ?>" + jQuery('#cmb_f').val();
		}
	</script>
	<div class="wrap">	
		<h2>Cotizaciones recepcionadas </h2>
		<p>&nbsp;</p>
		<form method="post">
		    <input type="hidden" name="page" value="Cotizaciones_List_Table" />
		    <?php $tablaCotizaciones->search_box('Buscar', 'search_cliente'); ?>
		</form>
		Estado 
		<select name="f" id="cmb_f" onChange="accionFiltro('hola')" >
			<option selected="selected" value="">Todos</option>
			<option <?php echo (isset($_GET['f']) && $_GET['f'] == '1' ) ? 'selected' : '' ?> value="1">Pendiente</option>
			<option <?php echo (isset($_GET['f']) && $_GET['f'] == '2' ) ? 'selected' : '' ?> value="2">Respondido</option>
		</select>
		<?php $tablaCotizaciones->display(); ?>
	</div>
	<?php	
}