<?php 
/*
 *	Libreria de funciones comunes para ser usado para el plugin
 *	de cotizacion
 *
 */
 
 class Fn {
 
 	/**
 	 *	Limpia los parametros de codigo nocivo
 	 */
	function limpiaParametros( $param ) {
	    $cross_site_scripting = array ( '@<script [^>]*?>.*?</script>@si',  		// Remover javascript
				                        '@< [\/\!]*?[^<>]*?>@si' );                 // Remover etiquetas HTML
	    $inyeccion_sql = array ( '/\bAND\b/i', '/\bOR\b/i', '/\bSELECT\b/i',
	                             '/\bFROM\b/i', '/\bWHERE\b/i', '/\bUPDATE\b/i',
	                             '/\bDELETE\b/i', '/\b\*\b/i', '/\bCREATE\b/i' );
	    $retorno = preg_replace ( $inyeccion_sql, "", $param );
	    $retorno = preg_replace ( $cross_site_scripting, "", $retorno );
	    $retorno = htmlentities( $retorno, ENT_QUOTES );
	    return trim( $retorno );
	}
 
 	/**
 	 *	Valida que sean numeros enteros
 	 */
 	function validaNumero ( $var ) {
	 	$patron = "/^[0-9]+$/";
 	    return preg_match($patron, $var);
 	}
 	
 	/**
 	 *	Valida el correo electronico
 	 */
 	function validaEmail ( $var ) {
	 	$patron = "/^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$/";
 	    return preg_match($patron, $var);	 	
 	}
 	
 	function validaRut ( $rut ) {
 		$patron = "/^([0-9]{7,8})\-([0-9]|k|K){1}$/";
 		if (preg_match($patron, $rut)) {
 			$dataRut = explode('-', $rut);
 			if ($dataRut[1] == 'K')
 				$dataRut[1] = 'k';
 			$digesto = Fn::_digesto($dataRut[0]);
 			return ($digesto == $dataRut[1]);	
 		}
 		return false;
 	}
 	function _digesto ( $T ) {
 		$M=0;$S=1;
 		for(;$T;$T=floor($T/10))
 			$S=($S + $T % 10 * ( 9 - $M++ % 6))%11;
 		return $S ? $S-1 : 'k';
 	}
 	
 	
 	/**
 	 *	totalizador de productos en carro
 	 */
 	function totalizadorProductos () {
	 	$totalProductos = 0;	 	
 		if ( $_SESSION[TAGCARRO] != null && is_array($_SESSION[TAGCARRO])) {
			foreach ( $_SESSION[TAGCARRO] as $key => $data ) {
				$totalProductos += intval($data['cantidad'], 10);
			}
		}
		return $totalProductos;
 	}
 	
 	
 	function subCategorias ( $slugCategoria ) {
 		$objCategoria = get_category_by_slug ( $slugCategoria );
 		$idCatPadre = $objCategoria->cat_ID;
 		return 'orderby=count&order=desc&show_count=0&hide_empty=0&use_desc_for_title=0&child_of='.$idCatPadre.'&taxonomy=category&title_li=&show_option_none=';
 	}
 	
 	
 	/**
 	 * Lista completa de la region
 	 * @param number $preSelect
 	 * @return string
 	 */
 	function lstRegiones ( $preSelect = 0 ) {
 		global $wpdb;
 		$html		= '';
 		$query 		= 'SELECT * FROM region';
 		$retorno 	= $wpdb->get_results ( $query, ARRAY_A );
 		$html 	   .= '<option value="">Seleccione...</option>';
 		foreach ($retorno as $data)
 			$html .= '<option value="' . $data['re_id'] . '" ' . (($preSelect == $data['re_id']) ? 'selected' : '') . ' >' . $data['re_titulo'] . '</option>';
 		return $html;
 	}
 	
 	
 	/**
 	 * Lista de provincias dependiendo de la region
 	 * @param number $region
 	 * @param number $preSelect
 	 * @return string
 	 */
 	function lstProvincias ( $region = 0, $preSelect = 0 ) {
 		global $wpdb;
 		$html		= '';
 		$query 		= "SELECT * FROM provincia WHERE pr_region='$region';";
 		$retorno 	= $wpdb->get_results ( $query, ARRAY_A );
 		$html 	   .= '<option value="">Seleccione...</option>';
 		foreach ($retorno as $data)
 			$html .= '<option value="' . $data['pr_id'] . '" ' . (($preSelect == $data['pr_id']) ? 'selected' : '') . ' >' . $data['pr_nombre'] . '</option>';
 		return $html;
 	} 
 	
 	/**
 	 * Lista de comunas dependientes de la provincia
 	 * @param number $provincia
 	 * @param number $preSelect
 	 * @return string
 	 */
 	function lstComunas ( $provincia = 0, $preSelect = 0 ) {
 		global $wpdb;
 		$html		= '';
 		$query 		= "SELECT * FROM comuna WHERE co_pr='$provincia';";
 		$retorno 	= $wpdb->get_results ( $query, ARRAY_A );
 		$html .= '<option value="">Seleccione...</option>';
 		foreach ($retorno as $data)
 			$html .= '<option value="' . $data['co_id'] . '" ' . (($preSelect == $data['co_id']) ? 'selected' : '') . ' >' . $data['co_descripcion'] . '</option>';
 		return $html; 		
 	}
 	
 	
 	/**
 	 * Obtiene el nombre de la region por su ID
 	 * @param number $id
 	 * @return string
 	 */
 	function getRegionById ( $id = 0 ) {
 		global $wpdb;
 		$query 		= "SELECT * FROM region WHERE re_id='$id'";
 		$retorno 	= $wpdb->get_results ( $query, ARRAY_A );
 		if (count($retorno) == 1) {
 			$retorno = $retorno[0];
 			return $retorno['re_rom'] . ' - ' . $retorno['re_titulo'];
 		}
 		return '';
 	}
 	
 	/**
 	 * Obtiene el nombre de la provincia por su ID
 	 * @param number $id
 	 * @return unknown|string
 	 */
 	function getProvinciaById ( $id = 0 ) {
 		global $wpdb;
 		$query 		= "SELECT * FROM provincia WHERE pr_id='$id'";
 		$retorno 	= $wpdb->get_results ( $query, ARRAY_A );
 		if (count($retorno) == 1) {
 			$retorno = $retorno[0];
 			return $retorno['pr_nombre'];
 		}
 		return '';
 	} 
 	
 	/**
 	 * Obtiene el nombre de la comuna por su ID
 	 * @param number $id
 	 * @return unknown|string
 	 */
 	function getComunaById ( $id = 0 ) {
 		global $wpdb;
 		$query 		= "SELECT * FROM comuna WHERE co_id='$id'";
 		$retorno 	= $wpdb->get_results ( $query, ARRAY_A );
 		if (count($retorno) == 1) {
 			$retorno = $retorno[0];
 			return $retorno['co_descripcion'];
 		}
 		return '';
 	}
 	
 }
 
 