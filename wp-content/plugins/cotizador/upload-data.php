<?php
/**
 * Administrador y procesador de precios
 */
add_action('admin_menu', 'inicia_cotizacion_upload_page');
function inicia_cotizacion_upload_page () {
	add_menu_page('Upload precios', 'uploadPrecios', 'administrator', __FILE__, 'cotizacion_upload_data', 'dashicons-cloud',31);
}


function cotizacion_upload_data() {
	
	?>
	<h2>Actualización del maestro</h2>
	<p>Suba archivo TXT con los valores a actualizar</p>
	<form action="" method="post" enctype="multipart/form-data">
		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row"><label>Archivo para actualizar</label></th>
					<td><input type="file" name="file_upload" ></td>
				</tr>
				<tr valign="top">
					<th scope="row">&nbsp;</th>
					<td><input type="submit" name="submit" class="button button-primary" value="Subir archivo">&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</tbody>
		</table>
	</form>
	<?php
}